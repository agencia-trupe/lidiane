<?php
class Image_upload extends CI_Model{

    var $gallery_path;
    var $gallery_path_url;

    function __construct() {
        parent::__construct();

        $this->gallery_path = realpath(APPPATH . '../assets/img/vagas');
        $this->gallery_path_url = base_url().'assets/img/vagas/';
        $this->load->library('canvas');
        }

    function do_upload(){

        $config = array(
            'allowed_types' => 'jpg|jpeg|gif|png',
            'upload_path' => $this->gallery_path,
            'max_size' => 2000
        );

        $this->load->library('upload', $config);
        $this->upload->do_upload();
        $image_data = $this->upload->data();

        $big = $image_data['full_path'];
        $bigger = new canvas();
        $bigger->carrega( $big)
            ->redimensiona( 580, 385, 'crop' )
            ->grava($this->gallery_path . '/big/' . $image_data['file_name']);

        return $image_data;

    }

    function delete($id){

        $query = $this->db->where('id', $id)
                          ->delete('fotos');
        return $query;
    }

}



/* End of file employee.php */
/* Location: ./application/moules/manager/models/fotosm_model.php */