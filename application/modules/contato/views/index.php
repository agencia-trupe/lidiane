<div class="conteudo contato">
    <div class="interna">
        <div class="form left">
            <h1>Envie uma mensagem</h1>
            <?php echo form_open('', 'id="contato-form"'); ?>
            <div class="inputs">
                <?=form_input(array('name'=>'nome','value'=>'','class'=>'nome_form textbox', 'placeholder'=>'nome', 'title'=>'nome'))?><br />
                <?=form_input(array('name'=>'email','value'=>'','class'=>'email_form textbox', 'placeholder'=>'email', 'title'=>'email'))?><br>
            </div>
            <div class="textarea">
                <?=form_textarea(array('name'=>'mensagem','value'=>'','class'=>'mensagem_form mensagem_contato textbox', 'placeholder'=>'mensagem', 'title'=>'mensagem'))?><br />
                <input type="submit" name="submit" value="enviar" id="contato-submit">
                <?=form_close("\n")?>
            </div>
        </div>
        <div class="contatos right">
            <h1>Siga nas redes sociais</h1>
            <ul class="sociais">
                <li><a href="" class="linkedin"></a></li>
                <li><a href="http://twitter.com/lidianelcc" class="twitter"></a></li>
                <li><a href="http://facebook.com/LidianeLourencoPaisagismoeInteriores" class="facebook"></a></li>
            </ul>
            <div class="clearfix"></div>
            <div class="telefone">
                <span class="texto">Telefone:</span> <small>11</small><span class="numero">3758 0278</span>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>
