<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Correspontente
*
*
* @author Nilton de Freitas
* @link http://trupe.net
*
*/
Class Correspondente extends Datamapper{
    var $table = 'correspondentes';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }

    function get_correspondente()
    {
        $correspondente = new Correspondente();
        $correspondente->where('id', '1');
        $correspondente->limit(1);
        $correspondente->get();

        return $correspondente;
    }

}