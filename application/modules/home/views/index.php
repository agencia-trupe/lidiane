<div class="conteudo-home">
	<div class="interna">
		<div class="slide-wrapper">
			<div class="slides">
				<?php foreach ($slides as $slide): ?>
					<div>
						<a class="projeto-home" href="<?php echo $slide->link ?>">
							<img src="<?php echo base_url('assets/img/slides/' . $slide->imagem) ?>" alt="<?php echo $slide->titulo ?>">
						</a>
					</div>
				<?php endforeach ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div> 
</div>
<div class="clearfix"></div>