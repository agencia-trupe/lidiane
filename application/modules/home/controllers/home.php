<?php
class Home extends MX_Controller
{
    var     $data;

    public function __construct()
    {
        parent::__construct();
        $seo['title'] = 'Celito Gonzalez &middot; Home';
        $seo['description'] = 'Nosso Escritório Cria Projetos Residenciais e Gerencia visando uma boa execução de mão de obra especializada priorizando o investimento do Cliente tornando o ambiente funcional, moderno e elegante. Criamos também Projetos Coorporativos Planejando a Área de Trabalho pensando na confiança e dinamismo que toda empresa deve passar aos seus clientes e colaboradores.';
        $seo['keywords'] = 'planejamento físico e financeiro, layout, personalizado, moderno, contemporâneo, funcional, estilo, combinação, elegante, automação, aconchego, arte, cores, tendência, combinação, paisagismo, clean, decoração, designer de interiores, arquitetura, reforma de ambientes,  acompanhamento de obras, projetos de arquitetura, como contratar um profissional de decoração';
        $this->load->library('seo', $seo);
        $this->data['pagina'] = 'home';
        $this->load->model('home/slide');
    }

    public function index()
    {
        $this->data['slides'] = $this->slide->get_all();
        $this->data['conteudo'] = 'home/index';
        $this->load->view('layout/template', $this->data);
    }

    public function insere()
    {
        $dados = array();
        $dados['titulo'] = 'Projeto 01';
        $dados['link']  = 'http://google.com';
        $dados['imagem'] = 'slide_home.jpg';
        $insere = $this->slide->insert($dados);
        if($insere)
        {
            echo 'Ok!';
        }
        else
        {
            echo 'Nooooooooo!';
        }
    }

    public function serve_slides()
    {
        $slides = $this->slide->get_all();
        foreach($slides as $slide)
        {
            $result[] = $slide->imagem;
        }
        $result = json_encode($result);
        echo $result;
    }

}