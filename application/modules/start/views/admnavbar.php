    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo base_url(); ?>">Lidiane Lourenço</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="<?php echo ($module == 'slideshow') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/slideshow'); ?>">Slideshow</a>
              </li>
              <li class="<?php echo ($module == 'paginas') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/paginas'); ?>">Páginas</a>
              </li>
              <li class="<?php echo ($module == 'projetos') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/tipos'); ?>">Projetos</a>
              </li>
              <li class="<?php echo ($module == 'midia') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/midia'); ?>">Clipping</a>
              </li>
              <li class="<?php echo ($module == 'contato') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/contato'); ?>">Contato</a>
              </li>
              <li class="<?php echo ($module == 'usuarios') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('auth/lista'); ?>">Usuários</a>
              </li>
              <li>
                <div class="alert span6 top-alert">
                  <span class="alert-content"></span>
                  <a class="close" href="#">&times;</a>
                </div>
              </li>
              
            </ul>
            <ul class="nav pull-right">
                <li><?php echo anchor('logout', 'Sair'); ?></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row-fluid">