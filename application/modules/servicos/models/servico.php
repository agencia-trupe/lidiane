<?php
class Servico extends Datamapper
{
    var $table = 'servicos';
    function get_all()
    {
        $servico = new Servico();
        $servico->order_by('id', 'ASC')->get();
        $arr = array();
        foreach($servico->all as $servico)
        {
            $arr[] = $servico;
        }
        if(sizeof($arr))
        {
            return $arr;
        }
        return FALSE;
    }

    function get_conteudo( $id )
    {
        $servico = new Servico();
        $servico->where( 'id', $id )->get();
        if( $servico->exists() ){
            return $servico;
        }
        return NULL;
    }

    function change($dados)
    {
        $servico = new Servico();
        $servico->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        $update = $servico->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function insert($dados)
    {
        $servico = new Servico();
        foreach ($dados as $key => $value)
        {
            $servico->$key = $value;
        }
        $servico->created = time();
        $insert = $servico->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $servico = new Servico();
        $servico->where('id', $id)->get();
        if($servico->delete())
        {
            return TRUE;
        }
        return FALSE;
    }
}