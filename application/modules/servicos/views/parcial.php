<div id="accordion">
    <?php foreach ( $servicos as $servico ): ?>
        <a href="#" class="header">
            <h1><?=$servico->titulo; ?></h1>
            <div class="item-icone"></div>
        </a>
        <div class="servicos-texto">
            <div class="servicos-descricao-wrapper">
                <?=$servico->descricao; ?>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php endforeach; ?>
</div>

