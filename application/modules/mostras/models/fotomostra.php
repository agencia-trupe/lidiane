<?php
class Fotomostra extends Datamapper
{
    var $table = 'fotomostras';
    var $has_one = array('mostra');

    function get_mostra($mostra_id)
    {
        $fotomostra = new Fotomostra();
        $fotomostra->where('mostra_id', $mostra_id);
        $fotomostra->order_by('ordem', 'asc');
        $fotomostra->get();
        $arr = array();
        foreach( $fotomostra->all as $f )
        {
            $arr[] = $f;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return FALSE;
    }
    function get_conteudo( $id )
    {
        $fotomostra = new Fotomostra();
        $fotomostra->where( 'id', $id )->get();
        if( $fotomostra->exists() ){
            return $fotomostra;
        }
        return NULL;
    }

    function get_related( $id, $position )
    {
        $fotomostra = new Fotomostra();
        switch ( $position ) {
            case 'prev':
                $fotomostra->where( 'id <', $id);
                break;
            case 'next':
                $fotomostra->where( 'id >', $id);
                break;
        }
        $fotomostra->get(1);
        if($fotomostra->exists())
        {
            return $fotomostra->id;
        }
        return FALSE;
    }
    /**
     * Temporario para desenvolvimento
     * @param  [type] $dados [description]
     * @return [type]        [description]
     */
    function insert($dados)
    {
        $fotomostra = new Fotomostra();
        foreach ($dados as $key => $value)
        {
            $fotomostra->$key = $value;
        }
        $fotomostra->created = time();
        $insert = $fotomostra->save();
        if($insert)
        {
            return $fotomostra->id;
        }
        return FALSE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $fotomostra = new Fotomostra();
            $fotomostra->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($fotomostra->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function deleta_fotomostra($fotomostra_id)
    {
        $fotomostra = new Fotomostra();
        $fotomostra->where('id', $fotomostra_id)->get();
        if($fotomostra->delete())
        {
            return TRUE;
        }
        return FALSE;
    }
}