<div class="conteudo projetos galeria">
    <div class="projeto-meta-nav-wrapper">
        <div class="interna">
            <div class="projeto-meta">
                <h1><?php echo $mostra->titulo ?></h1>
                <div class="projeto-meta-separador"></div>
                <div class="projeto-meta-descricao">
                    <?php echo $mostra->texto ?>
                </div>
            </div>
            <div class="projeto-nav">
                <div class="nav-container">
                    <?php if($prev): ?>
                    <?=anchor( 'mostras/detalhe/' . $prev, '<span>anterior</span>', 'class="projeto-anterior"'); ?>
                    <?php endif; ?>
                </div>                
                <div class="nav-container next-container">
                    <?php if($next): ?>
                    <?=anchor( 'mostras/detalhe/' . $next, '<span>próxima</span>', 'class="projeto-proximo"'); ?>
                    <?php endif; ?>
                </div>
                <div class="grid-container">
                    <a href="<?=site_url( 'mostras'); ?>" class="projeto-grid"><span>todas as mostras</span></a>
                </div>
            </div> 
            <div class="clearfix"></div> 
        </div>
    </div>
   
    <div class="projeto-galeria">
        <div id="c-carousel">
            <div id="wrapper">
                <div id="carousel">
                    <?php foreach ( $fotomostras as $foto ): ?>
                        <img width="810" height="540" src="<?=base_url('assets/img/mostras/fotos/' . $foto->imagem ); ?>" alt="<?=$foto->titulo; ?>">
                    <?php endforeach; ?>
                    <?php if (count($fotomostras) < 2): ?>
                        <img width="810" height="540" src="<?=base_url('assets/img/mostras/fotos/' . $fotomostras[0]->imagem ); ?>" alt="<?=$fotomostras[0]->titulo; ?>">
                        <img width="810" height="540" src="<?=base_url('assets/img/mostras/fotos/' . $fotomostras[0]->imagem ); ?>" alt="<?=$fotomostras[0]->titulo; ?>">
                        <img width="810" height="540" src="<?=base_url('assets/img/mostras/fotos/' . $fotomostras[0]->imagem ); ?>" alt="<?=$fotomostras[0]->titulo; ?>">
                    <?php elseif (count($fotomostras) < 4 && count($fotomostras) > 2): ?>
                        <img width="810" height="540" src="<?=base_url('assets/img/mostras/fotos/' . $fotomostras[0]->imagem ); ?>" alt="<?=$fotomostras[0]->titulo; ?>">
                        <img width="810" height="540" src="<?=base_url('assets/img/mostras/fotos/' . $fotomostras[1]->imagem ); ?>" alt="<?=$fotomostras[1]->titulo; ?>">
                    <?php endif ?>
                </div>
                <div id="prev"></div>
                <div id="next"></div>
                <div class="projetos-galeria-controles">
                    <a href="" class="prev"></a>
                    <a href="" class="next"></a>
                </div>
            </div>
        </div> <!-- /c-carousel -->
       
    </div>
</div>