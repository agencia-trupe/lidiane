<div class="conteudo-projetos projetos-interiores">
	<div class="interna">
		<div class="projetos-lista-slider-wrapper">
			<div class="projetos-lista-sliderrr">
				<div role="projeto-slide">
					<div class="projetos-lista-thumbs-wrapper">
						<a href="<?php echo site_url('projetos/interiores/pascoal-leite') ?>" class="projeto-thumb">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/interiores/1_pascoal_leite/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Pascoal Leite</span>
							</span>
						</a>
						<a href="<?php echo site_url('projetos/interiores/brooklin') ?>" class="projeto-thumb">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/interiores/2_brooklin/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Brooklin</span>
							</span>
						</a>
						<a href="<?php echo site_url('projetos/interiores/quarto-bebe-real-parque') ?>" class="projeto-thumb quarto-bebe-real-parque">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/interiores/3_quarto_bebe_real_parque/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Quarto de Bebê - Real Parque</span>
							</span>
						</a>
						<a href="<?php echo site_url('projetos/interiores/quarto-bebe-paineiras-morumbi') ?>" class="projeto-thumb quarto-bebe-paineiras-morumbi">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/interiores/4_quarto_bebe_paineiras_morumbi/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Quarto de Bebê - Paineiras do Morumbi</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>
<div class="clearfix"></div>