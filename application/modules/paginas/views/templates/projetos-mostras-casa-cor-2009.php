<div class="conteudo-projetos projetos-paisagismo projetos-detalhe">
	<div class="interna">
		<h1>Casa Cor 2009</h1>
		<div class="projetos-detalhe-wrapper">
			<div id="projetos-detalhe-tabs">
			  	<div class="projetos-detalhe-ampliada">
			  		<div id="foto1">
			  			<img src="<?php echo base_url('assets/img/projetos/mostras/3_casa_cor_2009/ampliada-1.jpg') ?>" alt="">
				  	</div>
				  	<div id="foto2">
				  		<img src="<?php echo base_url('assets/img/projetos/mostras/3_casa_cor_2009/ampliada-2.jpg') ?>" alt="">
				  	</div>
				  	<div id="foto3">
				  		<img src="<?php echo base_url('assets/img/projetos/mostras/3_casa_cor_2009/ampliada-3.jpg') ?>" alt="">
				  	</div>
					<div id="foto4">
						<img src="<?php echo base_url('assets/img/projetos/mostras/3_casa_cor_2009/ampliada-4.jpg') ?>" alt="">
				  	</div>
			  	</div>
			  	<ul class="projetos-detalhe-miniaturas">
			    	<li>
			    		<a class="bwWrapper" href="#foto1">
			    			<img src="<?php echo base_url('assets/img/projetos/mostras/3_casa_cor_2009/thumb-1.jpg') ?>" alt="">	
			    		</a>
			    	</li>
			    	<li>
			    		<a class="bwWrapper" href="#foto2">
			    			<img src="<?php echo base_url('assets/img/projetos/mostras/3_casa_cor_2009/thumb-2.jpg') ?>" alt="">
			    		</a>
			    	</li>
			    	<li>
			    		<a class="bwWrapper" href="#foto3">
							<img src="<?php echo base_url('assets/img/projetos/mostras/3_casa_cor_2009/thumb-3.jpg') ?>" alt="">
			    		</a>
			    	</li>
			    	<li>
			    		<a class="bwWrapper" href="#foto4">
							<img src="<?php echo base_url('assets/img/projetos/mostras/3_casa_cor_2009/thumb-4.jpg') ?>" alt="">
			    		</a>
			    	</li>
			  	</ul>
			  	<div class="clearfix"></div>
			</div>
			<a href="<?php echo site_url('projetos/mostras') ?>" class="projetos-detalhe-voltar">
				voltar
			</a>
			<div class="projetos-detalhe-social">
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
				<div class="g-plusone" data-size="medium"></div>
				<script src="//platform.linkedin.com/in.js" type="text/javascript">
				 lang: en_US
				</script>
				<script type="IN/Share" data-counter="right"></script>
				<iframe class="facebook-share" src="//www.facebook.com/plugins/like.php?href=<?php echo current_url(); ?>&amp;send=false&amp;layout=button_count&amp;width=85&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=355115464591773" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:85px; height:21px;" allowTransparency="true"></iframe>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="separador-pagina"></div>
	<div class="interna">
		<div class="outros-projetos">
			<h2>Veja outros projetos</h1>
			<div class="outros-projetos-wrapper">
				<a href="<?php echo site_url('projetos/mostras/fiaflora-2008') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/mostras/1_fiaflora_2008/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/mostras/mesas-decoradas-d-d') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/mostras/2_mesas_decoradas_d_d/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/interiores/quarto-bebe-real-parque') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/interiores/3_quarto_bebe_real_parque/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/paisagismo/casa-pascoal-leite') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/paisagismo/3_casa_pascoal_leite/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/paisagismo/igarat') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/paisagismo/4_igarat/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/paisagismo/campo-belo') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/paisagismo/5_campo_belo/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/paisagismo/brooklin') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/paisagismo/6_brooklin/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>