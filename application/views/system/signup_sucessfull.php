<div class="span6 offset3">
    <div class="well">
        <div class="row-fluid">
                <h2 style="color:#0086b3;">Consistere HRM</h2>
                <h4 style="color:#0086b3;">Criar conta</h4>
                <hr>

                <h3>Conta criada com sucesso!</h3>
                <p>Sua conta foi criada com sucesso, agora você pode completar seu cadastro, incluir
                seu currículo e participar de nossos processos seletivos.</p>
                
                <p>Antes disso, é preciso efetuar login utilizando o botão abaixo</p>
                
                    <?php echo anchor('login', 'Login', 'class="btn btn-small btn-info"'); ?>
                
        </div>
    </div>
</div>