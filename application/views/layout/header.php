<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <meta name="viewport" content="width=1100, initial-scale=1">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/normalize.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>">
        <script src="<?php echo base_url('assets/js/vendor/modernizr-2.6.2.min.js') ?>"></script>
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300">
        <!--[if IE]>
            <style type="text/css">
               .projeto-thumb-hover, .outro-projeto .hover { 
                   background:transparent;
                   filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#CC333333,endColorstr=#CC333333); 
                   zoom: 1;
                } 
            </style>
        <![endif]-->
        <title>Lidiane Lourenço</title>
    </head>
    <body>
        <div class="clearfix"></div>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <header class="pagina">
            <div class="interna">
                <a href="" class="marca-wrapper">
                    <span>Lidiane Lourenço</span>
                </a>
                <nav>
    <ul>
        <li><a class="home <?php echo ($pagina == 'home') ? 'active' : ''; ?>" href="<?php echo site_url() ?>">
                    <span class="imagem"></span>
                    <span class="texto">Home</span>
                    </a>
        </li>
        <li><a class="perfil <?php echo ($pagina == 'perfil') ? 'active' : ''; ?>" href="<?php echo site_url('perfil') ?>">
                    <span class="imagem"></span>
                    <span class="texto">Perfil</span>
                    </a>
        </li>
        <li class="dropdown-container <?php echo ( in_array($pagina, array('projetos', 'paisagismo', 'interiores', 'mostras'))) ? 'active' : ''; ?>"><a class="projetos <?php echo ( in_array($pagina, array('projetos', 'paisagismo', 'interiores', 'mostras'))) ? 'active' : ''; ?>" href="<?php echo site_url('projetos') ?>">
                    <span class="imagem"></span>
                    <span class="texto">Projetos</span>
            </a>
            <ul class="dropdown">
                        <li>
                            <a  class="<?php echo ($pagina == 'paisagismo') ? 'active' : '' ?>"
                                href="<?php echo site_url('projetos/paisagismo') ?>">
                                paisagismo
                            </a>
                        </li>
                        <li>
                            <a  class="<?php echo ($pagina == 'interiores') ? 'active' : '' ?>"
                                href="<?php echo site_url('projetos/interiores') ?>">
                                interiores
                            </a>
                        </li>
                        <li>
                            <a  class="<?php echo ($pagina == 'mostras') ? 'active' : '' ?>"
                                href="<?php echo site_url('projetos/mostras') ?>">
                                mostras
                            </a>
                        </li>
            </ul>
        </li>
        <li><a class="clipping <?php echo ($pagina == 'clipping') ? 'active' : ''; ?>" href="<?php echo site_url('clipping') ?>">
                    <span class="imagem"></span>
                    <span class="texto">Clipping</span>
                    </a>
        </li>
        <li><a class="contato <?php echo ($pagina == 'contato') ? 'active' : ''; ?>" href="<?php echo site_url('contato') ?>">
                    <span class="imagem"></span>
                    <span class="texto">Contato</span>
                    </a>
        </li>
    </ul>
</nav>
                <div class="clearfix"></div>
            </div>
        </header>
        <div id="main" class="pagina">