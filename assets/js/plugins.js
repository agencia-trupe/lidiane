// Avoid `console` errors in browsers that lack a console.
(function() {
    var noop = function noop() {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = window.console || {};

    while (length--) {
        // Only stub undefined methods.
        console[methods[length]] = console[methods[length]] || noop;
    }
}());

// Place any jQuery/helper plugins in here.
// 

/**
 * BxSlider v4.1 - Fully loaded, responsive content slider
 * http://bxslider.com
 *
 * Copyright 2012, Steven Wanderski - http://stevenwanderski.com - http://bxcreative.com
 * Written while drinking Belgian ales and listening to jazz
 *
 * Released under the WTFPL license - http://sam.zoy.org/wtfpl/
 */
(function(e){var t={},n={mode:"horizontal",slideSelector:"",infiniteLoop:!0,hideControlOnEnd:!1,speed:500,easing:null,slideMargin:0,startSlide:0,randomStart:!1,captions:!1,ticker:!1,tickerHover:!1,adaptiveHeight:!1,adaptiveHeightSpeed:500,video:!1,useCSS:!0,preloadImages:"visible",touchEnabled:!0,swipeThreshold:50,oneToOneTouch:!0,preventDefaultSwipeX:!0,preventDefaultSwipeY:!1,pager:!0,pagerType:"full",pagerShortSeparator:" / ",pagerSelector:null,buildPager:null,pagerCustom:null,controls:!0,nextText:"Next",prevText:"Prev",nextSelector:null,prevSelector:null,autoControls:!1,startText:"Start",stopText:"Stop",autoControlsCombine:!1,autoControlsSelector:null,auto:!1,pause:4e3,autoStart:!0,autoDirection:"next",autoHover:!1,autoDelay:0,minSlides:1,maxSlides:1,moveSlides:0,slideWidth:0,onSliderLoad:function(){},onSlideBefore:function(){},onSlideAfter:function(){},onSlideNext:function(){},onSlidePrev:function(){}};e.fn.bxSlider=function(s){if(0!=this.length){if(this.length>1)return this.each(function(){e(this).bxSlider(s)}),this;var o={},r=this;t.el=this;var a=e(window).width(),l=e(window).height(),d=function(){o.settings=e.extend({},n,s),o.settings.slideWidth=parseInt(o.settings.slideWidth),o.children=r.children(o.settings.slideSelector),o.children.length<o.settings.minSlides&&(o.settings.minSlides=o.children.length),o.children.length<o.settings.maxSlides&&(o.settings.maxSlides=o.children.length),o.settings.randomStart&&(o.settings.startSlide=Math.floor(Math.random()*o.children.length)),o.active={index:o.settings.startSlide},o.carousel=o.settings.minSlides>1||o.settings.maxSlides>1,o.carousel&&(o.settings.preloadImages="all"),o.minThreshold=o.settings.minSlides*o.settings.slideWidth+(o.settings.minSlides-1)*o.settings.slideMargin,o.maxThreshold=o.settings.maxSlides*o.settings.slideWidth+(o.settings.maxSlides-1)*o.settings.slideMargin,o.working=!1,o.controls={},o.interval=null,o.animProp="vertical"==o.settings.mode?"top":"left",o.usingCSS=o.settings.useCSS&&"fade"!=o.settings.mode&&function(){var e=document.createElement("div"),t=["WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var i in t)if(void 0!==e.style[t[i]])return o.cssPrefix=t[i].replace("Perspective","").toLowerCase(),o.animProp="-"+o.cssPrefix+"-transform",!0;return!1}(),"vertical"==o.settings.mode&&(o.settings.maxSlides=o.settings.minSlides),c()},c=function(){if(r.wrap('<div class="bx-wrapper"><div class="bx-viewport"></div></div>'),o.viewport=r.parent(),o.loader=e('<div class="bx-loading" />'),o.viewport.prepend(o.loader),r.css({width:"horizontal"==o.settings.mode?215*o.children.length+"%":"auto",position:"relative"}),o.usingCSS&&o.settings.easing?r.css("-"+o.cssPrefix+"-transition-timing-function",o.settings.easing):o.settings.easing||(o.settings.easing="swing"),v(),o.viewport.css({width:"100%",overflow:"hidden",position:"relative"}),o.viewport.parent().css({maxWidth:u()}),o.children.css({"float":"horizontal"==o.settings.mode?"left":"none",listStyle:"none",position:"relative"}),o.children.width(p()),"horizontal"==o.settings.mode&&o.settings.slideMargin>0&&o.children.css("marginRight",o.settings.slideMargin),"vertical"==o.settings.mode&&o.settings.slideMargin>0&&o.children.css("marginBottom",o.settings.slideMargin),"fade"==o.settings.mode&&(o.children.css({position:"absolute",zIndex:0,display:"none"}),o.children.eq(o.settings.startSlide).css({zIndex:50,display:"block"})),o.controls.el=e('<div class="bx-controls" />'),o.settings.captions&&E(),o.settings.infiniteLoop&&"fade"!=o.settings.mode&&!o.settings.ticker){var t="vertical"==o.settings.mode?o.settings.minSlides:o.settings.maxSlides,i=o.children.slice(0,t).clone().addClass("bx-clone"),n=o.children.slice(-t).clone().addClass("bx-clone");r.append(i).prepend(n)}o.active.last=o.settings.startSlide==f()-1,o.settings.video&&r.fitVids();var s=o.children.eq(o.settings.startSlide);"all"==o.settings.preloadImages&&(s=r.children()),o.settings.ticker||(o.settings.pager&&w(),o.settings.controls&&T(),o.settings.auto&&o.settings.autoControls&&C(),(o.settings.controls||o.settings.autoControls||o.settings.pager)&&o.viewport.after(o.controls.el)),s.imagesLoaded(g)},g=function(){o.loader.remove(),m(),"vertical"==o.settings.mode&&(o.settings.adaptiveHeight=!0),o.viewport.height(h()),r.redrawSlider(),o.settings.onSliderLoad(o.active.index),o.initialized=!0,e(window).bind("resize",X),o.settings.auto&&o.settings.autoStart&&L(),o.settings.ticker&&W(),o.settings.pager&&M(o.settings.startSlide),o.settings.controls&&D(),o.settings.touchEnabled&&!o.settings.ticker&&O()},h=function(){var t=0,n=e();if("vertical"==o.settings.mode||o.settings.adaptiveHeight)if(o.carousel){var s=1==o.settings.moveSlides?o.active.index:o.active.index*x();for(n=o.children.eq(s),i=1;o.settings.maxSlides-1>=i;i++)n=s+i>=o.children.length?n.add(o.children.eq(i-1)):n.add(o.children.eq(s+i))}else n=o.children.eq(o.active.index);else n=o.children;return"vertical"==o.settings.mode?(n.each(function(){t+=e(this).outerHeight()}),o.settings.slideMargin>0&&(t+=o.settings.slideMargin*(o.settings.minSlides-1))):t=Math.max.apply(Math,n.map(function(){return e(this).outerHeight(!1)}).get()),t},u=function(){var e="100%";return o.settings.slideWidth>0&&(e="horizontal"==o.settings.mode?o.settings.maxSlides*o.settings.slideWidth+(o.settings.maxSlides-1)*o.settings.slideMargin:o.settings.slideWidth),e},p=function(){var e=o.settings.slideWidth,t=o.viewport.width();return 0==o.settings.slideWidth||o.settings.slideWidth>t&&!o.carousel||"vertical"==o.settings.mode?e=t:o.settings.maxSlides>1&&"horizontal"==o.settings.mode&&(t>o.maxThreshold||o.minThreshold>t&&(e=(t-o.settings.slideMargin*(o.settings.minSlides-1))/o.settings.minSlides)),e},v=function(){var e=1;if("horizontal"==o.settings.mode&&o.settings.slideWidth>0)if(o.viewport.width()<o.minThreshold)e=o.settings.minSlides;else if(o.viewport.width()>o.maxThreshold)e=o.settings.maxSlides;else{var t=o.children.first().width();e=Math.floor(o.viewport.width()/t)}else"vertical"==o.settings.mode&&(e=o.settings.minSlides);return e},f=function(){var e=0;if(o.settings.moveSlides>0)if(o.settings.infiniteLoop)e=o.children.length/x();else for(var t=0,i=0;o.children.length>t;)++e,t=i+v(),i+=o.settings.moveSlides<=v()?o.settings.moveSlides:v();else e=Math.ceil(o.children.length/v());return e},x=function(){return o.settings.moveSlides>0&&o.settings.moveSlides<=v()?o.settings.moveSlides:v()},m=function(){if(o.children.length>o.settings.maxSlides&&o.active.last&&!o.settings.infiniteLoop){if("horizontal"==o.settings.mode){var e=o.children.last(),t=e.position();S(-(t.left-(o.viewport.width()-e.width())),"reset",0)}else if("vertical"==o.settings.mode){var i=o.children.length-o.settings.minSlides,t=o.children.eq(i).position();S(-t.top,"reset",0)}}else{var t=o.children.eq(o.active.index*x()).position();o.active.index==f()-1&&(o.active.last=!0),void 0!=t&&("horizontal"==o.settings.mode?S(-t.left,"reset",0):"vertical"==o.settings.mode&&S(-t.top,"reset",0))}},S=function(e,t,i,n){if(o.usingCSS){var s="vertical"==o.settings.mode?"translate3d(0, "+e+"px, 0)":"translate3d("+e+"px, 0, 0)";r.css("-"+o.cssPrefix+"-transition-duration",i/1e3+"s"),"slide"==t?(r.css(o.animProp,s),r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),I()})):"reset"==t?r.css(o.animProp,s):"ticker"==t&&(r.css("-"+o.cssPrefix+"-transition-timing-function","linear"),r.css(o.animProp,s),r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),S(n.resetValue,"reset",0),H()}))}else{var a={};a[o.animProp]=e,"slide"==t?r.animate(a,i,o.settings.easing,function(){I()}):"reset"==t?r.css(o.animProp,e):"ticker"==t&&r.animate(a,speed,"linear",function(){S(n.resetValue,"reset",0),H()})}},b=function(){var t="";pagerQty=f();for(var i=0;pagerQty>i;i++){var n="";o.settings.buildPager&&e.isFunction(o.settings.buildPager)?(n=o.settings.buildPager(i),o.pagerEl.addClass("bx-custom-pager")):(n=i+1,o.pagerEl.addClass("bx-default-pager")),t+='<div class="bx-pager-item"><a href="" data-slide-index="'+i+'" class="bx-pager-link">'+n+"</a></div>"}o.pagerEl.html(t)},w=function(){o.settings.pagerCustom?o.pagerEl=e(o.settings.pagerCustom):(o.pagerEl=e('<div class="bx-pager" />'),o.settings.pagerSelector?e(o.settings.pagerSelector).html(o.pagerEl):o.controls.el.addClass("bx-has-pager").append(o.pagerEl),b()),o.pagerEl.delegate("a","click",z)},T=function(){o.controls.next=e('<a class="bx-next" href="">'+o.settings.nextText+"</a>"),o.controls.prev=e('<a class="bx-prev" href="">'+o.settings.prevText+"</a>"),o.controls.next.bind("click",A),o.controls.prev.bind("click",P),o.settings.nextSelector&&e(o.settings.nextSelector).append(o.controls.next),o.settings.prevSelector&&e(o.settings.prevSelector).append(o.controls.prev),o.settings.nextSelector||o.settings.prevSelector||(o.controls.directionEl=e('<div class="bx-controls-direction" />'),o.controls.directionEl.append(o.controls.prev).append(o.controls.next),o.controls.el.addClass("bx-has-controls-direction").append(o.controls.directionEl))},C=function(){o.controls.start=e('<div class="bx-controls-auto-item"><a class="bx-start" href="">'+o.settings.startText+"</a></div>"),o.controls.stop=e('<div class="bx-controls-auto-item"><a class="bx-stop" href="">'+o.settings.stopText+"</a></div>"),o.controls.autoEl=e('<div class="bx-controls-auto" />'),o.controls.autoEl.delegate(".bx-start","click",y),o.controls.autoEl.delegate(".bx-stop","click",k),o.settings.autoControlsCombine?o.controls.autoEl.append(o.controls.start):o.controls.autoEl.append(o.controls.start).append(o.controls.stop),o.settings.autoControlsSelector?e(o.settings.autoControlsSelector).html(o.controls.autoEl):o.controls.el.addClass("bx-has-controls-auto").append(o.controls.autoEl),q(o.settings.autoStart?"stop":"start")},E=function(){o.children.each(function(){var t=e(this).find("img:first").attr("title");void 0!=t&&e(this).append('<div class="bx-caption"><span>'+t+"</span></div>")})},A=function(e){o.settings.auto&&r.stopAuto(),r.goToNextSlide(),e.preventDefault()},P=function(e){o.settings.auto&&r.stopAuto(),r.goToPrevSlide(),e.preventDefault()},y=function(e){r.startAuto(),e.preventDefault()},k=function(e){r.stopAuto(),e.preventDefault()},z=function(t){o.settings.auto&&r.stopAuto();var i=e(t.currentTarget),n=parseInt(i.attr("data-slide-index"));n!=o.active.index&&r.goToSlide(n),t.preventDefault()},M=function(t){return"short"==o.settings.pagerType?(o.pagerEl.html(t+1+o.settings.pagerShortSeparator+o.children.length),void 0):(o.pagerEl.find("a").removeClass("active"),o.pagerEl.each(function(i,n){e(n).find("a").eq(t).addClass("active")}),void 0)},I=function(){if(o.settings.infiniteLoop){var e="";0==o.active.index?e=o.children.eq(0).position():o.active.index==f()-1&&o.carousel?e=o.children.eq((f()-1)*x()).position():o.active.index==o.children.length-1&&(e=o.children.eq(o.children.length-1).position()),"horizontal"==o.settings.mode?S(-e.left,"reset",0):"vertical"==o.settings.mode&&S(-e.top,"reset",0)}o.working=!1,o.settings.onSlideAfter(o.children.eq(o.active.index),o.oldIndex,o.active.index)},q=function(e){o.settings.autoControlsCombine?o.controls.autoEl.html(o.controls[e]):(o.controls.autoEl.find("a").removeClass("active"),o.controls.autoEl.find("a:not(.bx-"+e+")").addClass("active"))},D=function(){!o.settings.infiniteLoop&&o.settings.hideControlOnEnd?0==o.active.index?(o.controls.prev.addClass("disabled"),o.controls.next.removeClass("disabled")):o.active.index==f()-1?(o.controls.next.addClass("disabled"),o.controls.prev.removeClass("disabled")):(o.controls.prev.removeClass("disabled"),o.controls.next.removeClass("disabled")):1==f()&&(o.controls.prev.addClass("disabled"),o.controls.next.addClass("disabled"))},L=function(){o.settings.autoDelay>0?setTimeout(r.startAuto,o.settings.autoDelay):r.startAuto(),o.settings.autoHover&&r.hover(function(){o.interval&&(r.stopAuto(!0),o.autoPaused=!0)},function(){o.autoPaused&&(r.startAuto(!0),o.autoPaused=null)})},W=function(){var t=0;if("next"==o.settings.autoDirection)r.append(o.children.clone().addClass("bx-clone"));else{r.prepend(o.children.clone().addClass("bx-clone"));var i=o.children.first().position();t="horizontal"==o.settings.mode?-i.left:-i.top}S(t,"reset",0),o.settings.pager=!1,o.settings.controls=!1,o.settings.autoControls=!1,o.settings.tickerHover&&!o.usingCSS&&o.viewport.hover(function(){r.stop()},function(){var t=0;o.children.each(function(){t+="horizontal"==o.settings.mode?e(this).outerWidth(!0):e(this).outerHeight(!0)});var i=o.settings.speed/t,n="horizontal"==o.settings.mode?"left":"top",s=i*(t-Math.abs(parseInt(r.css(n))));H(s)}),H()},H=function(e){speed=e?e:o.settings.speed;var t={left:0,top:0},i={left:0,top:0};"next"==o.settings.autoDirection?t=r.find(".bx-clone").first().position():i=o.children.first().position();var n="horizontal"==o.settings.mode?-t.left:-t.top,s="horizontal"==o.settings.mode?-i.left:-i.top,a={resetValue:s};S(n,"ticker",speed,a)},O=function(){o.touch={start:{x:0,y:0},end:{x:0,y:0}},o.viewport.bind("touchstart",N)},N=function(e){if(o.working)e.preventDefault();else{o.touch.originalPos=r.position();var t=e.originalEvent;o.touch.start.x=t.changedTouches[0].pageX,o.touch.start.y=t.changedTouches[0].pageY,o.viewport.bind("touchmove",B),o.viewport.bind("touchend",Q)}},B=function(e){var t=e.originalEvent,i=Math.abs(t.changedTouches[0].pageX-o.touch.start.x),n=Math.abs(t.changedTouches[0].pageY-o.touch.start.y);if(3*i>n&&o.settings.preventDefaultSwipeX?e.preventDefault():3*n>i&&o.settings.preventDefaultSwipeY&&e.preventDefault(),"fade"!=o.settings.mode&&o.settings.oneToOneTouch){var s=0;if("horizontal"==o.settings.mode){var r=t.changedTouches[0].pageX-o.touch.start.x;s=o.touch.originalPos.left+r}else{var r=t.changedTouches[0].pageY-o.touch.start.y;s=o.touch.originalPos.top+r}S(s,"reset",0)}},Q=function(e){o.viewport.unbind("touchmove",B);var t=e.originalEvent,i=0;if(o.touch.end.x=t.changedTouches[0].pageX,o.touch.end.y=t.changedTouches[0].pageY,"fade"==o.settings.mode){var n=Math.abs(o.touch.start.x-o.touch.end.x);n>=o.settings.swipeThreshold&&(o.touch.start.x>o.touch.end.x?r.goToNextSlide():r.goToPrevSlide(),r.stopAuto())}else{var n=0;"horizontal"==o.settings.mode?(n=o.touch.end.x-o.touch.start.x,i=o.touch.originalPos.left):(n=o.touch.end.y-o.touch.start.y,i=o.touch.originalPos.top),!o.settings.infiniteLoop&&(0==o.active.index&&n>0||o.active.last&&0>n)?S(i,"reset",200):Math.abs(n)>=o.settings.swipeThreshold?(0>n?r.goToNextSlide():r.goToPrevSlide(),r.stopAuto()):S(i,"reset",200)}o.viewport.unbind("touchend",Q)},X=function(){var t=e(window).width(),i=e(window).height();(a!=t||l!=i)&&(a=t,l=i,r.redrawSlider())};return r.goToSlide=function(t,i){if(!o.working&&o.active.index!=t)if(o.working=!0,o.oldIndex=o.active.index,o.active.index=0>t?f()-1:t>=f()?0:t,o.settings.onSlideBefore(o.children.eq(o.active.index),o.oldIndex,o.active.index),"next"==i?o.settings.onSlideNext(o.children.eq(o.active.index),o.oldIndex,o.active.index):"prev"==i&&o.settings.onSlidePrev(o.children.eq(o.active.index),o.oldIndex,o.active.index),o.active.last=o.active.index>=f()-1,o.settings.pager&&M(o.active.index),o.settings.controls&&D(),"fade"==o.settings.mode)o.settings.adaptiveHeight&&o.viewport.height()!=h()&&o.viewport.animate({height:h()},o.settings.adaptiveHeightSpeed),o.children.filter(":visible").fadeOut(o.settings.speed).css({zIndex:0}),o.children.eq(o.active.index).css("zIndex",51).fadeIn(o.settings.speed,function(){e(this).css("zIndex",50),I()});else{o.settings.adaptiveHeight&&o.viewport.height()!=h()&&o.viewport.animate({height:h()},o.settings.adaptiveHeightSpeed);var n=0,s={left:0,top:0};if(!o.settings.infiniteLoop&&o.carousel&&o.active.last)if("horizontal"==o.settings.mode){var a=o.children.eq(o.children.length-1);s=a.position(),n=o.viewport.width()-a.width()}else{var l=o.children.length-o.settings.minSlides;s=o.children.eq(l).position()}else if(o.carousel&&o.active.last&&"prev"==i){var d=1==o.settings.moveSlides?o.settings.maxSlides-x():(f()-1)*x()-(o.children.length-o.settings.maxSlides),a=r.children(".bx-clone").eq(d);s=a.position()}else if("next"==i&&0==o.active.index)s=r.find(".bx-clone").eq(o.settings.maxSlides).position(),o.active.last=!1;else if(t>=0){var c=t*x();s=o.children.eq(c).position()}var g="horizontal"==o.settings.mode?-(s.left-n):-s.top;S(g,"slide",o.settings.speed)}},r.goToNextSlide=function(){if(o.settings.infiniteLoop||!o.active.last){var e=parseInt(o.active.index)+1;r.goToSlide(e,"next")}},r.goToPrevSlide=function(){if(o.settings.infiniteLoop||0!=o.active.index){var e=parseInt(o.active.index)-1;r.goToSlide(e,"prev")}},r.startAuto=function(e){o.interval||(o.interval=setInterval(function(){"next"==o.settings.autoDirection?r.goToNextSlide():r.goToPrevSlide()},o.settings.pause),o.settings.autoControls&&1!=e&&q("stop"))},r.stopAuto=function(e){o.interval&&(clearInterval(o.interval),o.interval=null,o.settings.autoControls&&1!=e&&q("start"))},r.getCurrentSlide=function(){return o.active.index},r.getSlideCount=function(){return o.children.length},r.redrawSlider=function(){o.children.add(r.find(".bx-clone")).width(p()),o.viewport.css("height",h()),o.settings.ticker||m(),o.active.last&&(o.active.index=f()-1),o.active.index>=f()&&(o.active.last=!0),o.settings.pager&&!o.settings.pagerCustom&&(b(),M(o.active.index))},r.destroySlider=function(){o.initialized&&(o.initialized=!1,e(".bx-clone",this).remove(),o.children.removeAttr("style"),this.removeAttr("style").unwrap().unwrap(),o.controls.el&&o.controls.el.remove(),o.controls.next&&o.controls.next.remove(),o.controls.prev&&o.controls.prev.remove(),o.pagerEl&&o.pagerEl.remove(),e(".bx-caption",this).remove(),o.controls.autoEl&&o.controls.autoEl.remove(),clearInterval(o.interval),e(window).unbind("resize",X))},r.reloadSlider=function(e){void 0!=e&&(s=e),r.destroySlider(),d()},d(),this}}})(jQuery),function(e,t){var i="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";e.fn.imagesLoaded=function(n){function s(){var t=e(g),i=e(h);a&&(h.length?a.reject(d,t,i):a.resolve(d)),e.isFunction(n)&&n.call(r,d,t,i)}function o(t,n){t.src===i||-1!==e.inArray(t,c)||(c.push(t),n?h.push(t):g.push(t),e.data(t,"imagesLoaded",{isBroken:n,src:t.src}),l&&a.notifyWith(e(t),[n,d,e(g),e(h)]),d.length===c.length&&(setTimeout(s),d.unbind(".imagesLoaded")))}var r=this,a=e.isFunction(e.Deferred)?e.Deferred():0,l=e.isFunction(a.notify),d=r.find("img").add(r.filter("img")),c=[],g=[],h=[];return e.isPlainObject(n)&&e.each(n,function(e,t){"callback"===e?n=t:a&&a[e](t)}),d.length?d.bind("load.imagesLoaded error.imagesLoaded",function(e){o(e.target,"error"===e.type)}).each(function(n,s){var r=s.src,a=e.data(s,"imagesLoaded");a&&a.src===r?o(s,a.isBroken):s.complete&&s.naturalWidth!==t?o(s,0===s.naturalWidth||0===s.naturalHeight):(s.readyState||s.complete)&&(s.src=i,s.src=r)}):s(),a?a.promise(r):r}}(jQuery);

/*! Backstretch - v2.0.0 - 2012-09-05
* http://srobbin.com/jquery-plugins/backstretch/
* Copyright (c) 2012 Scott Robbin; Licensed MIT */

;(function ($, window, undefined) {
  'use strict';

  /* PLUGIN DEFINITION
   * ========================= */

  $.fn.backstretch = function (images, options) {
    /*
     * Scroll the page one pixel to get the right window height on iOS
     * Pretty harmless for everyone else
    */
    if ($(window).scrollTop() === 0 ) {
      window.scrollTo(0, 0);
    }

    return this.each(function () {
      var $this = $(this)
        , obj = $this.data('backstretch');

      // If we've already attached Backstretch to this element, remove the old instance.
      if (obj) {
        // Merge the old options with the new
        options = $.extend(obj.options, options);

        // Remove the old instance
        obj.destroy(true);
      }

      obj = new Backstretch(this, images, options);
      $this.data('backstretch', obj);
    });
  };

  // If no element is supplied, we'll attach to body
  $.backstretch = function (images, options) {
    // Return the instance
    return $('body')
            .backstretch(images, options)
            .data('backstretch');
  };

  /* DEFAULTS
   * ========================= */

  $.fn.backstretch.defaults = {
      centeredX: true   // Should we center the image on the X axis?
    , centeredY: true   // Should we center the image on the Y axis?
    , duration: 5000    // Amount of time in between slides (if slideshow)
    , fade: 'fast'      // Speed of fade transition between slides
  };

  /* STYLES
   * 
   * Baked-in styles that we'll apply to our elements.
   * In an effort to keep the plugin simple, these are not exposed as options.
   * That said, anyone can override these in their own stylesheet.
   * ========================= */
  var styles = {
      wrap: {
          left: 0
        , top: 0
        , overflow: 'hidden'
        , margin: 0
        , padding: 0
        , height: '100%'
        , width: '100%'
        , zIndex: -999999
      }
    , img: {
          position: 'absolute'
        , display: 'none'
        , margin: 0
        , padding: 0
        , border: 'none'
        , width: 'auto'
        , height: 'auto'
        , maxWidth: 'none'
        , zIndex: -999999
      }
  };

  /* CLASS DEFINITION
   * ========================= */
  var Backstretch = function (container, images, options) {
    this.options = $.extend({}, $.fn.backstretch.defaults, options || {});

    /* In its simplest form, we allow Backstretch to be called on an image path.
     * e.g. $.backstretch('/path/to/image.jpg')
     * So, we need to turn this back into an array.
     */
    this.images = $.isArray(images) ? images : [images];

    // Preload images
    $.each(this.images, function () {
      $('<img />')[0].src = this;
    });    

    // Convenience reference to know if the container is body.
    this.isBody = container === document.body;

    /* We're keeping track of a few different elements
     *
     * Container: the element that Backstretch was called on.
     * Wrap: a DIV that we place the image into, so we can hide the overflow.
     * Root: Convenience reference to help calculate the correct height.
     */
    this.$container = $(container);
    this.$wrap = $('<div class="backstretch"></div>').css(styles.wrap).appendTo(this.$container);
    this.$root = this.isBody ? supportsFixedPosition ? $(window) : $(document) : this.$container;

    // Non-body elements need some style adjustments
    if (!this.isBody) {
      // If the container is statically positioned, we need to make it relative,
      // and if no zIndex is defined, we should set it to zero.
      var position = this.$container.css('position')
        , zIndex = this.$container.css('zIndex');

      this.$container.css({
          position: position === 'static' ? 'relative' : position
        , zIndex: zIndex === 'auto' ? 0 : zIndex
        , background: 'none'
      });
      
      // Needs a higher z-index
      this.$wrap.css({zIndex: -999998});
    }

    // Fixed or absolute positioning?
    this.$wrap.css({
      position: this.isBody && supportsFixedPosition ? 'fixed' : 'absolute'
    });

    // Set the first image
    this.index = 0;
    this.show(this.index);

    // Listen for resize
    $(window).on('resize.backstretch', $.proxy(this.resize, this))
             .on('orientationchange.backstretch', $.proxy(function () {
                // Need to do this in order to get the right window height
                if (this.isBody && window.pageYOffset === 0) {
                  window.scrollTo(0, 1);
                  this.resize();
                }
             }, this));
  };

  /* PUBLIC METHODS
   * ========================= */
  Backstretch.prototype = {
      resize: function () {
        try {
          var bgCSS = {left: 0, top: 0}
            , rootWidth = this.isBody ? this.$root.width() : this.$root.innerWidth()
            , bgWidth = rootWidth
            , rootHeight = this.isBody ? ( window.innerHeight ? window.innerHeight : this.$root.height() ) : this.$root.innerHeight()
            , bgHeight = bgWidth / this.$img.data('ratio')
            , bgOffset;

            // Make adjustments based on image ratio
            if (bgHeight >= rootHeight) {
                bgOffset = (bgHeight - rootHeight) / 2;
                if(this.options.centeredY) {
                  bgCSS.top = '-' + bgOffset + 'px';
                }
            } else {
                bgHeight = rootHeight;
                bgWidth = bgHeight * this.$img.data('ratio');
                bgOffset = (bgWidth - rootWidth) / 2;
                if(this.options.centeredX) {
                  bgCSS.left = '-' + bgOffset + 'px';
                }
            }

            this.$wrap.css({width: rootWidth, height: rootHeight})
                      .find('img:not(.deleteable)').css({width: bgWidth, height: bgHeight}).css(bgCSS);
        } catch(err) {
            // IE7 seems to trigger resize before the image is loaded.
            // This try/catch block is a hack to let it fail gracefully.
        }

        return this;
      }

      // Show the slide at a certain position
    , show: function (index) {
        // Validate index
        if (Math.abs(index) > this.images.length - 1) {
          return;
        } else {
          this.index = index;
        }

        // Vars
        var self = this
          , oldImage = self.$wrap.find('img').addClass('deleteable')
          , evt = $.Event('backstretch.show', {
              relatedTarget: self.$container[0]
            });

        // Pause the slideshow
        clearInterval(self.interval);

        // New image
        self.$img = $('<img />')
                      .css(styles.img)
                      .bind('load', function (e) {
                        var imgWidth = this.width || $(e.target).width()
                          , imgHeight = this.height || $(e.target).height();
                        
                        // Save the ratio
                        $(this).data('ratio', imgWidth / imgHeight);

                        // Resize
                        self.resize();

                        // Show the image, then delete the old one
                        $(this).fadeIn(self.options.fade, function () {
                          oldImage.remove();

                          // Resume the slideshow
                          if (!self.paused) {
                            self.cycle();
                          }

                          // Trigger the event
                          self.$container.trigger(evt);
                        });
                      })
                      .appendTo(self.$wrap);

        // Hack for IE img onload event
        self.$img.attr('src', self.images[index]);
        return self;
      }

    , next: function () {
        // Next slide
        return this.show(this.index < this.images.length - 1 ? this.index + 1 : 0);
      }

    , prev: function () {
        // Previous slide
        return this.show(this.index === 0 ? this.images.length - 1 : this.index - 1);
      }

    , pause: function () {
        // Pause the slideshow
        this.paused = true;
        return this;
      }

    , resume: function () {
        // Resume the slideshow
        this.paused = false;
        this.next();
        return this;
      }

    , cycle: function () {
        // Start/resume the slideshow
        if(this.images.length > 1) {
          clearInterval(this.interval);
          this.interval = setInterval($.proxy(function () {
            // Check for paused slideshow
            if (!this.paused) {
              this.next();
            }
          }, this), this.options.duration);
        }
        return this;
      }

    , destroy: function (preserveBackground) {
        // Stop the resize events
        $(window).off('resize.backstretch orientationchange.backstretch');

        // Clear the interval
        clearInterval(this.interval);

        // Remove Backstretch
        if(!preserveBackground) {
          this.$wrap.remove();          
        }
        this.$container.removeData('backstretch');
      }
  };

  /* SUPPORTS FIXED POSITION?
   *
   * Based on code from jQuery Mobile 1.1.0
   * http://jquerymobile.com/
   *
   * In a nutshell, we need to figure out if fixed positioning is supported.
   * Unfortunately, this is very difficult to do on iOS, and usually involves
   * injecting content, scrolling the page, etc.. It's ugly.
   * jQuery Mobile uses this workaround. It's not ideal, but works.
   *
   * Modified to detect IE6
   * ========================= */

  var supportsFixedPosition = (function () {
    var ua = navigator.userAgent
      , platform = navigator.platform
        // Rendering engine is Webkit, and capture major version
      , wkmatch = ua.match( /AppleWebKit\/([0-9]+)/ )
      , wkversion = !!wkmatch && wkmatch[ 1 ]
      , ffmatch = ua.match( /Fennec\/([0-9]+)/ )
      , ffversion = !!ffmatch && ffmatch[ 1 ]
      , operammobilematch = ua.match( /Opera Mobi\/([0-9]+)/ )
      , omversion = !!operammobilematch && operammobilematch[ 1 ]
      , iematch = ua.match( /MSIE ([0-9]+)/ )
      , ieversion = !!iematch && iematch[ 1 ];

    return !(
      // iOS 4.3 and older : Platform is iPhone/Pad/Touch and Webkit version is less than 534 (ios5)
      ((platform.indexOf( "iPhone" ) > -1 || platform.indexOf( "iPad" ) > -1  || platform.indexOf( "iPod" ) > -1 ) && wkversion && wkversion < 534) ||
      
      // Opera Mini
      (window.operamini && ({}).toString.call( window.operamini ) === "[object OperaMini]") ||
      (operammobilematch && omversion < 7458) ||
      
      //Android lte 2.1: Platform is Android and Webkit version is less than 533 (Android 2.2)
      (ua.indexOf( "Android" ) > -1 && wkversion && wkversion < 533) ||
      
      // Firefox Mobile before 6.0 -
      (ffversion && ffversion < 6) ||
      
      // WebOS less than 3
      ("palmGetResource" in window && wkversion && wkversion < 534) ||
      
      // MeeGo
      (ua.indexOf( "MeeGo" ) > -1 && ua.indexOf( "NokiaBrowser/8.5.0" ) > -1) ||
      
      // IE6
      (ieversion && ieversion <= 6)
    );
  }());

}(jQuery, window));

jQuery.fn.topLink = function(settings) {
        settings = jQuery.extend({
            min: 1,
            fadeSpeed: 200,
            ieOffset: 50
        }, settings);
        return this.each(function() {
            //listen for scroll
            var el = $(this);
            el.css('display','none'); //in case the user forgot
            $(window).scroll(function() {
                //stupid IE hack
                if(!jQuery.support.hrefNormalized) {
                    el.css({
                        'position': 'absolute',
                        'top': $(window).scrollTop() + $(window).height() - settings.ieOffset
                    });
                }
                if($(window).scrollTop() >= settings.min)
                {
                    el.fadeIn(settings.fadeSpeed);
                }
                else
                {
                    el.fadeOut(settings.fadeSpeed);
                }
            });
        });
    };

    (function(){var p,t=Object.prototype.toString;Array.isArray=Array.isArray||function(a){return"[object Array]"==t.call(a)};var q=String.prototype.trim,g;if(q)g=function(a){return null==a?"":q.call(a)};else{var h,m;/\S/.test("\u00a0")?(h=/^[\s\xA0]+/,m=/[\s\xA0]+$/):(h=/^\s+/,m=/\s+$/);g=function(a){return null==a?"":a.toString().replace(h,"").replace(m,"")}}var u={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#39;"},r={},s=function(){};s.prototype={otag:"{{",ctag:"}}",pragmas:{},buffer:[],pragmas_implemented:{"IMPLICIT-ITERATOR":!0},
context:{},render:function(a,b,c,d){d||(this.context=b,this.buffer=[]);if(this.includes("",a)){a=this.render_pragmas(a);var e=this.render_section(a,b,c);!1===e&&(e=this.render_tags(a,b,c,d));if(d)return e;this.sendLines(e)}else{if(d)return a;this.send(a)}},send:function(a){""!==a&&this.buffer.push(a)},sendLines:function(a){if(a){a=a.split("\n");for(var b=0;b<a.length;b++)this.send(a[b])}},render_pragmas:function(a){if(!this.includes("%",a))return a;var b=this,c=this.getCachedRegex("render_pragmas",
function(a,b){return RegExp(a+"%([\\w-]+) ?([\\w]+=[\\w]+)?"+b,"g")});return a.replace(c,function(a,c,j){if(!b.pragmas_implemented[c])throw{message:"This implementation of mustache doesn't understand the '"+c+"' pragma"};b.pragmas[c]={};j&&(a=j.split("="),b.pragmas[c][a[0]]=a[1]);return""})},render_partial:function(a,b,c){a=g(a);if(!c||void 0===c[a])throw{message:"unknown_partial '"+a+"'"};return!b||"object"!=typeof b[a]?this.render(c[a],b,c,!0):this.render(c[a],b[a],c,!0)},render_section:function(a,
b,c){if(!this.includes("#",a)&&!this.includes("^",a))return!1;var d=this,e=this.getCachedRegex("render_section",function(a,b){return RegExp("^([\\s\\S]*?)"+a+"(\\^|\\#)\\s*(.+)\\s*"+b+"\n*([\\s\\S]*?)"+a+"\\/\\s*\\3\\s*"+b+"\\s*([\\s\\S]*)$","g")});return a.replace(e,function(a,e,g,f,k,l){a=e?d.render_tags(e,b,c,!0):"";l=l?d.render(l,b,c,!0):"";var n;f=d.find(f,b);"^"===g?n=!f||Array.isArray(f)&&0===f.length?d.render(k,b,c,!0):"":"#"===g&&(n=Array.isArray(f)?d.map(f,function(a){return d.render(k,
d.create_context(a),c,!0)}).join(""):d.is_object(f)?d.render(k,d.create_context(f),c,!0):"function"==typeof f?f.call(b,k,function(a){return d.render(a,b,c,!0)}):f?d.render(k,b,c,!0):"");return a+n+l})},render_tags:function(a,b,c,d){var e=this,j=function(){return e.getCachedRegex("render_tags",function(a,b){return RegExp(a+"(=|!|>|&|\\{|%)?([^#\\^]+?)\\1?"+b+"+","g")})},g=j(),h=function(a,d,f){switch(d){case "!":return"";case "=":return e.set_delimiters(f),g=j(),"";case ">":return e.render_partial(f,
b,c);case "{":case "&":return e.find(f,b);default:return a=e.find(f,b),String(a).replace(/&(?!\w+;)|[<>"']/g,function(a){return u[a]||a})}};a=a.split("\n");for(var f=0;f<a.length;f++)a[f]=a[f].replace(g,h,this),d||this.send(a[f]);if(d)return a.join("\n")},set_delimiters:function(a){a=a.split(" ");this.otag=this.escape_regex(a[0]);this.ctag=this.escape_regex(a[1])},escape_regex:function(a){arguments.callee.sRE||(arguments.callee.sRE=RegExp("(\\/|\\.|\\*|\\+|\\?|\\||\\(|\\)|\\[|\\]|\\{|\\}|\\\\)","g"));
return a.replace(arguments.callee.sRE,"\\$1")},find:function(a,b){a=g(a);var c;if(a.match(/([a-z_]+)\./ig)){var d=this.walk_context(a,b);(!1===d||0===d||d)&&(c=d)}else!1===b[a]||0===b[a]||b[a]?c=b[a]:(!1===this.context[a]||0===this.context[a]||this.context[a])&&(c=this.context[a]);return"function"==typeof c?c.apply(b):void 0!==c?c:""},walk_context:function(a,b){for(var c=a.split("."),d=void 0!=b[c[0]]?b:this.context,e=d[c.shift()];void 0!=e&&0<c.length;)d=e,e=e[c.shift()];return"function"==typeof e?
e.apply(d):e},includes:function(a,b){return-1!=b.indexOf(this.otag+a)},create_context:function(a){if(this.is_object(a))return a;var b=".";this.pragmas["IMPLICIT-ITERATOR"]&&(b=this.pragmas["IMPLICIT-ITERATOR"].iterator);var c={};c[b]=a;return c},is_object:function(a){return a&&"object"==typeof a},map:function(a,b){if("function"==typeof a.map)return a.map(b);for(var c=[],d=a.length,e=0;e<d;e++)c.push(b(a[e]));return c},getCachedRegex:function(a,b){var c=r[this.otag];c||(c=r[this.otag]={});var d=c[this.ctag];
d||(d=c[this.ctag]={});(c=d[a])||(c=d[a]=b(this.otag,this.ctag));return c}};p={name:"mustache.js",version:"0.4.0",to_html:function(a,b,c,d){var e=new s;d&&(e.send=d);e.render(a,b||{},c);if(!d)return e.buffer.join("\n")}};(function(){var a={VERSION:"0.10.2",templates:{},$:"undefined"!==typeof window?window.jQuery||window.Zepto||null:null,addTemplate:function(b,c){if("object"===typeof b)for(var d in b)this.addTemplate(d,b[d]);else a[b]?console.error("Invalid name: "+b+"."):a.templates[b]?console.error('Template "'+
b+'  " exists'):(a.templates[b]=c,a[b]=function(c,d){c=c||{};var g=p.to_html(a.templates[b],c,a.templates);return a.$&&!d?a.$(g):g})},clearAll:function(){for(var b in a.templates)delete a[b];a.templates={}},refresh:function(){a.clearAll();a.grabTemplates()},grabTemplates:function(){var b,c,d=document.getElementsByTagName("script"),e,g=[];b=0;for(c=d.length;b<c;b++)if((e=d[b])&&e.innerHTML&&e.id&&("text/html"===e.type||"text/x-icanhaz"===e.type))a.addTemplate(e.id,"".trim?e.innerHTML.trim():e.innerHTML.replace(/^\s+/,
"").replace(/\s+$/,"")),g.unshift(e);b=0;for(c=g.length;b<c;b++)g[b].parentNode.removeChild(g[b])}};"undefined"!==typeof exports?("undefined"!==typeof module&&module.exports&&(exports=module.exports=a),exports.ich=a):this.ich=a;"undefined"!==typeof document&&(a.$?a.$(function(){a.grabTemplates()}):document.addEventListener("DOMContentLoaded",function(){a.grabTemplates()},!0))})()})();

/*
 *  jQuery carouFredSel 6.2.1
 *  Demo's and documentation:
 *  caroufredsel.dev7studios.com
 *
 *  Copyright (c) 2013 Fred Heusschen
 *  www.frebsite.nl
 *
 *  Dual licensed under the MIT and GPL licenses.
 *  http://en.wikipedia.org/wiki/MIT_License
 *  http://en.wikipedia.org/wiki/GNU_General_Public_License
 */


(function($) {


    //  LOCAL

    if ( $.fn.carouFredSel )
    {
        return;
    }

    $.fn.caroufredsel = $.fn.carouFredSel = function(options, configs)
    {

        //  no element
        if (this.length == 0)
        {
            debug( true, 'No element found for "' + this.selector + '".' );
            return this;
        }

        //  multiple elements
        if (this.length > 1)
        {
            return this.each(function() {
                $(this).carouFredSel(options, configs);
            });
        }


        var $cfs = this,
            $tt0 = this[0],
            starting_position = false;

        if ($cfs.data('_cfs_isCarousel'))
        {
            starting_position = $cfs.triggerHandler('_cfs_triggerEvent', 'currentPosition');
            $cfs.trigger('_cfs_triggerEvent', ['destroy', true]);
        }

        var FN = {};

        FN._init = function(o, setOrig, start)
        {
            o = go_getObject($tt0, o);

            o.items = go_getItemsObject($tt0, o.items);
            o.scroll = go_getScrollObject($tt0, o.scroll);
            o.auto = go_getAutoObject($tt0, o.auto);
            o.prev = go_getPrevNextObject($tt0, o.prev);
            o.next = go_getPrevNextObject($tt0, o.next);
            o.pagination = go_getPaginationObject($tt0, o.pagination);
            o.swipe = go_getSwipeObject($tt0, o.swipe);
            o.mousewheel = go_getMousewheelObject($tt0, o.mousewheel);

            if (setOrig)
            {
                opts_orig = $.extend(true, {}, $.fn.carouFredSel.defaults, o);
            }

            opts = $.extend(true, {}, $.fn.carouFredSel.defaults, o);
            opts.d = cf_getDimensions(opts);

            crsl.direction = (opts.direction == 'up' || opts.direction == 'left') ? 'next' : 'prev';

            var a_itm = $cfs.children(),
                avail_primary = ms_getParentSize($wrp, opts, 'width');

            if (is_true(opts.cookie))
            {
                opts.cookie = 'caroufredsel_cookie_' + conf.serialNumber;
            }

            opts.maxDimension = ms_getMaxDimension(opts, avail_primary);

            //  complement items and sizes
            opts.items = in_complementItems(opts.items, opts, a_itm, start);
            opts[opts.d['width']] = in_complementPrimarySize(opts[opts.d['width']], opts, a_itm);
            opts[opts.d['height']] = in_complementSecondarySize(opts[opts.d['height']], opts, a_itm);

            //  primary size not set for a responsive carousel
            if (opts.responsive)
            {
                if (!is_percentage(opts[opts.d['width']]))
                {
                    opts[opts.d['width']] = '100%';
                }
            }

            //  primary size is percentage
            if (is_percentage(opts[opts.d['width']]))
            {
                crsl.upDateOnWindowResize = true;
                crsl.primarySizePercentage = opts[opts.d['width']];
                opts[opts.d['width']] = ms_getPercentage(avail_primary, crsl.primarySizePercentage);
                if (!opts.items.visible)
                {
                    opts.items.visibleConf.variable = true;
                }
            }

            if (opts.responsive)
            {
                opts.usePadding = false;
                opts.padding = [0, 0, 0, 0];
                opts.align = false;
                opts.items.visibleConf.variable = false;
            }
            else
            {
                //  visible-items not set
                if (!opts.items.visible)
                {
                    opts = in_complementVisibleItems(opts, avail_primary);
                }

                //  primary size not set -> calculate it or set to "variable"
                if (!opts[opts.d['width']])
                {
                    if (!opts.items.visibleConf.variable && is_number(opts.items[opts.d['width']]) && opts.items.filter == '*')
                    {
                        opts[opts.d['width']] = opts.items.visible * opts.items[opts.d['width']];
                        opts.align = false;
                    }
                    else
                    {
                        opts[opts.d['width']] = 'variable';
                    }
                }
                //  align not set -> set to center if primary size is number
                if (is_undefined(opts.align))
                {
                    opts.align = (is_number(opts[opts.d['width']]))
                        ? 'center'
                        : false;
                }
                //  set variabe visible-items
                if (opts.items.visibleConf.variable)
                {
                    opts.items.visible = gn_getVisibleItemsNext(a_itm, opts, 0);
                }
            }

            //  set visible items by filter
            if (opts.items.filter != '*' && !opts.items.visibleConf.variable)
            {
                opts.items.visibleConf.org = opts.items.visible;
                opts.items.visible = gn_getVisibleItemsNextFilter(a_itm, opts, 0);
            }

            opts.items.visible = cf_getItemsAdjust(opts.items.visible, opts, opts.items.visibleConf.adjust, $tt0);
            opts.items.visibleConf.old = opts.items.visible;

            if (opts.responsive)
            {
                if (!opts.items.visibleConf.min)
                {
                    opts.items.visibleConf.min = opts.items.visible;
                }
                if (!opts.items.visibleConf.max)
                {
                    opts.items.visibleConf.max = opts.items.visible;
                }
                opts = in_getResponsiveValues(opts, a_itm, avail_primary);
            }
            else
            {
                opts.padding = cf_getPadding(opts.padding);

                if (opts.align == 'top')
                {
                    opts.align = 'left';
                }
                else if (opts.align == 'bottom')
                {
                    opts.align = 'right';
                }

                switch (opts.align)
                {
                    //  align: center, left or right
                    case 'center':
                    case 'left':
                    case 'right':
                        if (opts[opts.d['width']] != 'variable')
                        {
                            opts = in_getAlignPadding(opts, a_itm);
                            opts.usePadding = true;
                        }
                        break;

                    //  padding
                    default:
                        opts.align = false;
                        opts.usePadding = (
                            opts.padding[0] == 0 && 
                            opts.padding[1] == 0 && 
                            opts.padding[2] == 0 && 
                            opts.padding[3] == 0
                        ) ? false : true;
                        break;
                }
            }

            if (!is_number(opts.scroll.duration))
            {
                opts.scroll.duration = 500;
            }
            if (is_undefined(opts.scroll.items))
            {
                opts.scroll.items = (opts.responsive || opts.items.visibleConf.variable || opts.items.filter != '*') 
                    ? 'visible'
                    : opts.items.visible;
            }

            opts.auto = $.extend(true, {}, opts.scroll, opts.auto);
            opts.prev = $.extend(true, {}, opts.scroll, opts.prev);
            opts.next = $.extend(true, {}, opts.scroll, opts.next);
            opts.pagination = $.extend(true, {}, opts.scroll, opts.pagination);
            //  swipe and mousewheel extend later on, per direction

            opts.auto = go_complementAutoObject($tt0, opts.auto);
            opts.prev = go_complementPrevNextObject($tt0, opts.prev);
            opts.next = go_complementPrevNextObject($tt0, opts.next);
            opts.pagination = go_complementPaginationObject($tt0, opts.pagination);
            opts.swipe = go_complementSwipeObject($tt0, opts.swipe);
            opts.mousewheel = go_complementMousewheelObject($tt0, opts.mousewheel);

            if (opts.synchronise)
            {
                opts.synchronise = cf_getSynchArr(opts.synchronise);
            }


            //  DEPRECATED
            if (opts.auto.onPauseStart)
            {
                opts.auto.onTimeoutStart = opts.auto.onPauseStart;
                deprecated('auto.onPauseStart', 'auto.onTimeoutStart');
            }
            if (opts.auto.onPausePause)
            {
                opts.auto.onTimeoutPause = opts.auto.onPausePause;
                deprecated('auto.onPausePause', 'auto.onTimeoutPause');
            }
            if (opts.auto.onPauseEnd)
            {
                opts.auto.onTimeoutEnd = opts.auto.onPauseEnd;
                deprecated('auto.onPauseEnd', 'auto.onTimeoutEnd');
            }
            if (opts.auto.pauseDuration)
            {
                opts.auto.timeoutDuration = opts.auto.pauseDuration;
                deprecated('auto.pauseDuration', 'auto.timeoutDuration');
            }
            //  /DEPRECATED


        };  //  /init


        FN._build = function() {
            $cfs.data('_cfs_isCarousel', true);

            var a_itm = $cfs.children(),
                orgCSS = in_mapCss($cfs, ['textAlign', 'float', 'position', 'top', 'right', 'bottom', 'left', 'zIndex', 'width', 'height', 'marginTop', 'marginRight', 'marginBottom', 'marginLeft']),
                newPosition = 'relative';

            switch (orgCSS.position)
            {
                case 'absolute':
                case 'fixed':
                    newPosition = orgCSS.position;
                    break;
            }

            if (conf.wrapper == 'parent')
            {
                sz_storeOrigCss($wrp);
            }
            else
            {
                $wrp.css(orgCSS);
            }
            $wrp.css({
                'overflow'      : 'hidden',
                'position'      : newPosition
            });

            sz_storeOrigCss($cfs);
            $cfs.data('_cfs_origCssZindex', orgCSS.zIndex);
            $cfs.css({
                'textAlign'     : 'left',
                'float'         : 'none',
                'position'      : 'absolute',
                'top'           : 0,
                'right'         : 'auto',
                'bottom'        : 'auto',
                'left'          : 0,
                'marginTop'     : 0,
                'marginRight'   : 0,
                'marginBottom'  : 0,
                'marginLeft'    : 0
            });

            sz_storeMargin(a_itm, opts);
            sz_storeOrigCss(a_itm);
            if (opts.responsive)
            {
                sz_setResponsiveSizes(opts, a_itm);
            }

        };  //  /build


        FN._bind_events = function() {
            FN._unbind_events();


            //  stop event
            $cfs.bind(cf_e('stop', conf), function(e, imm) {
                e.stopPropagation();

                //  button
                if (!crsl.isStopped)
                {
                    if (opts.auto.button)
                    {
                        opts.auto.button.addClass(cf_c('stopped', conf));
                    }
                }

                //  set stopped
                crsl.isStopped = true;

                if (opts.auto.play)
                {
                    opts.auto.play = false;
                    $cfs.trigger(cf_e('pause', conf), imm);
                }
                return true;
            });


            //  finish event
            $cfs.bind(cf_e('finish', conf), function(e) {
                e.stopPropagation();
                if (crsl.isScrolling)
                {
                    sc_stopScroll(scrl);
                }
                return true;
            });


            //  pause event
            $cfs.bind(cf_e('pause', conf), function(e, imm, res) {
                e.stopPropagation();
                tmrs = sc_clearTimers(tmrs);

                //  immediately pause
                if (imm && crsl.isScrolling)
                {
                    scrl.isStopped = true;
                    var nst = getTime() - scrl.startTime;
                    scrl.duration -= nst;
                    if (scrl.pre)
                    {
                        scrl.pre.duration -= nst;
                    }
                    if (scrl.post)
                    {
                        scrl.post.duration -= nst;
                    }
                    sc_stopScroll(scrl, false);
                }

                //  update remaining pause-time
                if (!crsl.isPaused && !crsl.isScrolling)
                {
                    if (res)
                    {
                        tmrs.timePassed += getTime() - tmrs.startTime;
                    }
                }

                //  button
                if (!crsl.isPaused)
                {
                    if (opts.auto.button)
                    {
                        opts.auto.button.addClass(cf_c('paused', conf));
                    }
                }

                //  set paused
                crsl.isPaused = true;

                //  pause pause callback
                if (opts.auto.onTimeoutPause)
                {
                    var dur1 = opts.auto.timeoutDuration - tmrs.timePassed,
                        perc = 100 - Math.ceil( dur1 * 100 / opts.auto.timeoutDuration );

                    opts.auto.onTimeoutPause.call($tt0, perc, dur1);
                }
                return true;
            });


            //  play event
            $cfs.bind(cf_e('play', conf), function(e, dir, del, res) {
                e.stopPropagation();
                tmrs = sc_clearTimers(tmrs);

                //  sort params
                var v = [dir, del, res],
                    t = ['string', 'number', 'boolean'],
                    a = cf_sortParams(v, t);

                dir = a[0];
                del = a[1];
                res = a[2];

                if (dir != 'prev' && dir != 'next')
                {
                    dir = crsl.direction;
                }
                if (!is_number(del))
                {
                    del = 0;
                }
                if (!is_boolean(res))
                {
                    res = false;
                }

                //  stopped?
                if (res)
                {
                    crsl.isStopped = false;
                    opts.auto.play = true;
                }
                if (!opts.auto.play)
                {
                    e.stopImmediatePropagation();
                    return debug(conf, 'Carousel stopped: Not scrolling.');
                }

                //  button
                if (crsl.isPaused)
                {
                    if (opts.auto.button)
                    {
                        opts.auto.button.removeClass(cf_c('stopped', conf));
                        opts.auto.button.removeClass(cf_c('paused', conf));
                    }
                }

                //  set playing
                crsl.isPaused = false;
                tmrs.startTime = getTime();

                //  timeout the scrolling
                var dur1 = opts.auto.timeoutDuration + del;
                    dur2 = dur1 - tmrs.timePassed;
                    perc = 100 - Math.ceil(dur2 * 100 / dur1);

                if (opts.auto.progress)
                {
                    tmrs.progress = setInterval(function() {
                        var pasd = getTime() - tmrs.startTime + tmrs.timePassed,
                            perc = Math.ceil(pasd * 100 / dur1);
                        opts.auto.progress.updater.call(opts.auto.progress.bar[0], perc);
                    }, opts.auto.progress.interval);
                }

                tmrs.auto = setTimeout(function() {
                    if (opts.auto.progress)
                    {
                        opts.auto.progress.updater.call(opts.auto.progress.bar[0], 100);
                    }
                    if (opts.auto.onTimeoutEnd)
                    {
                        opts.auto.onTimeoutEnd.call($tt0, perc, dur2);
                    }
                    if (crsl.isScrolling)
                    {
                        $cfs.trigger(cf_e('play', conf), dir);
                    }
                    else
                    {
                        $cfs.trigger(cf_e(dir, conf), opts.auto);
                    }
                }, dur2);

                //  pause start callback
                if (opts.auto.onTimeoutStart)
                {
                    opts.auto.onTimeoutStart.call($tt0, perc, dur2);
                }

                return true;
            });


            //  resume event
            $cfs.bind(cf_e('resume', conf), function(e) {
                e.stopPropagation();
                if (scrl.isStopped)
                {
                    scrl.isStopped = false;
                    crsl.isPaused = false;
                    crsl.isScrolling = true;
                    scrl.startTime = getTime();
                    sc_startScroll(scrl, conf);
                }
                else
                {
                    $cfs.trigger(cf_e('play', conf));
                }
                return true;
            });


            //  prev + next events
            $cfs.bind(cf_e('prev', conf)+' '+cf_e('next', conf), function(e, obj, num, clb, que) {
                e.stopPropagation();

                //  stopped or hidden carousel, don't scroll, don't queue
                if (crsl.isStopped || $cfs.is(':hidden'))
                {
                    e.stopImmediatePropagation();
                    return debug(conf, 'Carousel stopped or hidden: Not scrolling.');
                }

                //  not enough items
                var minimum = (is_number(opts.items.minimum)) ? opts.items.minimum : opts.items.visible + 1;
                if (minimum > itms.total)
                {
                    e.stopImmediatePropagation();
                    return debug(conf, 'Not enough items ('+itms.total+' total, '+minimum+' needed): Not scrolling.');
                }

                //  get config
                var v = [obj, num, clb, que],
                    t = ['object', 'number/string', 'function', 'boolean'],
                    a = cf_sortParams(v, t);

                obj = a[0];
                num = a[1];
                clb = a[2];
                que = a[3];

                var eType = e.type.slice(conf.events.prefix.length);

                if (!is_object(obj))
                {
                    obj = {};
                }
                if (is_function(clb))
                {
                    obj.onAfter = clb;
                }
                if (is_boolean(que))
                {
                    obj.queue = que;
                }
                obj = $.extend(true, {}, opts[eType], obj);

                //  test conditions callback
                if (obj.conditions && !obj.conditions.call($tt0, eType))
                {
                    e.stopImmediatePropagation();
                    return debug(conf, 'Callback "conditions" returned false.');
                }

                if (!is_number(num))
                {
                    if (opts.items.filter != '*')
                    {
                        num = 'visible';
                    }
                    else
                    {
                        var arr = [num, obj.items, opts[eType].items];
                        for (var a = 0, l = arr.length; a < l; a++)
                        {
                            if (is_number(arr[a]) || arr[a] == 'page' || arr[a] == 'visible') {
                                num = arr[a];
                                break;
                            }
                        }
                    }
                    switch(num) {
                        case 'page':
                            e.stopImmediatePropagation();
                            return $cfs.triggerHandler(cf_e(eType+'Page', conf), [obj, clb]);
                            break;

                        case 'visible':
                            if (!opts.items.visibleConf.variable && opts.items.filter == '*')
                            {
                                num = opts.items.visible;
                            }
                            break;
                    }
                }

                //  resume animation, add current to queue
                if (scrl.isStopped)
                {
                    $cfs.trigger(cf_e('resume', conf));
                    $cfs.trigger(cf_e('queue', conf), [eType, [obj, num, clb]]);
                    e.stopImmediatePropagation();
                    return debug(conf, 'Carousel resumed scrolling.');
                }

                //  queue if scrolling
                if (obj.duration > 0)
                {
                    if (crsl.isScrolling)
                    {
                        if (obj.queue)
                        {
                            if (obj.queue == 'last')
                            {
                                queu = [];
                            }
                            if (obj.queue != 'first' || queu.length == 0)
                            {
                                $cfs.trigger(cf_e('queue', conf), [eType, [obj, num, clb]]);
                            }
                        }
                        e.stopImmediatePropagation();
                        return debug(conf, 'Carousel currently scrolling.');
                    }
                }

                tmrs.timePassed = 0;
                $cfs.trigger(cf_e('slide_'+eType, conf), [obj, num]);

                //  synchronise
                if (opts.synchronise)
                {
                    var s = opts.synchronise,
                        c = [obj, num];

                    for (var j = 0, l = s.length; j < l; j++) {
                        var d = eType;
                        if (!s[j][2])
                        {
                            d = (d == 'prev') ? 'next' : 'prev';
                        }
                        if (!s[j][1])
                        {
                            c[0] = s[j][0].triggerHandler('_cfs_triggerEvent', ['configuration', d]);
                        }
                        c[1] = num + s[j][3];
                        s[j][0].trigger('_cfs_triggerEvent', ['slide_'+d, c]);
                    }
                }
                return true;
            });


            //  prev event
            $cfs.bind(cf_e('slide_prev', conf), function(e, sO, nI) {
                e.stopPropagation();
                var a_itm = $cfs.children();

                //  non-circular at start, scroll to end
                if (!opts.circular)
                {
                    if (itms.first == 0)
                    {
                        if (opts.infinite)
                        {
                            $cfs.trigger(cf_e('next', conf), itms.total-1);
                        }
                        return e.stopImmediatePropagation();
                    }
                }

                sz_resetMargin(a_itm, opts);

                //  find number of items to scroll
                if (!is_number(nI))
                {
                    if (opts.items.visibleConf.variable)
                    {
                        nI = gn_getVisibleItemsPrev(a_itm, opts, itms.total-1);
                    }
                    else if (opts.items.filter != '*')
                    {
                        var xI = (is_number(sO.items)) ? sO.items : gn_getVisibleOrg($cfs, opts);
                        nI = gn_getScrollItemsPrevFilter(a_itm, opts, itms.total-1, xI);
                    }
                    else
                    {
                        nI = opts.items.visible;
                    }
                    nI = cf_getAdjust(nI, opts, sO.items, $tt0);
                }

                //  prevent non-circular from scrolling to far
                if (!opts.circular)
                {
                    if (itms.total - nI < itms.first)
                    {
                        nI = itms.total - itms.first;
                    }
                }

                //  set new number of visible items
                opts.items.visibleConf.old = opts.items.visible;
                if (opts.items.visibleConf.variable)
                {
                    var vI = cf_getItemsAdjust(gn_getVisibleItemsNext(a_itm, opts, itms.total-nI), opts, opts.items.visibleConf.adjust, $tt0);
                    if (opts.items.visible+nI <= vI && nI < itms.total)
                    {
                        nI++;
                        vI = cf_getItemsAdjust(gn_getVisibleItemsNext(a_itm, opts, itms.total-nI), opts, opts.items.visibleConf.adjust, $tt0);
                    }
                    opts.items.visible = vI;
                }
                else if (opts.items.filter != '*')
                {
                    var vI = gn_getVisibleItemsNextFilter(a_itm, opts, itms.total-nI);
                    opts.items.visible = cf_getItemsAdjust(vI, opts, opts.items.visibleConf.adjust, $tt0);
                }

                sz_resetMargin(a_itm, opts, true);

                //  scroll 0, don't scroll
                if (nI == 0)
                {
                    e.stopImmediatePropagation();
                    return debug(conf, '0 items to scroll: Not scrolling.');
                }
                debug(conf, 'Scrolling '+nI+' items backward.');


                //  save new config
                itms.first += nI;
                while (itms.first >= itms.total)
                {
                    itms.first -= itms.total;
                }

                //  non-circular callback
                if (!opts.circular)
                {
                    if (itms.first == 0 && sO.onEnd)
                    {
                        sO.onEnd.call($tt0, 'prev');
                    }
                    if (!opts.infinite)
                    {
                        nv_enableNavi(opts, itms.first, conf);
                    }
                }

                //  rearrange items
                $cfs.children().slice(itms.total-nI, itms.total).prependTo($cfs);
                if (itms.total < opts.items.visible + nI)
                {
                    $cfs.children().slice(0, (opts.items.visible+nI)-itms.total).clone(true).appendTo($cfs);
                }

                //  the needed items
                var a_itm = $cfs.children(),
                    i_old = gi_getOldItemsPrev(a_itm, opts, nI),
                    i_new = gi_getNewItemsPrev(a_itm, opts),
                    i_cur_l = a_itm.eq(nI-1),
                    i_old_l = i_old.last(),
                    i_new_l = i_new.last();

                sz_resetMargin(a_itm, opts);

                var pL = 0,
                    pR = 0;

                if (opts.align)
                {
                    var p = cf_getAlignPadding(i_new, opts);
                    pL = p[0];
                    pR = p[1];
                }
                var oL = (pL < 0) ? opts.padding[opts.d[3]] : 0;

                //  hide items for fx directscroll
                var hiddenitems = false,
                    i_skp = $();
                if (opts.items.visible < nI)
                {
                    i_skp = a_itm.slice(opts.items.visibleConf.old, nI);
                    if (sO.fx == 'directscroll')
                    {
                        var orgW = opts.items[opts.d['width']];
                        hiddenitems = i_skp;
                        i_cur_l = i_new_l;
                        sc_hideHiddenItems(hiddenitems);
                        opts.items[opts.d['width']] = 'variable';
                    }
                }

                //  save new sizes
                var $cf2 = false,
                    i_siz = ms_getTotalSize(a_itm.slice(0, nI), opts, 'width'),
                    w_siz = cf_mapWrapperSizes(ms_getSizes(i_new, opts, true), opts, !opts.usePadding),
                    i_siz_vis = 0,
                    a_cfs = {},
                    a_wsz = {},
                    a_cur = {},
                    a_old = {},
                    a_new = {},
                    a_lef = {},
                    a_lef_vis = {},
                    a_dur = sc_getDuration(sO, opts, nI, i_siz);

                switch(sO.fx)
                {
                    case 'cover':
                    case 'cover-fade':
                        i_siz_vis = ms_getTotalSize(a_itm.slice(0, opts.items.visible), opts, 'width');
                        break;
                }

                if (hiddenitems)
                {
                    opts.items[opts.d['width']] = orgW;
                }

                sz_resetMargin(a_itm, opts, true);
                if (pR >= 0)
                {
                    sz_resetMargin(i_old_l, opts, opts.padding[opts.d[1]]);
                }
                if (pL >= 0)
                {
                    sz_resetMargin(i_cur_l, opts, opts.padding[opts.d[3]]);
                }

                if (opts.align)
                {
                    opts.padding[opts.d[1]] = pR;
                    opts.padding[opts.d[3]] = pL;
                }

                a_lef[opts.d['left']] = -(i_siz - oL);
                a_lef_vis[opts.d['left']] = -(i_siz_vis - oL);
                a_wsz[opts.d['left']] = w_siz[opts.d['width']];

                //  scrolling functions
                var _s_wrapper = function() {},
                    _a_wrapper = function() {},
                    _s_paddingold = function() {},
                    _a_paddingold = function() {},
                    _s_paddingnew = function() {},
                    _a_paddingnew = function() {},
                    _s_paddingcur = function() {},
                    _a_paddingcur = function() {},
                    _onafter = function() {},
                    _moveitems = function() {},
                    _position = function() {};

                //  clone carousel
                switch(sO.fx)
                {
                    case 'crossfade':
                    case 'cover':
                    case 'cover-fade':
                    case 'uncover':
                    case 'uncover-fade':
                        $cf2 = $cfs.clone(true).appendTo($wrp);
                        break;
                }
                switch(sO.fx)
                {
                    case 'crossfade':
                    case 'uncover':
                    case 'uncover-fade':
                        $cf2.children().slice(0, nI).remove();
                        $cf2.children().slice(opts.items.visibleConf.old).remove();
                        break;

                    case 'cover':
                    case 'cover-fade':
                        $cf2.children().slice(opts.items.visible).remove();
                        $cf2.css(a_lef_vis);
                        break;
                }

                $cfs.css(a_lef);

                //  reset all scrolls
                scrl = sc_setScroll(a_dur, sO.easing, conf);

                //  animate / set carousel
                a_cfs[opts.d['left']] = (opts.usePadding) ? opts.padding[opts.d[3]] : 0;

                //  animate / set wrapper
                if (opts[opts.d['width']] == 'variable' || opts[opts.d['height']] == 'variable')
                {
                    _s_wrapper = function() {
                        $wrp.css(w_siz);
                    };
                    _a_wrapper = function() {
                        scrl.anims.push([$wrp, w_siz]);
                    };
                }

                //  animate / set items
                if (opts.usePadding)
                {
                    if (i_new_l.not(i_cur_l).length)
                    {
                        a_cur[opts.d['marginRight']] = i_cur_l.data('_cfs_origCssMargin');

                        if (pL < 0)
                        {
                            i_cur_l.css(a_cur);
                        }
                        else
                        {
                            _s_paddingcur = function() {
                                i_cur_l.css(a_cur);
                            };
                            _a_paddingcur = function() {
                                scrl.anims.push([i_cur_l, a_cur]);
                            };
                        }
                    }
                    switch(sO.fx)
                    {
                        case 'cover':
                        case 'cover-fade':
                            $cf2.children().eq(nI-1).css(a_cur);
                            break;
                    }

                    if (i_new_l.not(i_old_l).length)
                    {
                        a_old[opts.d['marginRight']] = i_old_l.data('_cfs_origCssMargin');
                        _s_paddingold = function() {
                            i_old_l.css(a_old);
                        };
                        _a_paddingold = function() {
                            scrl.anims.push([i_old_l, a_old]);
                        };
                    }

                    if (pR >= 0)
                    {
                        a_new[opts.d['marginRight']] = i_new_l.data('_cfs_origCssMargin') + opts.padding[opts.d[1]];
                        _s_paddingnew = function() {
                            i_new_l.css(a_new);
                        };
                        _a_paddingnew = function() {
                            scrl.anims.push([i_new_l, a_new]);
                        };
                    }
                }

                //  set position
                _position = function() {
                    $cfs.css(a_cfs);
                };


                var overFill = opts.items.visible+nI-itms.total;

                //  rearrange items
                _moveitems = function() {
                    if (overFill > 0)
                    {
                        $cfs.children().slice(itms.total).remove();
                        i_old = $( $cfs.children().slice(itms.total-(opts.items.visible-overFill)).get().concat( $cfs.children().slice(0, overFill).get() ) );
                    }
                    sc_showHiddenItems(hiddenitems);

                    if (opts.usePadding)
                    {
                        var l_itm = $cfs.children().eq(opts.items.visible+nI-1);
                        l_itm.css(opts.d['marginRight'], l_itm.data('_cfs_origCssMargin'));
                    }
                };


                var cb_arguments = sc_mapCallbackArguments(i_old, i_skp, i_new, nI, 'prev', a_dur, w_siz);

                //  fire onAfter callbacks
                _onafter = function() {
                    sc_afterScroll($cfs, $cf2, sO);
                    crsl.isScrolling = false;
                    clbk.onAfter = sc_fireCallbacks($tt0, sO, 'onAfter', cb_arguments, clbk);
                    queu = sc_fireQueue($cfs, queu, conf);

                    if (!crsl.isPaused)
                    {
                        $cfs.trigger(cf_e('play', conf));
                    }
                };

                //  fire onBefore callback
                crsl.isScrolling = true;
                tmrs = sc_clearTimers(tmrs);
                clbk.onBefore = sc_fireCallbacks($tt0, sO, 'onBefore', cb_arguments, clbk);

                switch(sO.fx)
                {
                    case 'none':
                        $cfs.css(a_cfs);
                        _s_wrapper();
                        _s_paddingold();
                        _s_paddingnew();
                        _s_paddingcur();
                        _position();
                        _moveitems();
                        _onafter();
                        break;

                    case 'fade':
                        scrl.anims.push([$cfs, { 'opacity': 0 }, function() {
                            _s_wrapper();
                            _s_paddingold();
                            _s_paddingnew();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            scrl = sc_setScroll(a_dur, sO.easing, conf);
                            scrl.anims.push([$cfs, { 'opacity': 1 }, _onafter]);
                            sc_startScroll(scrl, conf);
                        }]);
                        break;

                    case 'crossfade':
                        $cfs.css({ 'opacity': 0 });
                        scrl.anims.push([$cf2, { 'opacity': 0 }]);
                        scrl.anims.push([$cfs, { 'opacity': 1 }, _onafter]);
                        _a_wrapper();
                        _s_paddingold();
                        _s_paddingnew();
                        _s_paddingcur();
                        _position();
                        _moveitems();
                        break;

                    case 'cover':
                        scrl.anims.push([$cf2, a_cfs, function() {
                            _s_paddingold();
                            _s_paddingnew();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            _onafter();
                        }]);
                        _a_wrapper();
                        break;

                    case 'cover-fade':
                        scrl.anims.push([$cfs, { 'opacity': 0 }]);
                        scrl.anims.push([$cf2, a_cfs, function() {
                            _s_paddingold();
                            _s_paddingnew();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            _onafter();
                        }]);
                        _a_wrapper();
                        break;

                    case 'uncover':
                        scrl.anims.push([$cf2, a_wsz, _onafter]);
                        _a_wrapper();
                        _s_paddingold();
                        _s_paddingnew();
                        _s_paddingcur();
                        _position();
                        _moveitems();
                        break;

                    case 'uncover-fade':
                        $cfs.css({ 'opacity': 0 });
                        scrl.anims.push([$cfs, { 'opacity': 1 }]);
                        scrl.anims.push([$cf2, a_wsz, _onafter]);
                        _a_wrapper();
                        _s_paddingold();
                        _s_paddingnew();
                        _s_paddingcur();
                        _position();
                        _moveitems();
                        break;

                    default:
                        scrl.anims.push([$cfs, a_cfs, function() {
                            _moveitems();
                            _onafter();
                        }]);
                        _a_wrapper();
                        _a_paddingold();
                        _a_paddingnew();
                        _a_paddingcur();
                        break;
                }

                sc_startScroll(scrl, conf);
                cf_setCookie(opts.cookie, $cfs, conf);

                $cfs.trigger(cf_e('updatePageStatus', conf), [false, w_siz]);

                return true;
            });


            //  next event
            $cfs.bind(cf_e('slide_next', conf), function(e, sO, nI) {
                e.stopPropagation();
                var a_itm = $cfs.children();

                //  non-circular at end, scroll to start
                if (!opts.circular)
                {
                    if (itms.first == opts.items.visible)
                    {
                        if (opts.infinite)
                        {
                            $cfs.trigger(cf_e('prev', conf), itms.total-1);
                        }
                        return e.stopImmediatePropagation();
                    }
                }

                sz_resetMargin(a_itm, opts);

                //  find number of items to scroll
                if (!is_number(nI))
                {
                    if (opts.items.filter != '*')
                    {
                        var xI = (is_number(sO.items)) ? sO.items : gn_getVisibleOrg($cfs, opts);
                        nI = gn_getScrollItemsNextFilter(a_itm, opts, 0, xI);
                    }
                    else
                    {
                        nI = opts.items.visible;
                    }
                    nI = cf_getAdjust(nI, opts, sO.items, $tt0);
                }

                var lastItemNr = (itms.first == 0) ? itms.total : itms.first;

                //  prevent non-circular from scrolling to far
                if (!opts.circular)
                {
                    if (opts.items.visibleConf.variable)
                    {
                        var vI = gn_getVisibleItemsNext(a_itm, opts, nI),
                            xI = gn_getVisibleItemsPrev(a_itm, opts, lastItemNr-1);
                    }
                    else
                    {
                        var vI = opts.items.visible,
                            xI = opts.items.visible;
                    }

                    if (nI + vI > lastItemNr)
                    {
                        nI = lastItemNr - xI;
                    }
                }

                //  set new number of visible items
                opts.items.visibleConf.old = opts.items.visible;
                if (opts.items.visibleConf.variable)
                {
                    var vI = cf_getItemsAdjust(gn_getVisibleItemsNextTestCircular(a_itm, opts, nI, lastItemNr), opts, opts.items.visibleConf.adjust, $tt0);
                    while (opts.items.visible-nI >= vI && nI < itms.total)
                    {
                        nI++;
                        vI = cf_getItemsAdjust(gn_getVisibleItemsNextTestCircular(a_itm, opts, nI, lastItemNr), opts, opts.items.visibleConf.adjust, $tt0);
                    }
                    opts.items.visible = vI;
                }
                else if (opts.items.filter != '*')
                {
                    var vI = gn_getVisibleItemsNextFilter(a_itm, opts, nI);
                    opts.items.visible = cf_getItemsAdjust(vI, opts, opts.items.visibleConf.adjust, $tt0);
                }

                sz_resetMargin(a_itm, opts, true);

                //  scroll 0, don't scroll
                if (nI == 0)
                {
                    e.stopImmediatePropagation();
                    return debug(conf, '0 items to scroll: Not scrolling.');
                }
                debug(conf, 'Scrolling '+nI+' items forward.');


                //  save new config
                itms.first -= nI;
                while (itms.first < 0)
                {
                    itms.first += itms.total;
                }

                //  non-circular callback
                if (!opts.circular)
                {
                    if (itms.first == opts.items.visible && sO.onEnd)
                    {
                        sO.onEnd.call($tt0, 'next');
                    }
                    if (!opts.infinite)
                    {
                        nv_enableNavi(opts, itms.first, conf);
                    }
                }

                //  rearrange items
                if (itms.total < opts.items.visible+nI)
                {
                    $cfs.children().slice(0, (opts.items.visible+nI)-itms.total).clone(true).appendTo($cfs);
                }

                //  the needed items
                var a_itm = $cfs.children(),
                    i_old = gi_getOldItemsNext(a_itm, opts),
                    i_new = gi_getNewItemsNext(a_itm, opts, nI),
                    i_cur_l = a_itm.eq(nI-1),
                    i_old_l = i_old.last(),
                    i_new_l = i_new.last();

                sz_resetMargin(a_itm, opts);

                var pL = 0,
                    pR = 0;

                if (opts.align)
                {
                    var p = cf_getAlignPadding(i_new, opts);
                    pL = p[0];
                    pR = p[1];
                }

                //  hide items for fx directscroll
                var hiddenitems = false,
                    i_skp = $();
                if (opts.items.visibleConf.old < nI)
                {
                    i_skp = a_itm.slice(opts.items.visibleConf.old, nI);
                    if (sO.fx == 'directscroll')
                    {
                        var orgW = opts.items[opts.d['width']];
                        hiddenitems = i_skp;
                        i_cur_l = i_old_l;
                        sc_hideHiddenItems(hiddenitems);
                        opts.items[opts.d['width']] = 'variable';
                    }
                }

                //  save new sizes
                var $cf2 = false,
                    i_siz = ms_getTotalSize(a_itm.slice(0, nI), opts, 'width'),
                    w_siz = cf_mapWrapperSizes(ms_getSizes(i_new, opts, true), opts, !opts.usePadding),
                    i_siz_vis = 0,
                    a_cfs = {},
                    a_cfs_vis = {},
                    a_cur = {},
                    a_old = {},
                    a_lef = {},
                    a_dur = sc_getDuration(sO, opts, nI, i_siz);

                switch(sO.fx)
                {
                    case 'uncover':
                    case 'uncover-fade':
                        i_siz_vis = ms_getTotalSize(a_itm.slice(0, opts.items.visibleConf.old), opts, 'width');
                        break;
                }

                if (hiddenitems)
                {
                    opts.items[opts.d['width']] = orgW;
                }

                if (opts.align)
                {
                    if (opts.padding[opts.d[1]] < 0)
                    {
                        opts.padding[opts.d[1]] = 0;
                    }
                }
                sz_resetMargin(a_itm, opts, true);
                sz_resetMargin(i_old_l, opts, opts.padding[opts.d[1]]);

                if (opts.align)
                {
                    opts.padding[opts.d[1]] = pR;
                    opts.padding[opts.d[3]] = pL;
                }

                a_lef[opts.d['left']] = (opts.usePadding) ? opts.padding[opts.d[3]] : 0;

                //  scrolling functions
                var _s_wrapper = function() {},
                    _a_wrapper = function() {},
                    _s_paddingold = function() {},
                    _a_paddingold = function() {},
                    _s_paddingcur = function() {},
                    _a_paddingcur = function() {},
                    _onafter = function() {},
                    _moveitems = function() {},
                    _position = function() {};

                //  clone carousel
                switch(sO.fx)
                {
                    case 'crossfade':
                    case 'cover':
                    case 'cover-fade':
                    case 'uncover':
                    case 'uncover-fade':
                        $cf2 = $cfs.clone(true).appendTo($wrp);
                        $cf2.children().slice(opts.items.visibleConf.old).remove();
                        break;
                }
                switch(sO.fx)
                {
                    case 'crossfade':
                    case 'cover':
                    case 'cover-fade':
                        $cfs.css('zIndex', 1);
                        $cf2.css('zIndex', 0);
                        break;
                }

                //  reset all scrolls
                scrl = sc_setScroll(a_dur, sO.easing, conf);

                //  animate / set carousel
                a_cfs[opts.d['left']] = -i_siz;
                a_cfs_vis[opts.d['left']] = -i_siz_vis;

                if (pL < 0)
                {
                    a_cfs[opts.d['left']] += pL;
                }

                //  animate / set wrapper
                if (opts[opts.d['width']] == 'variable' || opts[opts.d['height']] == 'variable')
                {
                    _s_wrapper = function() {
                        $wrp.css(w_siz);
                    };
                    _a_wrapper = function() {
                        scrl.anims.push([$wrp, w_siz]);
                    };
                }

                //  animate / set items
                if (opts.usePadding)
                {
                    var i_new_l_m = i_new_l.data('_cfs_origCssMargin');

                    if (pR >= 0)
                    {
                        i_new_l_m += opts.padding[opts.d[1]];
                    }
                    i_new_l.css(opts.d['marginRight'], i_new_l_m);

                    if (i_cur_l.not(i_old_l).length)
                    {
                        a_old[opts.d['marginRight']] = i_old_l.data('_cfs_origCssMargin');
                    }
                    _s_paddingold = function() {
                        i_old_l.css(a_old);
                    };
                    _a_paddingold = function() {
                        scrl.anims.push([i_old_l, a_old]);
                    };

                    var i_cur_l_m = i_cur_l.data('_cfs_origCssMargin');
                    if (pL > 0)
                    {
                        i_cur_l_m += opts.padding[opts.d[3]];
                    }

                    a_cur[opts.d['marginRight']] = i_cur_l_m;

                    _s_paddingcur = function() {
                        i_cur_l.css(a_cur);
                    };
                    _a_paddingcur = function() {
                        scrl.anims.push([i_cur_l, a_cur]);
                    };
                }

                //  set position
                _position = function() {
                    $cfs.css(a_lef);
                };


                var overFill = opts.items.visible+nI-itms.total;

                //  rearrange items
                _moveitems = function() {
                    if (overFill > 0)
                    {
                        $cfs.children().slice(itms.total).remove();
                    }
                    var l_itm = $cfs.children().slice(0, nI).appendTo($cfs).last();
                    if (overFill > 0)
                    {
                        i_new = gi_getCurrentItems(a_itm, opts);
                    }
                    sc_showHiddenItems(hiddenitems);

                    if (opts.usePadding)
                    {
                        if (itms.total < opts.items.visible+nI) {
                            var i_cur_l = $cfs.children().eq(opts.items.visible-1);
                            i_cur_l.css(opts.d['marginRight'], i_cur_l.data('_cfs_origCssMargin') + opts.padding[opts.d[1]]);
                        }
                        l_itm.css(opts.d['marginRight'], l_itm.data('_cfs_origCssMargin'));
                    }
                };


                var cb_arguments = sc_mapCallbackArguments(i_old, i_skp, i_new, nI, 'next', a_dur, w_siz);

                //  fire onAfter callbacks
                _onafter = function() {
                    $cfs.css('zIndex', $cfs.data('_cfs_origCssZindex'));
                    sc_afterScroll($cfs, $cf2, sO);
                    crsl.isScrolling = false;
                    clbk.onAfter = sc_fireCallbacks($tt0, sO, 'onAfter', cb_arguments, clbk);
                    queu = sc_fireQueue($cfs, queu, conf);
                    
                    if (!crsl.isPaused)
                    {
                        $cfs.trigger(cf_e('play', conf));
                    }
                };

                //  fire onBefore callbacks
                crsl.isScrolling = true;
                tmrs = sc_clearTimers(tmrs);
                clbk.onBefore = sc_fireCallbacks($tt0, sO, 'onBefore', cb_arguments, clbk);

                switch(sO.fx)
                {
                    case 'none':
                        $cfs.css(a_cfs);
                        _s_wrapper();
                        _s_paddingold();
                        _s_paddingcur();
                        _position();
                        _moveitems();
                        _onafter();
                        break;

                    case 'fade':
                        scrl.anims.push([$cfs, { 'opacity': 0 }, function() {
                            _s_wrapper();
                            _s_paddingold();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            scrl = sc_setScroll(a_dur, sO.easing, conf);
                            scrl.anims.push([$cfs, { 'opacity': 1 }, _onafter]);
                            sc_startScroll(scrl, conf);
                        }]);
                        break;

                    case 'crossfade':
                        $cfs.css({ 'opacity': 0 });
                        scrl.anims.push([$cf2, { 'opacity': 0 }]);
                        scrl.anims.push([$cfs, { 'opacity': 1 }, _onafter]);
                        _a_wrapper();
                        _s_paddingold();
                        _s_paddingcur();
                        _position();
                        _moveitems();
                        break;

                    case 'cover':
                        $cfs.css(opts.d['left'], $wrp[opts.d['width']]());
                        scrl.anims.push([$cfs, a_lef, _onafter]);
                        _a_wrapper();
                        _s_paddingold();
                        _s_paddingcur();
                        _moveitems();
                        break;

                    case 'cover-fade':
                        $cfs.css(opts.d['left'], $wrp[opts.d['width']]());
                        scrl.anims.push([$cf2, { 'opacity': 0 }]);
                        scrl.anims.push([$cfs, a_lef, _onafter]);
                        _a_wrapper();
                        _s_paddingold();
                        _s_paddingcur();
                        _moveitems();
                        break;

                    case 'uncover':
                        scrl.anims.push([$cf2, a_cfs_vis, _onafter]);
                        _a_wrapper();
                        _s_paddingold();
                        _s_paddingcur();
                        _position();
                        _moveitems();
                        break;

                    case 'uncover-fade':
                        $cfs.css({ 'opacity': 0 });
                        scrl.anims.push([$cfs, { 'opacity': 1 }]);
                        scrl.anims.push([$cf2, a_cfs_vis, _onafter]);
                        _a_wrapper();
                        _s_paddingold();
                        _s_paddingcur();
                        _position();
                        _moveitems();
                        break;

                    default:
                        scrl.anims.push([$cfs, a_cfs, function() {
                            _position();
                            _moveitems();
                            _onafter();
                        }]);
                        _a_wrapper();
                        _a_paddingold();
                        _a_paddingcur();
                        break;
                }

                sc_startScroll(scrl, conf);
                cf_setCookie(opts.cookie, $cfs, conf);

                $cfs.trigger(cf_e('updatePageStatus', conf), [false, w_siz]);

                return true;
            });


            //  slideTo event
            $cfs.bind(cf_e('slideTo', conf), function(e, num, dev, org, obj, dir, clb) {
                e.stopPropagation();

                var v = [num, dev, org, obj, dir, clb],
                    t = ['string/number/object', 'number', 'boolean', 'object', 'string', 'function'],
                    a = cf_sortParams(v, t);

                obj = a[3];
                dir = a[4];
                clb = a[5];

                num = gn_getItemIndex(a[0], a[1], a[2], itms, $cfs);

                if (num == 0)
                {
                    return false;
                }
                if (!is_object(obj))
                {
                    obj = false;
                }

                if (dir != 'prev' && dir != 'next')
                {
                    if (opts.circular)
                    {
                        dir = (num <= itms.total / 2) ? 'next' : 'prev';
                    }
                    else
                    {
                        dir = (itms.first == 0 || itms.first > num) ? 'next' : 'prev';
                    }
                }

                if (dir == 'prev')
                {
                    num = itms.total-num;
                }
                $cfs.trigger(cf_e(dir, conf), [obj, num, clb]);

                return true;
            });


            //  prevPage event
            $cfs.bind(cf_e('prevPage', conf), function(e, obj, clb) {
                e.stopPropagation();
                var cur = $cfs.triggerHandler(cf_e('currentPage', conf));
                return $cfs.triggerHandler(cf_e('slideToPage', conf), [cur-1, obj, 'prev', clb]);
            });


            //  nextPage event
            $cfs.bind(cf_e('nextPage', conf), function(e, obj, clb) {
                e.stopPropagation();
                var cur = $cfs.triggerHandler(cf_e('currentPage', conf));
                return $cfs.triggerHandler(cf_e('slideToPage', conf), [cur+1, obj, 'next', clb]);
            });


            //  slideToPage event
            $cfs.bind(cf_e('slideToPage', conf), function(e, pag, obj, dir, clb) {
                e.stopPropagation();
                if (!is_number(pag))
                {
                    pag = $cfs.triggerHandler(cf_e('currentPage', conf));
                }
                var ipp = opts.pagination.items || opts.items.visible,
                    max = Math.ceil(itms.total / ipp)-1;

                if (pag < 0)
                {
                    pag = max;
                }
                if (pag > max)
                {
                    pag = 0;
                }
                return $cfs.triggerHandler(cf_e('slideTo', conf), [pag*ipp, 0, true, obj, dir, clb]);
            });

            //  jumpToStart event
            $cfs.bind(cf_e('jumpToStart', conf), function(e, s) {
                e.stopPropagation();
                if (s)
                {
                    s = gn_getItemIndex(s, 0, true, itms, $cfs);
                }
                else
                {
                    s = 0;
                }

                s += itms.first;
                if (s != 0)
                {
                    if (itms.total > 0)
                    {
                        while (s > itms.total)
                        {
                            s -= itms.total;
                        }
                    }
                    $cfs.prepend($cfs.children().slice(s, itms.total));
                }
                return true;
            });


            //  synchronise event
            $cfs.bind(cf_e('synchronise', conf), function(e, s) {
                e.stopPropagation();
                if (s)
                {
                    s = cf_getSynchArr(s);
                }
                else if (opts.synchronise)
                {
                    s = opts.synchronise;
                }
                else
                {
                    return debug(conf, 'No carousel to synchronise.');
                }

                var n = $cfs.triggerHandler(cf_e('currentPosition', conf)),
                    x = true;

                for (var j = 0, l = s.length; j < l; j++)
                {
                    if (!s[j][0].triggerHandler(cf_e('slideTo', conf), [n, s[j][3], true]))
                    {
                        x = false;
                    }
                }
                return x;
            });


            //  queue event
            $cfs.bind(cf_e('queue', conf), function(e, dir, opt) {
                e.stopPropagation();
                if (is_function(dir))
                {
                    dir.call($tt0, queu);
                }
                else if (is_array(dir))
                {
                    queu = dir;
                }
                else if (!is_undefined(dir))
                {
                    queu.push([dir, opt]);
                }
                return queu;
            });


            //  insertItem event
            $cfs.bind(cf_e('insertItem', conf), function(e, itm, num, org, dev) {
                e.stopPropagation();

                var v = [itm, num, org, dev],
                    t = ['string/object', 'string/number/object', 'boolean', 'number'],
                    a = cf_sortParams(v, t);

                itm = a[0];
                num = a[1];
                org = a[2];
                dev = a[3];

                if (is_object(itm) && !is_jquery(itm))
                { 
                    itm = $(itm);
                }
                else if (is_string(itm))
                {
                    itm = $(itm);
                }
                if (!is_jquery(itm) || itm.length == 0)
                {
                    return debug(conf, 'Not a valid object.');
                }

                if (is_undefined(num))
                {
                    num = 'end';
                }

                sz_storeMargin(itm, opts);
                sz_storeOrigCss(itm);

                var orgNum = num,
                    before = 'before';

                if (num == 'end')
                {
                    if (org)
                    {
                        if (itms.first == 0)
                        {
                            num = itms.total-1;
                            before = 'after';
                        }
                        else
                        {
                            num = itms.first;
                            itms.first += itm.length;
                        }
                        if (num < 0)
                        {
                            num = 0;
                        }
                    }
                    else
                    {
                        num = itms.total-1;
                        before = 'after';
                    }
                }
                else
                {
                    num = gn_getItemIndex(num, dev, org, itms, $cfs);
                }

                var $cit = $cfs.children().eq(num);
                if ($cit.length)
                {
                    $cit[before](itm);
                }
                else
                {
                    debug(conf, 'Correct insert-position not found! Appending item to the end.');
                    $cfs.append(itm);
                }

                if (orgNum != 'end' && !org)
                {
                    if (num < itms.first)
                    {
                        itms.first += itm.length;
                    }
                }
                itms.total = $cfs.children().length;
                if (itms.first >= itms.total)
                {
                    itms.first -= itms.total;
                }

                $cfs.trigger(cf_e('updateSizes', conf));
                $cfs.trigger(cf_e('linkAnchors', conf));

                return true;
            });


            //  removeItem event
            $cfs.bind(cf_e('removeItem', conf), function(e, num, org, dev) {
                e.stopPropagation();

                var v = [num, org, dev],
                    t = ['string/number/object', 'boolean', 'number'],
                    a = cf_sortParams(v, t);

                num = a[0];
                org = a[1];
                dev = a[2];

                var removed = false;

                if (num instanceof $ && num.length > 1)
                {
                    $removed = $();
                    num.each(function(i, el) {
                        var $rem = $cfs.trigger(cf_e('removeItem', conf), [$(this), org, dev]);
                        if ( $rem ) 
                        {
                            $removed = $removed.add($rem);
                        }
                    });
                    return $removed;
                }

                if (is_undefined(num) || num == 'end')
                {
                    $removed = $cfs.children().last();
                }
                else
                {
                    num = gn_getItemIndex(num, dev, org, itms, $cfs);
                    var $removed = $cfs.children().eq(num);
                    if ( $removed.length )
                    {
                        if (num < itms.first)
                        {
                            itms.first -= $removed.length;
                        }
                    }
                }
                if ( $removed && $removed.length )
                {
                    $removed.detach();
                    itms.total = $cfs.children().length;
                    $cfs.trigger(cf_e('updateSizes', conf));
                }

                return $removed;
            });


            //  onBefore and onAfter event
            $cfs.bind(cf_e('onBefore', conf)+' '+cf_e('onAfter', conf), function(e, fn) {
                e.stopPropagation();
                var eType = e.type.slice(conf.events.prefix.length);
                if (is_array(fn))
                {
                    clbk[eType] = fn;
                }
                if (is_function(fn))
                {
                    clbk[eType].push(fn);
                }
                return clbk[eType];
            });


            //  currentPosition event
            $cfs.bind(cf_e('currentPosition', conf), function(e, fn) {
                e.stopPropagation();
                if (itms.first == 0)
                {
                    var val = 0;
                }
                else
                {
                    var val = itms.total - itms.first;
                }
                if (is_function(fn))
                {
                    fn.call($tt0, val);
                }
                return val;
            });


            //  currentPage event
            $cfs.bind(cf_e('currentPage', conf), function(e, fn) {
                e.stopPropagation();
                var ipp = opts.pagination.items || opts.items.visible,
                    max = Math.ceil(itms.total/ipp-1),
                    nr;
                if (itms.first == 0)
                {
                    nr = 0;
                }
                else if (itms.first < itms.total % ipp)
                {
                    nr = 0;
                }
                else if (itms.first == ipp && !opts.circular)
                {
                    nr = max;
                }
                else 
                {
                     nr = Math.round((itms.total-itms.first)/ipp);
                }
                if (nr < 0)
                {
                    nr = 0;
                }
                if (nr > max)
                {
                    nr = max;
                }
                if (is_function(fn))
                {
                    fn.call($tt0, nr);
                }
                return nr;
            });


            //  currentVisible event
            $cfs.bind(cf_e('currentVisible', conf), function(e, fn) {
                e.stopPropagation();
                var $i = gi_getCurrentItems($cfs.children(), opts);
                if (is_function(fn))
                {
                    fn.call($tt0, $i);
                }
                return $i;
            });


            //  slice event
            $cfs.bind(cf_e('slice', conf), function(e, f, l, fn) {
                e.stopPropagation();

                if (itms.total == 0)
                {
                    return false;
                }

                var v = [f, l, fn],
                    t = ['number', 'number', 'function'],
                    a = cf_sortParams(v, t);

                f = (is_number(a[0])) ? a[0] : 0;
                l = (is_number(a[1])) ? a[1] : itms.total;
                fn = a[2];

                f += itms.first;
                l += itms.first;

                if (items.total > 0)
                {
                    while (f > itms.total)
                    {
                        f -= itms.total;
                    }
                    while (l > itms.total)
                    {
                        l -= itms.total;
                    }
                    while (f < 0)
                    {
                        f += itms.total;
                    }
                    while (l < 0)
                    {
                        l += itms.total;
                    }
                }
                var $iA = $cfs.children(),
                    $i;

                if (l > f)
                {
                    $i = $iA.slice(f, l);
                }
                else
                {
                    $i = $( $iA.slice(f, itms.total).get().concat( $iA.slice(0, l).get() ) );
                }

                if (is_function(fn))
                {
                    fn.call($tt0, $i);
                }
                return $i;
            });


            //  isPaused, isStopped and isScrolling events
            $cfs.bind(cf_e('isPaused', conf)+' '+cf_e('isStopped', conf)+' '+cf_e('isScrolling', conf), function(e, fn) {
                e.stopPropagation();
                var eType = e.type.slice(conf.events.prefix.length),
                    value = crsl[eType];
                if (is_function(fn))
                {
                    fn.call($tt0, value);
                }
                return value;
            });


            //  configuration event
            $cfs.bind(cf_e('configuration', conf), function(e, a, b, c) {
                e.stopPropagation();
                var reInit = false;

                //  return entire configuration-object
                if (is_function(a))
                {
                    a.call($tt0, opts);
                }
                //  set multiple options via object
                else if (is_object(a))
                {
                    opts_orig = $.extend(true, {}, opts_orig, a);
                    if (b !== false) reInit = true;
                    else opts = $.extend(true, {}, opts, a);

                }
                else if (!is_undefined(a))
                {

                    //  callback function for specific option
                    if (is_function(b))
                    {
                        var val = eval('opts.'+a);
                        if (is_undefined(val))
                        {
                            val = '';
                        }
                        b.call($tt0, val);
                    }
                    //  set individual option
                    else if (!is_undefined(b))
                    {
                        if (typeof c !== 'boolean') c = true;
                        eval('opts_orig.'+a+' = b');
                        if (c !== false) reInit = true;
                        else eval('opts.'+a+' = b');
                    }
                    //  return value for specific option
                    else
                    {
                        return eval('opts.'+a);
                    }
                }
                if (reInit)
                {
                    sz_resetMargin($cfs.children(), opts);
                    FN._init(opts_orig);
                    FN._bind_buttons();
                    var sz = sz_setSizes($cfs, opts);
                    $cfs.trigger(cf_e('updatePageStatus', conf), [true, sz]);
                }
                return opts;
            });


            //  linkAnchors event
            $cfs.bind(cf_e('linkAnchors', conf), function(e, $con, sel) {
                e.stopPropagation();

                if (is_undefined($con))
                {
                    $con = $('body');
                }
                else if (is_string($con))
                {
                    $con = $($con);
                }
                if (!is_jquery($con) || $con.length == 0)
                {
                    return debug(conf, 'Not a valid object.');
                }
                if (!is_string(sel))
                {
                    sel = 'a.caroufredsel';
                }

                $con.find(sel).each(function() {
                    var h = this.hash || '';
                    if (h.length > 0 && $cfs.children().index($(h)) != -1)
                    {
                        $(this).unbind('click').click(function(e) {
                            e.preventDefault();
                            $cfs.trigger(cf_e('slideTo', conf), h);
                        });
                    }
                });
                return true;
            });


            //  updatePageStatus event
            $cfs.bind(cf_e('updatePageStatus', conf), function(e, build, sizes) {
                e.stopPropagation();
                if (!opts.pagination.container)
                {
                    return;
                }

                var ipp = opts.pagination.items || opts.items.visible,
                    pgs = Math.ceil(itms.total/ipp);

                if (build)
                {
                    if (opts.pagination.anchorBuilder)
                    {
                        opts.pagination.container.children().remove();
                        opts.pagination.container.each(function() {
                            for (var a = 0; a < pgs; a++)
                            {
                                var i = $cfs.children().eq( gn_getItemIndex(a*ipp, 0, true, itms, $cfs) );
                                $(this).append(opts.pagination.anchorBuilder.call(i[0], a+1));
                            }
                        });
                    }
                    opts.pagination.container.each(function() {
                        $(this).children().unbind(opts.pagination.event).each(function(a) {
                            $(this).bind(opts.pagination.event, function(e) {
                                e.preventDefault();
                                $cfs.trigger(cf_e('slideTo', conf), [a*ipp, -opts.pagination.deviation, true, opts.pagination]);
                            });
                        });
                    });
                }

                var selected = $cfs.triggerHandler(cf_e('currentPage', conf)) + opts.pagination.deviation;
                if (selected >= pgs)
                {
                    selected = 0;
                }
                if (selected < 0)
                {
                    selected = pgs-1;
                }
                opts.pagination.container.each(function() {
                    $(this).children().removeClass(cf_c('selected', conf)).eq(selected).addClass(cf_c('selected', conf));
                });
                return true;
            });


            //  updateSizes event
            $cfs.bind(cf_e('updateSizes', conf), function(e) {
                var vI = opts.items.visible,
                    a_itm = $cfs.children(),
                    avail_primary = ms_getParentSize($wrp, opts, 'width');

                itms.total = a_itm.length;

                if (crsl.primarySizePercentage)
                {
                    opts.maxDimension = avail_primary;
                    opts[opts.d['width']] = ms_getPercentage(avail_primary, crsl.primarySizePercentage);
                }
                else
                {
                    opts.maxDimension = ms_getMaxDimension(opts, avail_primary);
                }

                if (opts.responsive)
                {
                    opts.items.width = opts.items.sizesConf.width;
                    opts.items.height = opts.items.sizesConf.height;
                    opts = in_getResponsiveValues(opts, a_itm, avail_primary);
                    vI = opts.items.visible;
                    sz_setResponsiveSizes(opts, a_itm);
                }
                else if (opts.items.visibleConf.variable)
                {
                    vI = gn_getVisibleItemsNext(a_itm, opts, 0);
                }
                else if (opts.items.filter != '*')
                {
                    vI = gn_getVisibleItemsNextFilter(a_itm, opts, 0);
                }

                if (!opts.circular && itms.first != 0 && vI > itms.first) {
                    if (opts.items.visibleConf.variable)
                    {
                        var nI = gn_getVisibleItemsPrev(a_itm, opts, itms.first) - itms.first;
                    }
                    else if (opts.items.filter != '*')
                    {
                        var nI = gn_getVisibleItemsPrevFilter(a_itm, opts, itms.first) - itms.first;
                    }
                    else
                    {
                        var nI = opts.items.visible - itms.first;
                    }
                    debug(conf, 'Preventing non-circular: sliding '+nI+' items backward.');
                    $cfs.trigger(cf_e('prev', conf), nI);
                }

                opts.items.visible = cf_getItemsAdjust(vI, opts, opts.items.visibleConf.adjust, $tt0);
                opts.items.visibleConf.old = opts.items.visible;
                opts = in_getAlignPadding(opts, a_itm);

                var sz = sz_setSizes($cfs, opts);
                $cfs.trigger(cf_e('updatePageStatus', conf), [true, sz]);
                nv_showNavi(opts, itms.total, conf);
                nv_enableNavi(opts, itms.first, conf);

                return sz;
            });


            //  destroy event
            $cfs.bind(cf_e('destroy', conf), function(e, orgOrder) {
                e.stopPropagation();
                tmrs = sc_clearTimers(tmrs);

                $cfs.data('_cfs_isCarousel', false);
                $cfs.trigger(cf_e('finish', conf));
                if (orgOrder)
                {
                    $cfs.trigger(cf_e('jumpToStart', conf));
                }
                sz_restoreOrigCss($cfs.children());
                sz_restoreOrigCss($cfs);
                FN._unbind_events();
                FN._unbind_buttons();
                if (conf.wrapper == 'parent')
                {
                    sz_restoreOrigCss($wrp);
                }
                else
                {
                    $wrp.replaceWith($cfs);
                }

                return true;
            });


            //  debug event
            $cfs.bind(cf_e('debug', conf), function(e) {
                debug(conf, 'Carousel width: ' + opts.width);
                debug(conf, 'Carousel height: ' + opts.height);
                debug(conf, 'Item widths: ' + opts.items.width);
                debug(conf, 'Item heights: ' + opts.items.height);
                debug(conf, 'Number of items visible: ' + opts.items.visible);
                if (opts.auto.play)
                {
                    debug(conf, 'Number of items scrolled automatically: ' + opts.auto.items);
                }
                if (opts.prev.button)
                {
                    debug(conf, 'Number of items scrolled backward: ' + opts.prev.items);
                }
                if (opts.next.button)
                {
                    debug(conf, 'Number of items scrolled forward: ' + opts.next.items);
                }
                return conf.debug;
            });


            //  triggerEvent, making prefixed and namespaced events accessible from outside
            $cfs.bind('_cfs_triggerEvent', function(e, n, o) {
                e.stopPropagation();
                return $cfs.triggerHandler(cf_e(n, conf), o);
            });
        };  //  /bind_events


        FN._unbind_events = function() {
            $cfs.unbind(cf_e('', conf));
            $cfs.unbind(cf_e('', conf, false));
            $cfs.unbind('_cfs_triggerEvent');
        };  //  /unbind_events


        FN._bind_buttons = function() {
            FN._unbind_buttons();
            nv_showNavi(opts, itms.total, conf);
            nv_enableNavi(opts, itms.first, conf);

            if (opts.auto.pauseOnHover)
            {
                var pC = bt_pauseOnHoverConfig(opts.auto.pauseOnHover);
                $wrp.bind(cf_e('mouseenter', conf, false), function() { $cfs.trigger(cf_e('pause', conf), pC);  })
                    .bind(cf_e('mouseleave', conf, false), function() { $cfs.trigger(cf_e('resume', conf));     });
            }

            //  play button
            if (opts.auto.button)
            {
                opts.auto.button.bind(cf_e(opts.auto.event, conf, false), function(e) {
                    e.preventDefault();
                    var ev = false,
                        pC = null;

                    if (crsl.isPaused)
                    {
                        ev = 'play';
                    }
                    else if (opts.auto.pauseOnEvent)
                    {
                        ev = 'pause';
                        pC = bt_pauseOnHoverConfig(opts.auto.pauseOnEvent);
                    }
                    if (ev)
                    {
                        $cfs.trigger(cf_e(ev, conf), pC);
                    }
                });
            }

            //  prev button
            if (opts.prev.button)
            {
                opts.prev.button.bind(cf_e(opts.prev.event, conf, false), function(e) {
                    e.preventDefault();
                    $cfs.trigger(cf_e('prev', conf));
                });
                if (opts.prev.pauseOnHover)
                {
                    var pC = bt_pauseOnHoverConfig(opts.prev.pauseOnHover);
                    opts.prev.button.bind(cf_e('mouseenter', conf, false), function() { $cfs.trigger(cf_e('pause', conf), pC);  })
                                    .bind(cf_e('mouseleave', conf, false), function() { $cfs.trigger(cf_e('resume', conf));     });
                }
            }

            //  next butotn
            if (opts.next.button)
            {
                opts.next.button.bind(cf_e(opts.next.event, conf, false), function(e) {
                    e.preventDefault();
                    $cfs.trigger(cf_e('next', conf));
                });
                if (opts.next.pauseOnHover)
                {
                    var pC = bt_pauseOnHoverConfig(opts.next.pauseOnHover);
                    opts.next.button.bind(cf_e('mouseenter', conf, false), function() { $cfs.trigger(cf_e('pause', conf), pC);  })
                                    .bind(cf_e('mouseleave', conf, false), function() { $cfs.trigger(cf_e('resume', conf));     });
                }
            }

            //  pagination
            if (opts.pagination.container)
            {
                if (opts.pagination.pauseOnHover)
                {
                    var pC = bt_pauseOnHoverConfig(opts.pagination.pauseOnHover);
                    opts.pagination.container.bind(cf_e('mouseenter', conf, false), function() { $cfs.trigger(cf_e('pause', conf), pC); })
                                             .bind(cf_e('mouseleave', conf, false), function() { $cfs.trigger(cf_e('resume', conf));    });
                }
            }

            //  prev/next keys
            if (opts.prev.key || opts.next.key)
            {
                $(document).bind(cf_e('keyup', conf, false, true, true), function(e) {
                    var k = e.keyCode;
                    if (k == opts.next.key)
                    {
                        e.preventDefault();
                        $cfs.trigger(cf_e('next', conf));
                    }
                    if (k == opts.prev.key)
                    {
                        e.preventDefault();
                        $cfs.trigger(cf_e('prev', conf));
                    }
                });
            }

            //  pagination keys
            if (opts.pagination.keys)
            {
                $(document).bind(cf_e('keyup', conf, false, true, true), function(e) {
                    var k = e.keyCode;
                    if (k >= 49 && k < 58)
                    {
                        k = (k-49) * opts.items.visible;
                        if (k <= itms.total)
                        {
                            e.preventDefault();
                            $cfs.trigger(cf_e('slideTo', conf), [k, 0, true, opts.pagination]);
                        }
                    }
                });
            }

            //  swipe
            if ($.fn.swipe)
            {
                var isTouch = 'ontouchstart' in window;
                if ((isTouch && opts.swipe.onTouch) || (!isTouch && opts.swipe.onMouse))
                {
                    var scP = $.extend(true, {}, opts.prev, opts.swipe),
                        scN = $.extend(true, {}, opts.next, opts.swipe),
                        swP = function() { $cfs.trigger(cf_e('prev', conf), [scP]) },
                        swN = function() { $cfs.trigger(cf_e('next', conf), [scN]) };

                    switch (opts.direction)
                    {
                        case 'up':
                        case 'down':
                            opts.swipe.options.swipeUp = swN;
                            opts.swipe.options.swipeDown = swP;
                            break;
                        default:
                            opts.swipe.options.swipeLeft = swN;
                            opts.swipe.options.swipeRight = swP;
                    }
                    if (crsl.swipe)
                    {
                        $cfs.swipe('destroy');
                    }
                    $wrp.swipe(opts.swipe.options);
                    $wrp.css('cursor', 'move');
                    crsl.swipe = true;
                }
            }

            //  mousewheel
            if ($.fn.mousewheel)
            {

                if (opts.mousewheel)
                {
                    var mcP = $.extend(true, {}, opts.prev, opts.mousewheel),
                        mcN = $.extend(true, {}, opts.next, opts.mousewheel);

                    if (crsl.mousewheel)
                    {
                        $wrp.unbind(cf_e('mousewheel', conf, false));
                    }
                    $wrp.bind(cf_e('mousewheel', conf, false), function(e, delta) { 
                        e.preventDefault();
                        if (delta > 0)
                        {
                            $cfs.trigger(cf_e('prev', conf), [mcP]);
                        }
                        else
                        {
                            $cfs.trigger(cf_e('next', conf), [mcN]);
                        }
                    });
                    crsl.mousewheel = true;
                }
            }

            if (opts.auto.play)
            {
                $cfs.trigger(cf_e('play', conf), opts.auto.delay);
            }

            if (crsl.upDateOnWindowResize)
            {
                var resizeFn = function(e) {
                    $cfs.trigger(cf_e('finish', conf));
                    if (opts.auto.pauseOnResize && !crsl.isPaused)
                    {
                        $cfs.trigger(cf_e('play', conf));
                    }
                    sz_resetMargin($cfs.children(), opts);
                    $cfs.trigger(cf_e('updateSizes', conf));
                };

                var $w = $(window),
                    onResize = null;

                if ($.debounce && conf.onWindowResize == 'debounce')
                {
                    onResize = $.debounce(200, resizeFn);
                }
                else if ($.throttle && conf.onWindowResize == 'throttle')
                {
                    onResize = $.throttle(300, resizeFn);
                }
                else
                {
                    var _windowWidth = 0,
                        _windowHeight = 0;

                    onResize = function() {
                        var nw = $w.width(),
                            nh = $w.height();

                        if (nw != _windowWidth || nh != _windowHeight)
                        {
                            resizeFn();
                            _windowWidth = nw;
                            _windowHeight = nh;
                        }
                    };
                }
                $w.bind(cf_e('resize', conf, false, true, true), onResize);
            }
        };  //  /bind_buttons


        FN._unbind_buttons = function() {
            var ns1 = cf_e('', conf),
                ns2 = cf_e('', conf, false);
                ns3 = cf_e('', conf, false, true, true);

            $(document).unbind(ns3);
            $(window).unbind(ns3);
            $wrp.unbind(ns2);

            if (opts.auto.button)
            {
                opts.auto.button.unbind(ns2);
            }
            if (opts.prev.button)
            {
                opts.prev.button.unbind(ns2);
            }
            if (opts.next.button)
            {
                opts.next.button.unbind(ns2);
            }
            if (opts.pagination.container)
            {
                opts.pagination.container.unbind(ns2);
                if (opts.pagination.anchorBuilder)
                {
                    opts.pagination.container.children().remove();
                }
            }
            if (crsl.swipe)
            {
                $cfs.swipe('destroy');
                $wrp.css('cursor', 'default');
                crsl.swipe = false;
            }
            if (crsl.mousewheel)
            {
                crsl.mousewheel = false;
            }

            nv_showNavi(opts, 'hide', conf);
            nv_enableNavi(opts, 'removeClass', conf);

        };  //  /unbind_buttons



        //  START

        if (is_boolean(configs))
        {
            configs = {
                'debug': configs
            };
        }

        //  set vars
        var crsl = {
                'direction'     : 'next',
                'isPaused'      : true,
                'isScrolling'   : false,
                'isStopped'     : false,
                'mousewheel'    : false,
                'swipe'         : false
            },
            itms = {
                'total'         : $cfs.children().length,
                'first'         : 0
            },
            tmrs = {
                'auto'          : null,
                'progress'      : null,
                'startTime'     : getTime(),
                'timePassed'    : 0
            },
            scrl = {
                'isStopped'     : false,
                'duration'      : 0,
                'startTime'     : 0,
                'easing'        : '',
                'anims'         : []
            },
            clbk = {
                'onBefore'      : [],
                'onAfter'       : []
            },
            queu = [],
            conf = $.extend(true, {}, $.fn.carouFredSel.configs, configs),
            opts = {},
            opts_orig = $.extend(true, {}, options),
            $wrp = (conf.wrapper == 'parent')
                ? $cfs.parent()
                : $cfs.wrap('<'+conf.wrapper.element+' class="'+conf.wrapper.classname+'" />').parent();


        conf.selector       = $cfs.selector;
        conf.serialNumber   = $.fn.carouFredSel.serialNumber++;

        conf.transition = (conf.transition && $.fn.transition) ? 'transition' : 'animate';

        //  create carousel
        FN._init(opts_orig, true, starting_position);
        FN._build();
        FN._bind_events();
        FN._bind_buttons();

        //  find item to start
        if (is_array(opts.items.start))
        {
            var start_arr = opts.items.start;
        }
        else
        {
            var start_arr = [];
            if (opts.items.start != 0)
            {
                start_arr.push(opts.items.start);
            }
        }
        if (opts.cookie)
        {
            start_arr.unshift(parseInt(cf_getCookie(opts.cookie), 10));
        }

        if (start_arr.length > 0)
        {
            for (var a = 0, l = start_arr.length; a < l; a++)
            {
                var s = start_arr[a];
                if (s == 0)
                {
                    continue;
                }
                if (s === true)
                {
                    s = window.location.hash;
                    if (s.length < 1)
                    {
                        continue;
                    }
                }
                else if (s === 'random')
                {
                    s = Math.floor(Math.random()*itms.total);
                }
                if ($cfs.triggerHandler(cf_e('slideTo', conf), [s, 0, true, { fx: 'none' }]))
                {
                    break;
                }
            }
        }
        var siz = sz_setSizes($cfs, opts),
            itm = gi_getCurrentItems($cfs.children(), opts);

        if (opts.onCreate)
        {
            opts.onCreate.call($tt0, {
                'width': siz.width,
                'height': siz.height,
                'items': itm
            });
        }

        $cfs.trigger(cf_e('updatePageStatus', conf), [true, siz]);
        $cfs.trigger(cf_e('linkAnchors', conf));

        if (conf.debug)
        {
            $cfs.trigger(cf_e('debug', conf));
        }

        return $cfs;
    };



    //  GLOBAL PUBLIC

    $.fn.carouFredSel.serialNumber = 1;
    $.fn.carouFredSel.defaults = {
        'synchronise'   : false,
        'infinite'      : true,
        'circular'      : true,
        'responsive'    : false,
        'direction'     : 'left',
        'items'         : {
            'start'         : 0
        },
        'scroll'        : {
            'easing'        : 'swing',
            'duration'      : 500,
            'pauseOnHover'  : false,
            'event'         : 'click',
            'queue'         : false
        }
    };
    $.fn.carouFredSel.configs = {
        'debug'         : false,
        'transition'    : false,
        'onWindowResize': 'throttle',
        'events'        : {
            'prefix'        : '',
            'namespace'     : 'cfs'
        },
        'wrapper'       : {
            'element'       : 'div',
            'classname'     : 'caroufredsel_wrapper'
        },
        'classnames'    : {}
    };
    $.fn.carouFredSel.pageAnchorBuilder = function(nr) {
        return '<a href="#"><span>'+nr+'</span></a>';
    };
    $.fn.carouFredSel.progressbarUpdater = function(perc) {
        $(this).css('width', perc+'%');
    };

    $.fn.carouFredSel.cookie = {
        get: function(n) {
            n += '=';
            var ca = document.cookie.split(';');
            for (var a = 0, l = ca.length; a < l; a++)
            {
                var c = ca[a];
                while (c.charAt(0) == ' ')
                {
                    c = c.slice(1);
                }
                if (c.indexOf(n) == 0)
                {
                    return c.slice(n.length);
                }
            }
            return 0;
        },
        set: function(n, v, d) {
            var e = "";
            if (d)
            {
                var date = new Date();
                date.setTime(date.getTime() + (d * 24 * 60 * 60 * 1000));
                e = "; expires=" + date.toGMTString();
            }
            document.cookie = n + '=' + v + e + '; path=/';
        },
        remove: function(n) {
            $.fn.carouFredSel.cookie.set(n, "", -1);
        }
    };


    //  GLOBAL PRIVATE

    //  scrolling functions
    function sc_setScroll(d, e, c) {
        if (c.transition == 'transition')
        {
            if (e == 'swing')
            {
                e = 'ease';
            }
        }
        return {
            anims: [],
            duration: d,
            orgDuration: d,
            easing: e,
            startTime: getTime()
        };
    }
    function sc_startScroll(s, c) {
        for (var a = 0, l = s.anims.length; a < l; a++)
        {
            var b = s.anims[a];
            if (!b)
            {
                continue;
            }
            b[0][c.transition](b[1], s.duration, s.easing, b[2]);
        }
    }
    function sc_stopScroll(s, finish) {
        if (!is_boolean(finish))
        {
            finish = true;
        }
        if (is_object(s.pre))
        {
            sc_stopScroll(s.pre, finish);
        }
        for (var a = 0, l = s.anims.length; a < l; a++)
        {
            var b = s.anims[a];
            b[0].stop(true);
            if (finish)
            {
                b[0].css(b[1]);
                if (is_function(b[2]))
                {
                    b[2]();
                }
            }
        }
        if (is_object(s.post))
        {
            sc_stopScroll(s.post, finish);
        }
    }
    function sc_afterScroll( $c, $c2, o ) {
        if ($c2)
        {
            $c2.remove();
        }

        switch(o.fx) {
            case 'fade':
            case 'crossfade':
            case 'cover-fade':
            case 'uncover-fade':
                $c.css('opacity', 1);
                $c.css('filter', '');
                break;
        }
    }
    function sc_fireCallbacks($t, o, b, a, c) {
        if (o[b])
        {
            o[b].call($t, a);
        }
        if (c[b].length)
        {
            for (var i = 0, l = c[b].length; i < l; i++)
            {
                c[b][i].call($t, a);
            }
        }
        return [];
    }
    function sc_fireQueue($c, q, c) {

        if (q.length)
        {
            $c.trigger(cf_e(q[0][0], c), q[0][1]);
            q.shift();
        }
        return q;
    }
    function sc_hideHiddenItems(hiddenitems) {
        hiddenitems.each(function() {
            var hi = $(this);
            hi.data('_cfs_isHidden', hi.is(':hidden')).hide();
        });
    }
    function sc_showHiddenItems(hiddenitems) {
        if (hiddenitems)
        {
            hiddenitems.each(function() {
                var hi = $(this);
                if (!hi.data('_cfs_isHidden'))
                {
                    hi.show();
                }
            });
        }
    }
    function sc_clearTimers(t) {
        if (t.auto)
        {
            clearTimeout(t.auto);
        }
        if (t.progress)
        {
            clearInterval(t.progress);
        }
        return t;
    }
    function sc_mapCallbackArguments(i_old, i_skp, i_new, s_itm, s_dir, s_dur, w_siz) {
        return {
            'width': w_siz.width,
            'height': w_siz.height,
            'items': {
                'old': i_old,
                'skipped': i_skp,
                'visible': i_new
            },
            'scroll': {
                'items': s_itm,
                'direction': s_dir,
                'duration': s_dur
            }
        };
    }
    function sc_getDuration( sO, o, nI, siz ) {
        var dur = sO.duration;
        if (sO.fx == 'none')
        {
            return 0;
        }
        if (dur == 'auto')
        {
            dur = o.scroll.duration / o.scroll.items * nI;
        }
        else if (dur < 10)
        {
            dur = siz / dur;
        }
        if (dur < 1)
        {
            return 0;
        }
        if (sO.fx == 'fade')
        {
            dur = dur / 2;
        }
        return Math.round(dur);
    }

    //  navigation functions
    function nv_showNavi(o, t, c) {
        var minimum = (is_number(o.items.minimum)) ? o.items.minimum : o.items.visible + 1;
        if (t == 'show' || t == 'hide')
        {
            var f = t;
        }
        else if (minimum > t)
        {
            debug(c, 'Not enough items ('+t+' total, '+minimum+' needed): Hiding navigation.');
            var f = 'hide';
        }
        else
        {
            var f = 'show';
        }
        var s = (f == 'show') ? 'removeClass' : 'addClass',
            h = cf_c('hidden', c);

        if (o.auto.button)
        {
            o.auto.button[f]()[s](h);
        }
        if (o.prev.button)
        {
            o.prev.button[f]()[s](h);
        }
        if (o.next.button)
        {
            o.next.button[f]()[s](h);
        }
        if (o.pagination.container)
        {
            o.pagination.container[f]()[s](h);
        }
    }
    function nv_enableNavi(o, f, c) {
        if (o.circular || o.infinite) return;
        var fx = (f == 'removeClass' || f == 'addClass') ? f : false,
            di = cf_c('disabled', c);

        if (o.auto.button && fx)
        {
            o.auto.button[fx](di);
        }
        if (o.prev.button)
        {
            var fn = fx || (f == 0) ? 'addClass' : 'removeClass';
            o.prev.button[fn](di);
        }
        if (o.next.button)
        {
            var fn = fx || (f == o.items.visible) ? 'addClass' : 'removeClass';
            o.next.button[fn](di);
        }
    }

    //  get object functions
    function go_getObject($tt, obj) {
        if (is_function(obj))
        {
            obj = obj.call($tt);
        }
        else if (is_undefined(obj))
        {
            obj = {};
        }
        return obj;
    }
    function go_getItemsObject($tt, obj) {
        obj = go_getObject($tt, obj);
        if (is_number(obj))
        {
            obj = {
                'visible': obj
            };
        }
        else if (obj == 'variable')
        {
            obj = {
                'visible': obj,
                'width': obj, 
                'height': obj
            };
        }
        else if (!is_object(obj))
        {
            obj = {};
        }
        return obj;
    }
    function go_getScrollObject($tt, obj) {
        obj = go_getObject($tt, obj);
        if (is_number(obj))
        {
            if (obj <= 50)
            {
                obj = {
                    'items': obj
                };
            }
            else
            {
                obj = {
                    'duration': obj
                };
            }
        }
        else if (is_string(obj))
        {
            obj = {
                'easing': obj
            };
        }
        else if (!is_object(obj))
        {
            obj = {};
        }
        return obj;
    }
    function go_getNaviObject($tt, obj) {
        obj = go_getObject($tt, obj);
        if (is_string(obj))
        {
            var temp = cf_getKeyCode(obj);
            if (temp == -1)
            {
                obj = $(obj);
            }
            else
            {
                obj = temp;
            }
        }
        return obj;
    }

    function go_getAutoObject($tt, obj) {
        obj = go_getNaviObject($tt, obj);
        if (is_jquery(obj))
        {
            obj = {
                'button': obj
            };
        }
        else if (is_boolean(obj))
        {
            obj = {
                'play': obj
            };
        }
        else if (is_number(obj))
        {
            obj = {
                'timeoutDuration': obj
            };
        }
        if (obj.progress)
        {
            if (is_string(obj.progress) || is_jquery(obj.progress))
            {
                obj.progress = {
                    'bar': obj.progress
                };
            }
        }
        return obj;
    }
    function go_complementAutoObject($tt, obj) {
        if (is_function(obj.button))
        {
            obj.button = obj.button.call($tt);
        }
        if (is_string(obj.button))
        {
            obj.button = $(obj.button);
        }
        if (!is_boolean(obj.play))
        {
            obj.play = true;
        }
        if (!is_number(obj.delay))
        {
            obj.delay = 0;
        }
        if (is_undefined(obj.pauseOnEvent))
        {
            obj.pauseOnEvent = true;
        }
        if (!is_boolean(obj.pauseOnResize))
        {
            obj.pauseOnResize = true;
        }
        if (!is_number(obj.timeoutDuration))
        {
            obj.timeoutDuration = (obj.duration < 10)
                ? 2500
                : obj.duration * 5;
        }
        if (obj.progress)
        {
            if (is_function(obj.progress.bar))
            {
                obj.progress.bar = obj.progress.bar.call($tt);
            }
            if (is_string(obj.progress.bar))
            {
                obj.progress.bar = $(obj.progress.bar);
            }
            if (obj.progress.bar)
            {
                if (!is_function(obj.progress.updater))
                {
                    obj.progress.updater = $.fn.carouFredSel.progressbarUpdater;
                }
                if (!is_number(obj.progress.interval))
                {
                    obj.progress.interval = 50;
                }
            }
            else
            {
                obj.progress = false;
            }
        }
        return obj;
    }

    function go_getPrevNextObject($tt, obj) {
        obj = go_getNaviObject($tt, obj);
        if (is_jquery(obj))
        {
            obj = {
                'button': obj
            };
        }
        else if (is_number(obj))
        {
            obj = {
                'key': obj
            };
        }
        return obj;
    }
    function go_complementPrevNextObject($tt, obj) {
        if (is_function(obj.button))
        {
            obj.button = obj.button.call($tt);
        }
        if (is_string(obj.button))
        {
            obj.button = $(obj.button);
        }
        if (is_string(obj.key))
        {
            obj.key = cf_getKeyCode(obj.key);
        }
        return obj;
    }

    function go_getPaginationObject($tt, obj) {
        obj = go_getNaviObject($tt, obj);
        if (is_jquery(obj))
        {
            obj = {
                'container': obj
            };
        }
        else if (is_boolean(obj))
        {
            obj = {
                'keys': obj
            };
        }
        return obj;
    }
    function go_complementPaginationObject($tt, obj) {
        if (is_function(obj.container))
        {
            obj.container = obj.container.call($tt);
        }
        if (is_string(obj.container))
        {
            obj.container = $(obj.container);
        }
        if (!is_number(obj.items))
        {
            obj.items = false;
        }
        if (!is_boolean(obj.keys))
        {
            obj.keys = false;
        }
        if (!is_function(obj.anchorBuilder) && !is_false(obj.anchorBuilder))
        {
            obj.anchorBuilder = $.fn.carouFredSel.pageAnchorBuilder;
        }
        if (!is_number(obj.deviation))
        {
            obj.deviation = 0;
        }
        return obj;
    }

    function go_getSwipeObject($tt, obj) {
        if (is_function(obj))
        {
            obj = obj.call($tt);
        }
        if (is_undefined(obj))
        {
            obj = {
                'onTouch': false
            };
        }
        if (is_true(obj))
        {
            obj = {
                'onTouch': obj
            };
        }
        else if (is_number(obj))
        {
            obj = {
                'items': obj
            };
        }
        return obj;
    }
    function go_complementSwipeObject($tt, obj) {
        if (!is_boolean(obj.onTouch))
        {
            obj.onTouch = true;
        }
        if (!is_boolean(obj.onMouse))
        {
            obj.onMouse = false;
        }
        if (!is_object(obj.options))
        {
            obj.options = {};
        }
        if (!is_boolean(obj.options.triggerOnTouchEnd))
        {
            obj.options.triggerOnTouchEnd = false;
        }
        return obj;
    }
    function go_getMousewheelObject($tt, obj) {
        if (is_function(obj))
        {
            obj = obj.call($tt);
        }
        if (is_true(obj))
        {
            obj = {};
        }
        else if (is_number(obj))
        {
            obj = {
                'items': obj
            };
        }
        else if (is_undefined(obj))
        {
            obj = false;
        }
        return obj;
    }
    function go_complementMousewheelObject($tt, obj) {
        return obj;
    }

    //  get number functions
    function gn_getItemIndex(num, dev, org, items, $cfs) {
        if (is_string(num))
        {
            num = $(num, $cfs);
        }

        if (is_object(num))
        {
            num = $(num, $cfs);
        }
        if (is_jquery(num))
        {
            num = $cfs.children().index(num);
            if (!is_boolean(org))
            {
                org = false;
            }
        }
        else
        {
            if (!is_boolean(org))
            {
                org = true;
            }
        }
        if (!is_number(num))
        {
            num = 0;
        }
        if (!is_number(dev))
        {
            dev = 0;
        }

        if (org)
        {
            num += items.first;
        }
        num += dev;
        if (items.total > 0)
        {
            while (num >= items.total)
            {
                num -= items.total;
            }
            while (num < 0)
            {
                num += items.total;
            }
        }
        return num;
    }

    //  items prev
    function gn_getVisibleItemsPrev(i, o, s) {
        var t = 0,
            x = 0;

        for (var a = s; a >= 0; a--)
        {
            var j = i.eq(a);
            t += (j.is(':visible')) ? j[o.d['outerWidth']](true) : 0;
            if (t > o.maxDimension)
            {
                return x;
            }
            if (a == 0)
            {
                a = i.length;
            }
            x++;
        }
    }
    function gn_getVisibleItemsPrevFilter(i, o, s) {
        return gn_getItemsPrevFilter(i, o.items.filter, o.items.visibleConf.org, s);
    }
    function gn_getScrollItemsPrevFilter(i, o, s, m) {
        return gn_getItemsPrevFilter(i, o.items.filter, m, s);
    }
    function gn_getItemsPrevFilter(i, f, m, s) {
        var t = 0,
            x = 0;

        for (var a = s, l = i.length; a >= 0; a--)
        {
            x++;
            if (x == l)
            {
                return x;
            }

            var j = i.eq(a);
            if (j.is(f))
            {
                t++;
                if (t == m)
                {
                    return x;
                }
            }
            if (a == 0)
            {
                a = l;
            }
        }
    }

    function gn_getVisibleOrg($c, o) {
        return o.items.visibleConf.org || $c.children().slice(0, o.items.visible).filter(o.items.filter).length;
    }

    //  items next
    function gn_getVisibleItemsNext(i, o, s) {
        var t = 0,
            x = 0;

        for (var a = s, l = i.length-1; a <= l; a++)
        {
            var j = i.eq(a);

            t += (j.is(':visible')) ? j[o.d['outerWidth']](true) : 0;
            if (t > o.maxDimension)
            {
                return x;
            }

            x++;
            if (x == l+1)
            {
                return x;
            }
            if (a == l)
            {
                a = -1;
            }
        }
    }
    function gn_getVisibleItemsNextTestCircular(i, o, s, l) {
        var v = gn_getVisibleItemsNext(i, o, s);
        if (!o.circular)
        {
            if (s + v > l)
            {
                v = l - s;
            }
        }
        return v;
    }
    function gn_getVisibleItemsNextFilter(i, o, s) {
        return gn_getItemsNextFilter(i, o.items.filter, o.items.visibleConf.org, s, o.circular);
    }
    function gn_getScrollItemsNextFilter(i, o, s, m) {
        return gn_getItemsNextFilter(i, o.items.filter, m+1, s, o.circular) - 1;
    }
    function gn_getItemsNextFilter(i, f, m, s, c) {
        var t = 0,
            x = 0;

        for (var a = s, l = i.length-1; a <= l; a++)
        {
            x++;
            if (x >= l)
            {
                return x;
            }

            var j = i.eq(a);
            if (j.is(f))
            {
                t++;
                if (t == m)
                {
                    return x;
                }
            }
            if (a == l)
            {
                a = -1;
            }
        }
    }

    //  get items functions
    function gi_getCurrentItems(i, o) {
        return i.slice(0, o.items.visible);
    }
    function gi_getOldItemsPrev(i, o, n) {
        return i.slice(n, o.items.visibleConf.old+n);
    }
    function gi_getNewItemsPrev(i, o) {
        return i.slice(0, o.items.visible);
    }
    function gi_getOldItemsNext(i, o) {
        return i.slice(0, o.items.visibleConf.old);
    }
    function gi_getNewItemsNext(i, o, n) {
        return i.slice(n, o.items.visible+n);
    }

    //  sizes functions
    function sz_storeMargin(i, o, d) {
        if (o.usePadding)
        {
            if (!is_string(d))
            {
                d = '_cfs_origCssMargin';
            }
            i.each(function() {
                var j = $(this),
                    m = parseInt(j.css(o.d['marginRight']), 10);
                if (!is_number(m)) 
                {
                    m = 0;
                }
                j.data(d, m);
            });
        }
    }
    function sz_resetMargin(i, o, m) {
        if (o.usePadding)
        {
            var x = (is_boolean(m)) ? m : false;
            if (!is_number(m))
            {
                m = 0;
            }
            sz_storeMargin(i, o, '_cfs_tempCssMargin');
            i.each(function() {
                var j = $(this);
                j.css(o.d['marginRight'], ((x) ? j.data('_cfs_tempCssMargin') : m + j.data('_cfs_origCssMargin')));
            });
        }
    }
    function sz_storeOrigCss(i) {
        i.each(function() {
            var j = $(this);
            j.data('_cfs_origCss', j.attr('style') || '');
        });
    }
    function sz_restoreOrigCss(i) {
        i.each(function() {
            var j = $(this);
            j.attr('style', j.data('_cfs_origCss') || '');
        });
    }
    function sz_setResponsiveSizes(o, all) {
        var visb = o.items.visible,
            newS = o.items[o.d['width']],
            seco = o[o.d['height']],
            secp = is_percentage(seco);

        all.each(function() {
            var $t = $(this),
                nw = newS - ms_getPaddingBorderMargin($t, o, 'Width');

            $t[o.d['width']](nw);
            if (secp)
            {
                $t[o.d['height']](ms_getPercentage(nw, seco));
            }
        });
    }
    function sz_setSizes($c, o) {
        var $w = $c.parent(),
            $i = $c.children(),
            $v = gi_getCurrentItems($i, o),
            sz = cf_mapWrapperSizes(ms_getSizes($v, o, true), o, false);

        $w.css(sz);

        if (o.usePadding)
        {
            var p = o.padding,
                r = p[o.d[1]];

            if (o.align && r < 0)
            {
                r = 0;
            }
            var $l = $v.last();
            $l.css(o.d['marginRight'], $l.data('_cfs_origCssMargin') + r);
            $c.css(o.d['top'], p[o.d[0]]);
            $c.css(o.d['left'], p[o.d[3]]);
        }

        $c.css(o.d['width'], sz[o.d['width']]+(ms_getTotalSize($i, o, 'width')*2));
        $c.css(o.d['height'], ms_getLargestSize($i, o, 'height'));
        return sz;
    }

    //  measuring functions
    function ms_getSizes(i, o, wrapper) {
        return [ms_getTotalSize(i, o, 'width', wrapper), ms_getLargestSize(i, o, 'height', wrapper)];
    }
    function ms_getLargestSize(i, o, dim, wrapper) {
        if (!is_boolean(wrapper))
        {
            wrapper = false;
        }
        if (is_number(o[o.d[dim]]) && wrapper)
        {
            return o[o.d[dim]];
        }
        if (is_number(o.items[o.d[dim]]))
        {
            return o.items[o.d[dim]];
        }
        dim = (dim.toLowerCase().indexOf('width') > -1) ? 'outerWidth' : 'outerHeight';
        return ms_getTrueLargestSize(i, o, dim);
    }
    function ms_getTrueLargestSize(i, o, dim) {
        var s = 0;

        for (var a = 0, l = i.length; a < l; a++)
        {
            var j = i.eq(a);

            var m = (j.is(':visible')) ? j[o.d[dim]](true) : 0;
            if (s < m)
            {
                s = m;
            }
        }
        return s;
    }

    function ms_getTotalSize(i, o, dim, wrapper) {
        if (!is_boolean(wrapper))
        {
            wrapper = false;
        }
        if (is_number(o[o.d[dim]]) && wrapper)
        {
            return o[o.d[dim]];
        }
        if (is_number(o.items[o.d[dim]]))
        {
            return o.items[o.d[dim]] * i.length;
        }

        var d = (dim.toLowerCase().indexOf('width') > -1) ? 'outerWidth' : 'outerHeight',
            s = 0;

        for (var a = 0, l = i.length; a < l; a++)
        {
            var j = i.eq(a);
            s += (j.is(':visible')) ? j[o.d[d]](true) : 0;
        }
        return s;
    }
    function ms_getParentSize($w, o, d) {
        var isVisible = $w.is(':visible');
        if (isVisible)
        {
            $w.hide();
        }
        var s = $w.parent()[o.d[d]]();
        if (isVisible)
        {
            $w.show();
        }
        return s;
    }
    function ms_getMaxDimension(o, a) {
        return (is_number(o[o.d['width']])) ? o[o.d['width']] : a;
    }
    function ms_hasVariableSizes(i, o, dim) {
        var s = false,
            v = false;

        for (var a = 0, l = i.length; a < l; a++)
        {
            var j = i.eq(a);

            var c = (j.is(':visible')) ? j[o.d[dim]](true) : 0;
            if (s === false)
            {
                s = c;
            }
            else if (s != c)
            {
                v = true;
            }
            if (s == 0)
            {
                v = true;
            }
        }
        return v;
    }
    function ms_getPaddingBorderMargin(i, o, d) {
        return i[o.d['outer'+d]](true) - i[o.d[d.toLowerCase()]]();
    }
    function ms_getPercentage(s, o) {
        if (is_percentage(o))
        {
            o = parseInt( o.slice(0, -1), 10 );
            if (!is_number(o))
            {
                return s;
            }
            s *= o/100;
        }
        return s;
    }

    //  config functions
    function cf_e(n, c, pf, ns, rd) {
        if (!is_boolean(pf))
        {
            pf = true;
        }
        if (!is_boolean(ns))
        {
            ns = true;
        }
        if (!is_boolean(rd))
        {
            rd = false;
        }

        if (pf)
        {
            n = c.events.prefix + n;
        }
        if (ns)
        {
            n = n +'.'+ c.events.namespace;
        }
        if (ns && rd)
        {
            n += c.serialNumber;
        }

        return n;
    }
    function cf_c(n, c) {
        return (is_string(c.classnames[n])) ? c.classnames[n] : n;
    }
    function cf_mapWrapperSizes(ws, o, p) {
        if (!is_boolean(p))
        {
            p = true;
        }
        var pad = (o.usePadding && p) ? o.padding : [0, 0, 0, 0];
        var wra = {};

        wra[o.d['width']] = ws[0] + pad[1] + pad[3];
        wra[o.d['height']] = ws[1] + pad[0] + pad[2];

        return wra;
    }
    function cf_sortParams(vals, typs) {
        var arr = [];
        for (var a = 0, l1 = vals.length; a < l1; a++)
        {
            for (var b = 0, l2 = typs.length; b < l2; b++)
            {
                if (typs[b].indexOf(typeof vals[a]) > -1 && is_undefined(arr[b]))
                {
                    arr[b] = vals[a];
                    break;
                }
            }
        }
        return arr;
    }
    function cf_getPadding(p) {
        if (is_undefined(p))
        {
            return [0, 0, 0, 0];
        }
        if (is_number(p))
        {
            return [p, p, p, p];
        }
        if (is_string(p))
        {
            p = p.split('px').join('').split('em').join('').split(' ');
        }

        if (!is_array(p))
        {
            return [0, 0, 0, 0];
        }
        for (var i = 0; i < 4; i++)
        {
            p[i] = parseInt(p[i], 10);
        }
        switch (p.length)
        {
            case 0:
                return [0, 0, 0, 0];
            case 1:
                return [p[0], p[0], p[0], p[0]];
            case 2:
                return [p[0], p[1], p[0], p[1]];
            case 3:
                return [p[0], p[1], p[2], p[1]];
            default:
                return [p[0], p[1], p[2], p[3]];
        }
    }
    function cf_getAlignPadding(itm, o) {
        var x = (is_number(o[o.d['width']])) ? Math.ceil(o[o.d['width']] - ms_getTotalSize(itm, o, 'width')) : 0;
        switch (o.align)
        {
            case 'left': 
                return [0, x];
            case 'right':
                return [x, 0];
            case 'center':
            default:
                return [Math.ceil(x/2), Math.floor(x/2)];
        }
    }
    function cf_getDimensions(o) {
        var dm = [
                ['width'    , 'innerWidth'  , 'outerWidth'  , 'height'  , 'innerHeight' , 'outerHeight' , 'left', 'top' , 'marginRight' , 0, 1, 2, 3],
                ['height'   , 'innerHeight' , 'outerHeight' , 'width'   , 'innerWidth'  , 'outerWidth'  , 'top' , 'left', 'marginBottom', 3, 2, 1, 0]
            ];

        var dl = dm[0].length,
            dx = (o.direction == 'right' || o.direction == 'left') ? 0 : 1;

        var dimensions = {};
        for (var d = 0; d < dl; d++)
        {
            dimensions[dm[0][d]] = dm[dx][d];
        }
        return dimensions;
    }
    function cf_getAdjust(x, o, a, $t) {
        var v = x;
        if (is_function(a))
        {
            v = a.call($t, v);

        }
        else if (is_string(a))
        {
            var p = a.split('+'),
                m = a.split('-');

            if (m.length > p.length)
            {
                var neg = true,
                    sta = m[0],
                    adj = m[1];
            }
            else
            {
                var neg = false,
                    sta = p[0],
                    adj = p[1];
            }

            switch(sta)
            {
                case 'even':
                    v = (x % 2 == 1) ? x-1 : x;
                    break;
                case 'odd':
                    v = (x % 2 == 0) ? x-1 : x;
                    break;
                default:
                    v = x;
                    break;
            }
            adj = parseInt(adj, 10);
            if (is_number(adj))
            {
                if (neg)
                {
                    adj = -adj;
                }
                v += adj;
            }
        }
        if (!is_number(v) || v < 1)
        {
            v = 1;
        }
        return v;
    }
    function cf_getItemsAdjust(x, o, a, $t) {
        return cf_getItemAdjustMinMax(cf_getAdjust(x, o, a, $t), o.items.visibleConf);
    }
    function cf_getItemAdjustMinMax(v, i) {
        if (is_number(i.min) && v < i.min)
        {
            v = i.min;
        }
        if (is_number(i.max) && v > i.max)
        {
            v = i.max;
        }
        if (v < 1)
        {
            v = 1;
        }
        return v;
    }
    function cf_getSynchArr(s) {
        if (!is_array(s))
        {
            s = [[s]];
        }
        if (!is_array(s[0]))
        {
            s = [s];
        }
        for (var j = 0, l = s.length; j < l; j++)
        {
            if (is_string(s[j][0]))
            {
                s[j][0] = $(s[j][0]);
            }
            if (!is_boolean(s[j][1]))
            {
                s[j][1] = true;
            }
            if (!is_boolean(s[j][2]))
            {
                s[j][2] = true;
            }
            if (!is_number(s[j][3]))
            {
                s[j][3] = 0;
            }
        }
        return s;
    }
    function cf_getKeyCode(k) {
        if (k == 'right')
        {
            return 39;
        }
        if (k == 'left')
        {
            return 37;
        }
        if (k == 'up')
        {
            return 38;
        }
        if (k == 'down')
        {
            return 40;
        }
        return -1;
    }
    function cf_setCookie(n, $c, c) {
        if (n)
        {
            var v = $c.triggerHandler(cf_e('currentPosition', c));
            $.fn.carouFredSel.cookie.set(n, v);
        }
    }
    function cf_getCookie(n) {
        var c = $.fn.carouFredSel.cookie.get(n);
        return (c == '') ? 0 : c;
    }

    //  init function
    function in_mapCss($elem, props) {
        var css = {};
        for (var p = 0, l = props.length; p < l; p++)
        {
            css[props[p]] = $elem.css(props[p]);
        }
        return css;
    }
    function in_complementItems(obj, opt, itm, sta) {
        if (!is_object(obj.visibleConf))
        {
            obj.visibleConf = {};
        }
        if (!is_object(obj.sizesConf))
        {
            obj.sizesConf = {};
        }

        if (obj.start == 0 && is_number(sta))
        {
            obj.start = sta;
        }

        //  visible items
        if (is_object(obj.visible))
        {
            obj.visibleConf.min = obj.visible.min;
            obj.visibleConf.max = obj.visible.max;
            obj.visible = false;
        }
        else if (is_string(obj.visible))
        {
            //  variable visible items
            if (obj.visible == 'variable')
            {
                obj.visibleConf.variable = true;
            }
            //  adjust string visible items
            else
            {
                obj.visibleConf.adjust = obj.visible;
            }
            obj.visible = false;
        }
        else if (is_function(obj.visible))
        {
            obj.visibleConf.adjust = obj.visible;
            obj.visible = false;
        }

        //  set items filter
        if (!is_string(obj.filter))
        {
            obj.filter = (itm.filter(':hidden').length > 0) ? ':visible' : '*';
        }

        //  primary item-size not set
        if (!obj[opt.d['width']])
        {
            //  responsive carousel -> set to largest
            if (opt.responsive)
            {
                debug(true, 'Set a '+opt.d['width']+' for the items!');
                obj[opt.d['width']] = ms_getTrueLargestSize(itm, opt, 'outerWidth');
            }
            //   non-responsive -> measure it or set to "variable"
            else
            {
                obj[opt.d['width']] = (ms_hasVariableSizes(itm, opt, 'outerWidth')) 
                    ? 'variable' 
                    : itm[opt.d['outerWidth']](true);
            }
        }

        //  secondary item-size not set -> measure it or set to "variable"
        if (!obj[opt.d['height']])
        {
            obj[opt.d['height']] = (ms_hasVariableSizes(itm, opt, 'outerHeight')) 
                ? 'variable' 
                : itm[opt.d['outerHeight']](true);
        }

        obj.sizesConf.width = obj.width;
        obj.sizesConf.height = obj.height;
        return obj;
    }
    function in_complementVisibleItems(opt, avl) {
        //  primary item-size variable -> set visible items variable
        if (opt.items[opt.d['width']] == 'variable')
        {
            opt.items.visibleConf.variable = true;
        }
        if (!opt.items.visibleConf.variable) {
            //  primary size is number -> calculate visible-items
            if (is_number(opt[opt.d['width']]))
            {
                opt.items.visible = Math.floor(opt[opt.d['width']] / opt.items[opt.d['width']]);
            }
            //  measure and calculate primary size and visible-items
            else
            {
                opt.items.visible = Math.floor(avl / opt.items[opt.d['width']]);
                opt[opt.d['width']] = opt.items.visible * opt.items[opt.d['width']];
                if (!opt.items.visibleConf.adjust)
                {
                    opt.align = false;
                }
            }
            if (opt.items.visible == 'Infinity' || opt.items.visible < 1)
            {
                debug(true, 'Not a valid number of visible items: Set to "variable".');
                opt.items.visibleConf.variable = true;
            }
        }
        return opt;
    }
    function in_complementPrimarySize(obj, opt, all) {
        //  primary size set to auto -> measure largest item-size and set it
        if (obj == 'auto')
        {
            obj = ms_getTrueLargestSize(all, opt, 'outerWidth');
        }
        return obj;
    }
    function in_complementSecondarySize(obj, opt, all) {
        //  secondary size set to auto -> measure largest item-size and set it
        if (obj == 'auto')
        {
            obj = ms_getTrueLargestSize(all, opt, 'outerHeight');
        }
        //  secondary size not set -> set to secondary item-size
        if (!obj)
        {
            obj = opt.items[opt.d['height']];
        }
        return obj;
    }
    function in_getAlignPadding(o, all) {
        var p = cf_getAlignPadding(gi_getCurrentItems(all, o), o);
        o.padding[o.d[1]] = p[1];
        o.padding[o.d[3]] = p[0];
        return o;
    }
    function in_getResponsiveValues(o, all, avl) {

        var visb = cf_getItemAdjustMinMax(Math.ceil(o[o.d['width']] / o.items[o.d['width']]), o.items.visibleConf);
        if (visb > all.length)
        {
            visb = all.length;
        }

        var newS = Math.floor(o[o.d['width']]/visb);

        o.items.visible = visb;
        o.items[o.d['width']] = newS;
        o[o.d['width']] = visb * newS;
        return o;
    }


    //  buttons functions
    function bt_pauseOnHoverConfig(p) {
        if (is_string(p))
        {
            var i = (p.indexOf('immediate') > -1) ? true : false,
                r = (p.indexOf('resume')    > -1) ? true : false;
        }
        else
        {
            var i = r = false;
        }
        return [i, r];
    }
    function bt_mousesheelNumber(mw) {
        return (is_number(mw)) ? mw : null
    }

    //  helper functions
    function is_null(a) {
        return (a === null);
    }
    function is_undefined(a) {
        return (is_null(a) || typeof a == 'undefined' || a === '' || a === 'undefined');
    }
    function is_array(a) {
        return (a instanceof Array);
    }
    function is_jquery(a) {
        return (a instanceof jQuery);
    }
    function is_object(a) {
        return ((a instanceof Object || typeof a == 'object') && !is_null(a) && !is_jquery(a) && !is_array(a) && !is_function(a));
    }
    function is_number(a) {
        return ((a instanceof Number || typeof a == 'number') && !isNaN(a));
    }
    function is_string(a) {
        return ((a instanceof String || typeof a == 'string') && !is_undefined(a) && !is_true(a) && !is_false(a));
    }
    function is_function(a) {
        return (a instanceof Function || typeof a == 'function');
    }
    function is_boolean(a) {
        return (a instanceof Boolean || typeof a == 'boolean' || is_true(a) || is_false(a));
    }
    function is_true(a) {
        return (a === true || a === 'true');
    }
    function is_false(a) {
        return (a === false || a === 'false');
    }
    function is_percentage(x) {
        return (is_string(x) && x.slice(-1) == '%');
    }


    function getTime() {
        return new Date().getTime();
    }

    function deprecated( o, n ) {
        debug(true, o+' is DEPRECATED, support for it will be removed. Use '+n+' instead.');
    }
    function debug(d, m) {
        if (!is_undefined(window.console) && !is_undefined(window.console.log))
        {
            if (is_object(d))
            {
                var s = ' ('+d.selector+')';
                d = d.debug;
            }
            else
            {
                var s = '';
            }
            if (!d)
            {
                return false;
            }
    
            if (is_string(m))
            {
                m = 'carouFredSel'+s+': ' + m;
            }
            else
            {
                m = ['carouFredSel'+s+':', m];
            }
            window.console.log(m);
        }
        return false;
    }



    //  EASING FUNCTIONS
    $.extend($.easing, {
        'quadratic': function(t) {
            var t2 = t * t;
            return t * (-t2 * t + 4 * t2 - 6 * t + 4);
        },
        'cubic': function(t) {
            return t * (4 * t * t - 9 * t + 6);
        },
        'elastic': function(t) {
            var t2 = t * t;
            return t * (33 * t2 * t2 - 106 * t2 * t + 126 * t2 - 67 * t + 15);
        }
    });


})(jQuery);

/*
 * nyroModal v2.0.0
 * Core
 *
 */
jQuery(function($, undefined) {

    var uaMatch = function(ua) {
        ua = ua.toLowerCase();
        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        return {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };
    },
    matched = uaMatch(navigator.userAgent),
    browser = {};

    if (matched.browser) {
        browser[matched.browser] = true;
        browser.version = matched.version;
    }

    // Chrome is Webkit, but Webkit is also Safari.
    if (browser.chrome) {
        browser.webkit = true;
    } else if (browser.webkit) {
        browser.safari = true;
    }

    var $w = $(window),
        $d = $(document),
        $b = $('body'),
        baseHref = $('base').attr('href'),
        // nyroModal Object
        _nmObj = {
            filters: [],    // List of filters used
            callbacks: {},  // Sepcific callbacks
            anims: {},  // Sepcific animations functions
            loadFilter: undefined,  // Name of the filter used for loading

            enabled: true, // Indicates if it's enabled or not
            modal: false,   // Indicates if it's a modal window or not
            closeOnEscape: true,    // Indicates if the modal should close on Escape key
            closeOnClick: true, // Indicates if a click on the background should close the modal
            useKeyHandler: false,   // Indicates if the modal has to handle key down event

            showCloseButton: true,  // Indicates if the closeButonn should be added
            closeButton: '<a href="#" class="nyroModalClose nyroModalCloseButton nmReposition" title="close">Close</a>',    // Close button HTML

            stack: false,   // Indicates if links automatically binded inside the modal should stack or not
            nonStackable: 'form',   // Filter to not stack DOM element

            header: undefined,  // header include in every modal
            footer: undefined,  // footer include in every modal
            
            // Specific confirguation for gallery filter
            galleryLoop: true,  // Indicates if the gallery should loop
            galleryCounts: true,    // Indicates if the gallery counts should be shown
            ltr: true, // Left to Right by default. Put to false for Hebrew or Right to Left language. Used in gallery filter

            // Specific confirguation for DOM filter
            domCopy: false, // Indicates if DOM element should be copied or moved
            
            // Specific confirguation for link and form filters
            ajax: {}, // Ajax options to be used in link and form filter
            
            // Specific confirguation for image filter
            imageRegex: '[^\.]\.(jpg|jpeg|png|tiff|gif|bmp)\s*$',   // Regex used to detect image link

            selIndicator: 'nyroModalSel', // Value added when a form or Ajax is sent with a filter content

            swfObjectId: undefined, // Object id for swf object
            swf:  { // Default SWF attributes
                allowFullScreen: 'true',
                allowscriptaccess: 'always',
                wmode: 'transparent'
            },

            store: {},  // Storage object for filters.
            errorMsg: 'An error occured',   // Error message
            elts: { // HTML elements for the modal
                all: undefined,
                bg: undefined,
                load: undefined,
                cont: undefined,
                hidden: undefined
            },
            sizes: {    // Size information
                initW: undefined,   // Initial width
                initH: undefined,   // Initial height
                w: undefined,       // width
                h: undefined,       // height
                minW: undefined,    // minimum Width
                minH: undefined,    // minimum height
                wMargin: undefined, // Horizontal margin
                hMargin: undefined  // Vertical margin
            },
            anim: { // Animation names to use
                def: undefined,         // Default animation set to use if sspecific are not defined or doesn't exist
                showBg: undefined,      // Set to use for showBg animation
                hideBg: undefined,      // Set to use for hideBg animation
                showLoad: undefined,    // Set to use for showLoad animation
                hideLoad: undefined,    // Set to use for hideLoad animation
                showCont: undefined,    // Set to use for showCont animation
                hideCont: undefined,    // Set to use for hideCont animation
                showTrans: undefined,   // Set to use for showTrans animation
                hideTrans: undefined,   // Set to use for hideTrans animation
                resize: undefined       // Set to use for resize animation
            },

            _open: false,   // Indicates if the modal is open
            _bgReady: false,    // Indicates if the background is ready
            _opened: false, // Indicates if the modal was opened (useful for stacking)
            _loading: false,    // Indicates if the loading is shown
            _animated: false,   // Indicates if the modal is currently animated
            _transition: false, //Indicates if the modal is in transition
            _nmOpener: undefined,   // nmObj of the modal that opened the current one in non stacking mode
            _nbContentLoading: 0,   // Counter for contentLoading call
            _scripts: '',   // Scripts tags to be included
            _scriptsShown: '',  //Scripts tags to be included once the modal is swhon

            // save the object in data
            saveObj: function() {
                this.opener.data('nmObj', this);
            },
            // Open the modal
            open: function() {
                if (!this.enabled)
                    return false;
                if (this._nmOpener)
                    this._nmOpener._close();
                this.getInternal()._pushStack(this.opener);
                this._opened = false;
                this._bgReady = false;
                this._open = true;
                this._initElts();
                this._load();
                this._nbContentLoading = 0;
                this._callAnim('showBg', $.proxy(function() {
                    this._bgReady = true;
                    if (this._nmOpener) {
                        // fake closing of the opener nyroModal
                        this._nmOpener._bgReady = false;
                        this._nmOpener._loading = false;
                        this._nmOpener._animated = false;
                        this._nmOpener._opened = false;
                        this._nmOpener._open = false;
                        this._nmOpener.elts.cont = this._nmOpener.elts.hidden = this._nmOpener.elts.load = this._nmOpener.elts.bg = this._nmOpener.elts.all = undefined;
                        this._nmOpener.saveObj();
                        this._nmOpener = undefined;
                    }
                    this._contentLoading();
                }, this));
            },

            // Resize the modal according to sizes.initW and sizes.initH
            // Will call size function
            // @param recalc boolean: Indicate if the size should be recalaculated (useful when content has changed)
            resize: function(recalc) {
                if (recalc) {
                    this.elts.hidden.append(this.elts.cont.children().first().clone());
                    this.sizes.initW = this.sizes.w = this.elts.hidden.width();
                    this.sizes.initH = this.sizes.h = this.elts.hidden.height();
                    this.elts.hidden.empty();
                } else {
                    this.sizes.w = this.sizes.initW;
                    this.sizes.h = this.sizes.initH;
                }
                this._unreposition();
                this.size();
                this._callAnim('resize', $.proxy(function() {
                    this._reposition();
                }, this));
            },

            // Update sizes element to not go outsize the viewport.
            // Will call 'size' callback filter
            size: function() {
                var maxHeight = this.getInternal().fullSize.viewH - this.sizes.hMargin,
                    maxWidth = this.getInternal().fullSize.viewW - this.sizes.wMargin;
                if (this.sizes.minW && this.sizes.minW > this.sizes.w)
                    this.sizes.w = this.sizes.minW;
                if (this.sizes.minH && this.sizes.minH > this.sizes.h)
                    this.sizes.h = this.sizes.minH;
                if (this.sizes.h > maxHeight || this.sizes.w > maxWidth) {
                    // We're gonna resize the modal as it will goes outside the view port
                    this.sizes.h = Math.min(this.sizes.h, maxHeight);
                    this.sizes.w = Math.min(this.sizes.w, maxWidth);
                }
                this._callFilters('size');
            },

            // Get the nmObject for a new nyroModal
            getForNewLinks: function(elt) {
                var ret;
                if (this.stack && (!elt || this.isStackable(elt))) {
                    ret = $.extend(true, {}, this);
                    ret._nmOpener = undefined;
                    ret.elts.all = undefined;
                } else {
                    ret = $.extend({}, this);
                    ret._nmOpener = this;
                }
                ret.filters = [];
                ret.opener = undefined;
                ret._open = false;
                return ret;
            },
            
            // Indicate if an element can be stackable or not, regarding the nonStackable setting
            isStackable: function(elt) {
                return !elt.is(this.nonStackable);
            },

            // key handle function.
            // Will call 'keyHandle' callback filter
            keyHandle: function(e) {
                this.keyEvent = e;
                this._callFilters('keyHandle');
                this.keyEvent = undefined;
                delete(this.keyEvent);
            },

            // Get the internal object
            getInternal: function() {
                return _internal;
            },

            // Internal function for closing a nyroModal
            // Will call 'close' callback filter
            _close: function() {
                this.getInternal()._removeStack(this.opener);
                this._opened = false;
                this._open = false;
                this._callFilters('close');
            },
            // Public function for closing a nyroModal
            close: function() {
                this._close();
                this._callFilters('beforeClose');
                var self = this;
                this._unreposition();
                self._callAnim('hideCont', function() {
                    self._callAnim('hideLoad', function() {
                        self._callAnim('hideBg', function() {
                            self._callFilters('afterClose');
                            self.elts.cont.remove();
                            self.elts.hidden.remove();
                            self.elts.load.remove();
                            self.elts.bg.remove();
                            self.elts.all.remove();
                            self.elts.cont = self.elts.hidden = self.elts.load = self.elts.bg = self.elts.all = undefined;
                        });
                    });
                });
            },
            
            // Public function for destroying a nyroModal instance, only for non open modal
            destroy: function() {
                if (this._open)
                    return false;
                this._callFilters('destroy');
                if (this.elts.all)
                    this.elts.all.remove();
                return true;
            },

            // Init HTML elements
            _initElts: function() {
                if (!this.stack && this.getInternal().stack.length > 1)
                    this.elts = this.getInternal().stack[this.getInternal().stack.length-2]['nmObj'].elts;
                if (!this.elts.all || this.elts.all.closest('body').length == 0)
                    this.elts.all = this.elts.bg = this.elts.cont = this.elts.hidden = this.elts.load = undefined;
                if (!this.elts.all)
                    this.elts.all = $('<div />').appendTo(this.getInternal()._container);
                if (!this.elts.bg)
                    this.elts.bg = $('<div />').hide().appendTo(this.elts.all);
                if (!this.elts.cont)
                    this.elts.cont = $('<div />').hide().appendTo(this.elts.all);
                if (!this.elts.hidden)
                    this.elts.hidden = $('<div />').hide().appendTo(this.elts.all);
                this.elts.hidden.empty();
                if (!this.elts.load)
                    this.elts.load = $('<div />').hide().appendTo(this.elts.all);
                this._callFilters('initElts');
            },

            // Trigger the error
            // Will call 'error' callback filter
            _error: function(jqXHR) {
                this._callFilters('error', jqXHR);
            },

            // Set the HTML content to show.
            // - html: HTML content
            // - selector: selector to filter the content
            // Will init the size and call the 'size' function.
            // Will call 'filledContent' callback filter
            _setCont: function(html, selector) {
                if (selector) {
                    var tmp = [],
                        i = 0;
                    // Looking for script to store them
                    html = html
                        .replace(/\r\n/gi, 'nyroModalLN')
                        .replace(/<script(.|\s)*?\/script>/gi, function(x) {
                                tmp[i] = x;
                                return '<pre class=nyroModalScript rel="'+(i++)+'"></pre>';
                            });
                    var cur = $('<div>'+html+'</div>').find(selector);
                    if (cur.length) {
                        html = cur.html()
                            .replace(/<pre class="?nyroModalScript"? rel="?([0-9]*)"?><\/pre>/gi, function(x, y, z) { return tmp[y]; })
                            .replace(/nyroModalLN/gi, "\r\n");
                    } else {
                        // selector not found
                        this._error();
                        return;
                    }
                }
                this.elts.hidden
                    .append(this._filterScripts(html))
                    .prepend(this.header)
                    .append(this.footer)
                    .wrapInner($('<div />', {'class': 'nyroModal'+ucfirst(this.loadFilter)}));

                // Store the size of the element
                this.sizes.initW = this.sizes.w = this.elts.hidden.width();
                this.sizes.initH = this.sizes.h = this.elts.hidden.height();
                var outer = this.getInternal()._getOuter(this.elts.cont);
                this.sizes.hMargin = outer.h.total;
                this.sizes.wMargin = outer.w.total;

                this.size();

                this.loading = false;
                this._callFilters('filledContent');
                this._contentLoading();
            },

            // Filter an html content to remove the script[src] and store them appropriately if needed
            // - data: Data to filter
            _filterScripts: function(data) {
                if (typeof data != 'string')
                    return data;

                this._scripts = [];
                this._scriptsShown = [];
                var start = 0,
                    stStart = '<script',
                    stEnd = '</script>',
                    endLn = stEnd.length,
                    pos,
                    pos2,
                    tmp;
                while ((pos = data.indexOf(stStart, start)) > -1) {
                    pos2 = data.indexOf(stEnd)+endLn;
                    tmp = $(data.substring(pos, pos2));
                    if (!tmp.attr('src') || tmp.attr('rel') == 'forceLoad') {
                        if (tmp.attr('rev') == 'shown')
                            this._scriptsShown.push(tmp.get(0));
                        else
                            this._scripts.push(tmp.get(0));
                    }
                    data = data.substring(0, pos)+data.substr(pos2);
                    start = pos;
                }
                return data;
            },

            // Check if the nmObject has a specific filter
            // - filter: Filter name
            _hasFilter: function(filter) {
                var ret = false;
                $.each(this.filters, function(i, f) {
                    ret = ret || f == filter;
                });
                return ret;
            },

            // Remove a specific filter
            // - filter: Filter name
            _delFilter: function(filter) {
                this.filters = $.map(this.filters, function(v) {
                    if (v != filter)
                        return v;
                });
            },

            // Call a function against all active filters
            // - fct: Function name
            // - prm: Parameter to be used in callback
            // return an array of all return of callbacks; keys are filters name
            _callFilters: function(fct, prm) {
                this.getInternal()._debug(fct);
                var ret = [],
                    self = this;
                $.each(this.filters, function(i, f) {
                    ret[f] = self._callFilter(f, fct, prm);
                });
                if (this.callbacks[fct] && $.isFunction(this.callbacks[fct]))
                    this.callbacks[fct](this, prm);
                return ret;
            },

            // Call a filter function for a specific filter
            // - f: Filter name
            // - fct: Function name
            // - prm: Parameter to be used in callback
            // return the return of the callback
            _callFilter: function(f, fct, prm) {
                if (_filters[f] && _filters[f][fct] && $.isFunction(_filters[f][fct]))
                    return _filters[f][fct](this, prm);
                return undefined;
            },

            // Call animation callback.
            // Will also call beforeNNN and afterNNN filter callbacks
            // - fct: Animation function name
            // - clb: Callback once the animation is done
            _callAnim: function(fct, clb) {
                this.getInternal()._debug(fct);
                this._callFilters('before'+ucfirst(fct));
                if (!this._animated) {
                    this._animated = true;
                    if (!$.isFunction(clb)) clb = $.noop;
                    if (this.anims[fct] && $.isFunction(this.anims[fct])) {
                        curFct = this.anims[fct];
                    } else {
                        var set = this.anim[fct] || this.anim.def || 'basic';
                        if (!_animations[set] || !_animations[set][fct] || !$.isFunction(_animations[set][fct]))
                            set = 'basic';
                        curFct = _animations[set][fct];
                    }
                    curFct(this, $.proxy(function() {
                            this._animated = false;
                            this._callFilters('after'+ucfirst(fct));
                            clb();
                        }, this));
                }
            },

            // Load the content
            // Will call the 'load' function of the filter specified in the loadFilter parameter
            _load: function() {
                this.getInternal()._debug('_load');
                if (!this.loading && this.loadFilter) {
                    this.loading = true;
                    this._callFilter(this.loadFilter, 'load');
                }
            },

            // Show the content or the loading according to the current state of the modal
            _contentLoading: function() {
                if (!this._animated && this._bgReady) {
                    if (!this._transition && this.elts.cont.html().length > 0)
                        this._transition = true;
                    this._nbContentLoading++;
                    if (!this.loading) {
                        if (!this._opened) {
                            this._opened = true;
                            if (this._transition) {
                                var fct = $.proxy(function() {
                                    this._writeContent();
                                    this._callFilters('beforeShowCont');
                                    this._callAnim('hideTrans', $.proxy(function() {
                                        this._transition = false;
                                        this._callFilters('afterShowCont');
                                        this.elts.cont.append(this._scriptsShown);
                                        this._reposition();
                                        this.elts.cont.scrollTop(0);
                                    }, this));
                                }, this);
                                if (this._nbContentLoading == 1) {
                                    this._unreposition();
                                    this._callAnim('showTrans', fct);
                                } else {
                                    fct();
                                }
                            } else {
                                this._callAnim('hideLoad', $.proxy(function() {
                                    this._writeContent();
                                    this._callAnim('showCont', $.proxy(function() {
                                        this.elts.cont.append(this._scriptsShown);
                                        this._reposition();
                                        this.elts.cont.scrollTop(0);
                                    }, this));
                                }, this));
                            }
                        }
                    } else if (this._nbContentLoading == 1) {
                        var outer = this.getInternal()._getOuter(this.elts.load);
                        this.elts.load
                            .css({
                                position: 'fixed',
                                top: (this.getInternal().fullSize.viewH - this.elts.load.height() - outer.h.margin)/2,
                                left: (this.getInternal().fullSize.viewW - this.elts.load.width() - outer.w.margin)/2
                            });
                        if (this._transition) {
                            this._unreposition();
                            this._callAnim('showTrans', $.proxy(function() {
                                this._contentLoading();
                            }, this));
                        } else {
                            this._callAnim('showLoad', $.proxy(function() {
                                this._contentLoading();
                            }, this));
                        }
                    }
                }
            },

            // Write the content in the modal.
            // Content comes from the hidden div, scripts and eventually close button.
            _writeContent: function() {
                this.elts.cont
                    .empty()
                    .append(this.elts.hidden.contents())
                    .append(this._scripts)
                    .append(this.showCloseButton ? this.closeButton : '')
                    .css({
                        position: 'fixed',
                        width: this.sizes.w,
                        height: this.sizes.h,
                        top: (this.getInternal().fullSize.viewH - this.sizes.h - this.sizes.hMargin)/2,
                        left: (this.getInternal().fullSize.viewW - this.sizes.w - this.sizes.wMargin)/2
                    });
            },

            // Reposition elements with a class nmReposition
            _reposition: function() {
                var elts = this.elts.cont.find('.nmReposition');
                if (elts.length) {
                    var space = this.getInternal()._getSpaceReposition();
                    elts.each(function() {
                        var me = $(this),
                            offset = me.offset();
                        me.css({
                            position: 'fixed',
                            top: offset.top - space.top,
                            left: offset.left - space.left
                        });
                    });
                    this.elts.cont.after(elts);
                }
                this.elts.cont.css('overflow', 'auto');
                this._callFilters('afterReposition');
            },

            // Unreposition elements with a class nmReposition
            // Exaclty the reverse of the _reposition function
            _unreposition: function() {
                this.elts.cont.css('overflow', '');
                var elts = this.elts.all.find('.nmReposition');
                if (elts.length)
                    this.elts.cont.append(elts.removeAttr('style'));
                this._callFilters('afterUnreposition');
            }
        },
        _internal = {
            firstInit: true,
            debug: false,
            stack: [],
            fullSize: {
                w: 0,
                h: 0,
                wW: 0,
                wH: 0,
                viewW: 0,
                viewH: 0
            },
            nyroModal: function(opts, fullObj) {
                if (_internal.firstInit) {
                    _internal._container = $('<div />').appendTo($b);
                    $w.smartresize($.proxy(_internal._resize, _internal));
                    $d.on('keydown.nyroModal', $.proxy(_internal._keyHandler, _internal));
                    _internal._calculateFullSize();
                    _internal.firstInit = false;
                }
                return this.nmInit(opts, fullObj).each(function() {
                    _internal._init($(this).data('nmObj'));
                });
            },
            nmInit: function(opts, fullObj) {
                return this.each(function() {
                    var me = $(this);
                    if (fullObj)
                        me.data('nmObj', $.extend(true, {opener: me}, opts));
                    else
                        me.data('nmObj',
                            me.data('nmObj')
                                ? $.extend(true, me.data('nmObj'), opts)
                                : $.extend(true, {opener: me}, _nmObj, opts));
                });
            },
            nmDestroy: function() {
                return this.each(function() {
                    var me = $(this);
                    if (me.data('nmObj')) {
                        if (me.data('nmObj').destroy())
                            me.removeData('nmObj');
                    }
                });
            },
            nmCall: function() {
                return this.trigger('nyroModal');
            },

            nmManual: function(url, opts) {
                $('<a />', {href: url}).nyroModal(opts).trigger('nyroModal');
            },
            nmData: function(data, opts) {
                this.nmManual('#', $.extend({data: data}, opts));
            },
            nmObj: function(opts) {
                $.extend(true, _nmObj, opts);
            },
            nmInternal: function(opts) {
                $.extend(true, _internal, opts);
            },
            nmAnims: function(opts) {
                $.extend(true, _animations, opts);
            },
            nmFilters: function(opts) {
                $.extend(true, _filters, opts);
            },
            nmTop: function() {
                if (_internal.stack.length)
                    return _internal.stack[_internal.stack.length-1]['nmObj'];
                return undefined;
            },

            _debug: function(msg) {
                if (this.debug && window.console && window.console.log)
                    window.console.log(msg);
            },

            _container: undefined,

            _init: function(nm) {
                nm.filters = [];
                $.each(_filters, function(f, obj) {
                    if (obj.is && $.isFunction(obj.is) && obj.is(nm)) {
                        nm.filters.push(f);
                    }
                });
                nm._callFilters('initFilters');
                nm._callFilters('init');
                nm.opener
                    .off('nyroModal.nyroModal nmDisable.nyroModal nmEnable.nyroModal nmClose.nyroModal nmResize.nyroModal')
                    .on({
                        'nyroModal.nyroModal':  function() { nm.open(); return false;},
                        'nmDisable.nyroModal':  function() { nm.enabled = false; return false;},
                        'nmEnable.nyroModal':   function() { nm.enabled = true; return false;},
                        'nmClose.nyroModal':    function() { nm.close(); return false;},
                        'nmResize.nyroModal':   function() { nm.resize(); return false;}
                    });
            },

            _selNyroModal: function(obj) {
                return $(obj).data('nmObj') ? true : false;
            },

            _selNyroModalOpen: function(obj) {
                var me = $(obj);
                return me.data('nmObj') ? me.data('nmObj')._open : false;
            },

            _keyHandler: function(e) {
                var nmTop = $.nmTop();
                if (nmTop && nmTop.useKeyHandler) {
                    return nmTop.keyHandle(e);
                }
            },
            _pushStack: function(obj) {
                this.stack = $.map(this.stack, function(elA) {
                    if (elA['nmOpener'] != obj.get(0))
                        return elA;
                });
                this.stack.push({
                    nmOpener: obj.get(0),
                    nmObj: $(obj).data('nmObj')
                });
            },
            _removeStack: function(obj) {
                this.stack = $.map(this.stack, function(elA) {
                    if (elA['nmOpener'] != obj.get(0))
                        return elA;
                });
            },
            _resize: function() {
                var opens = $(':nmOpen').each(function() {
                    $(this).data('nmObj')._unreposition();
                });
                this._calculateFullSize();
                opens.trigger('nmResize');
            },
            _calculateFullSize: function() {
                this.fullSize = {
                    w: $d.width(),
                    h: $d.height(),
                    wW: $w.width(),
                    wH: $w.height()
                };
                this.fullSize.viewW = Math.min(this.fullSize.w, this.fullSize.wW);
                this.fullSize.viewH = Math.min(this.fullSize.h, this.fullSize.wH);
            },
            _getCurCSS: function(elm, name) {
                var ret = parseInt($.css(elm, name, true));
                return isNaN(ret) ? 0 : ret;
            },
            _getOuter: function(elm) {
                elm = elm.get(0);
                var ret = {
                    h: {
                        margin: this._getCurCSS(elm, 'marginTop') + this._getCurCSS(elm, 'marginBottom'),
                        border: this._getCurCSS(elm, 'borderTopWidth') + this._getCurCSS(elm, 'borderBottomWidth'),
                        padding: this._getCurCSS(elm, 'paddingTop') + this._getCurCSS(elm, 'paddingBottom')
                    },
                    w: {
                        margin: this._getCurCSS(elm, 'marginLeft') + this._getCurCSS(elm, 'marginRight'),
                        border: this._getCurCSS(elm, 'borderLeftWidth') + this._getCurCSS(elm, 'borderRightWidth'),
                        padding: this._getCurCSS(elm, 'paddingLeft') + this._getCurCSS(elm, 'paddingRight')
                    }
                };

                ret.h.outer = ret.h.margin + ret.h.border;
                ret.w.outer = ret.w.margin + ret.w.border;

                ret.h.inner = ret.h.padding + ret.h.border;
                ret.w.inner = ret.w.padding + ret.w.border;

                ret.h.total = ret.h.outer + ret.h.padding;
                ret.w.total = ret.w.outer + ret.w.padding;

                return ret;
            },
            _getSpaceReposition: function() {
                var outer = this._getOuter($b),
                    ie7 = browser.msie && browser.version < 8 && !(screen.height <= $w.height()+23);
                return {
                    top: $w.scrollTop() - (!ie7 ? outer.h.border / 2 : 0),
                    left: $w.scrollLeft() - (!ie7 ? outer.w.border / 2 : 0)
                };
            },

            _getHash: function(url) {
                if (typeof url == 'string') {
                    var hashPos = url.indexOf('#');
                    if (hashPos > -1)
                        return url.substring(hashPos);
                }
                return '';
            },
            _extractUrl: function(url) {
                var ret = {
                    url: undefined,
                    sel: undefined
                };

                if (url) {
                    var hash = this._getHash(url),
                        hashLoc = this._getHash(window.location.href),
                        curLoc = window.location.href.substring(0, window.location.href.length - hashLoc.length),
                        req = url.substring(0, url.length - hash.length);
                    ret.sel = hash;
                    if (req != curLoc && req != baseHref)
                        ret.url = req;
                }
                return ret;
            }
        },
        _animations = {
            basic: {
                showBg: function(nm, clb) {
                    nm.elts.bg.css({opacity: 0.7}).show();
                    clb();
                },
                hideBg: function(nm, clb) {
                    nm.elts.bg.hide();
                    clb();
                },
                showLoad: function(nm, clb) {
                    nm.elts.load.show();
                    clb();
                },
                hideLoad: function(nm, clb) {
                    nm.elts.load.hide();
                    clb();
                },
                showCont: function(nm, clb) {
                    nm.elts.cont.show();
                    clb();
                },
                hideCont: function(nm, clb) {
                    nm.elts.cont.hide();
                    clb();
                },
                showTrans: function(nm, clb) {
                    nm.elts.cont.hide();
                    nm.elts.load.show();
                    clb();
                },
                hideTrans: function(nm, clb) {
                    nm.elts.cont.show();
                    nm.elts.load.hide();
                    clb();
                },
                resize: function(nm, clb) {
                    nm.elts.cont.css({
                        width: nm.sizes.w,
                        height: nm.sizes.h,
                        top: (nm.getInternal().fullSize.viewH - nm.sizes.h - nm.sizes.hMargin)/2,
                        left: (nm.getInternal().fullSize.viewW - nm.sizes.w - nm.sizes.wMargin)/2
                    });
                    clb();
                }
            }
        },
        _filters = {
            basic: {
                is: function(nm) {
                    return true;
                },
                init: function(nm) {
                    if (nm.opener.attr('rev') == 'modal')
                        nm.modal = true;
                    if (nm.modal)
                        nm.closeOnEscape = nm.closeOnClick = nm.showCloseButton = false;
                    if (nm.closeOnEscape)
                        nm.useKeyHandler = true;
                },
                initElts: function(nm) {
                    nm.elts.bg.addClass('nyroModalBg');
                    if (nm.closeOnClick)
                        nm.elts.bg.off('click.nyroModal').on('click.nyroModal', function(e) {
                            e.preventDefault();
                            nm.close();
                        });
                    nm.elts.cont.addClass('nyroModalCont');
                    nm.elts.hidden.addClass('nyroModalCont nyroModalHidden');
                    nm.elts.load.addClass('nyroModalCont nyroModalLoad');
                },
                error: function(nm) {
                    nm.elts.hidden.addClass('nyroModalError');
                    nm.elts.cont.addClass('nyroModalError');
                    nm._setCont(nm.errorMsg);
                },
                beforeShowCont: function(nm) {
                    nm.elts.cont
                        .find('.nyroModal').each(function() {
                            var cur = $(this);
                            cur.nyroModal(nm.getForNewLinks(cur), true);
                        }).end()
                        .find('.nyroModalClose').on('click.nyroModal', function(e) {
                            e.preventDefault();
                            nm.close();
                        });
                },
                keyHandle: function(nm) {
                    // used for escape key
                    if (nm.keyEvent.keyCode == 27 && nm.closeOnEscape) {
                        nm.keyEvent.preventDefault();
                        nm.close();
                    }
                }
            },

            custom: {
                is: function(nm) {
                    return true;
                }
            }
        };

    // Add jQuery call fucntions
    $.fn.extend({
        nm: _internal.nyroModal,
        nyroModal: _internal.nyroModal,
        nmInit: _internal.nmInit,
        nmDestroy: _internal.nmDestroy,
        nmCall: _internal.nmCall
    });

    // Add global jQuery functions
    $.extend({
        nmManual: _internal.nmManual,
        nmData: _internal.nmData,
        nmObj: _internal.nmObj,
        nmInternal: _internal.nmInternal,
        nmAnims: _internal.nmAnims,
        nmFilters: _internal.nmFilters,
        nmTop: _internal.nmTop
    });

    // Add jQuery selectors
    $.expr[':'].nyroModal = $.expr[':'].nm = _internal._selNyroModal;
    $.expr[':'].nmOpen = _internal._selNyroModalOpen;
});

// Smartresize plugin
(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  };
    // smartresize
    jQuery.fn[sr] = function(fn){  return fn ? this.on('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
// ucFirst
function ucfirst(str) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: ucfirst('kevin van zonneveld');
    // *     returns 1: 'Kevin van zonneveld'
    str += '';
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1);
}

/**
 *
 * Version: 0.2.6
 * Author:  Gianluca Guarini
 * Contact: gianluca.guarini@gmail.com
 * Website: http://www.gianlucaguarini.com/
 * Twitter: @gianlucaguarini
 *
 * Copyright (c) 2013 Gianluca Guarini
 */
(function(e){e.fn.extend({BlackAndWhite:function(d){var j=this;d=e.extend({hoverEffect:!0,webworkerPath:!1,responsive:!0,invertHoverEffect:!1,speed:500,onImageReady:null},d);var y=d.hoverEffect,s=d.webworkerPath,q=d.invertHoverEffect,z=d.responsive,t=e.isPlainObject(d.speed)?d.speed.fadeIn:d.speed;e.isPlainObject(d.speed);var A=document.all&&!window.opera&&window.XMLHttpRequest?!0:!1,m={},u=function(c){if(m[c]||""===m[c])return m[c]+c;var e=document.createElement("div"),b=" Moz Webkit O ms Khtml".split(" "),
a;for(a in b)if("undefined"!==typeof e.style[b[a]+c])return m[c]=b[a],b[a]+c;return c.toLowerCase()},v,n=document.createElement("div");n.style.cssText=" -webkit- -moz- -o- -ms- ".split(" ").join("filter:blur(2px); ");v=!!n.style.length&&(void 0===document.documentMode||9<document.documentMode);var B=!!document.createElement("canvas").getContext,C=e(window),n="undefined"!==typeof Worker?!0:!1;u("Filter");var l=[],p=n&&s?new Worker(s+"BnWWorker.js"):!1,D=function(c){e(c.currentTarget).find(".BWfade").stop(!0,
!0)[!q?"fadeIn":"fadeOut"](t)},E=function(c){e(c.currentTarget).find(".BWfade").stop(!0,!0)[q?"fadeIn":"fadeOut"](t)},r=function(c){if("function"===typeof d.onImageReady)d.onImageReady(c)},w=function(){l.length&&(p.postMessage(l[0].imageData),p.onmessage=function(c){l[0].ctx.putImageData(c.data,0,0);r(l[0].img);l.splice(0,1);w()})},x=function(c,d){var b=c[0],a=b.src,g=c.width(),f=c.height(),h={position:"absolute",top:0,left:0,display:q?"none":"block",width:g,height:f};if(B&&!v){g=b.width;f=b.height;
e('<canvas class="BWfade" width="'+g+'" height="'+f+'"></canvas>').prependTo(d);a=d.find("canvas");a.css(h);h=a[0].getContext("2d");a=0;h.drawImage(b,0,0,g,f);var f=h.getImageData(0,0,g,f),k=f.data,j=k.length;if(p)l.push({imageData:f,ctx:h,img:b});else{for(;a<j;a+=4)g=0.3*k[a]+0.59*k[a+1]+0.11*k[a+2],k[a]=k[a+1]=k[a+2]=g;h.putImageData(f,0,0);r(b)}}else h[u("Filter")]="grayscale(100%)",e("<img src="+a+' width="'+g+'" height="'+f+'" class="BWFilter BWfade" /> ').prependTo(d),e(".BWFilter").css(e.extend(h,
{filter:"progid:DXImageTransform.Microsoft.BasicImage(grayscale=1)"})),r(b)};this.init=function(){j.each(function(c,d){var b=e(d),a=b.find("img");if(a.width())x(a,b);else a.on("load",function(){x(a,b)})});p&&w();y&&(j.on("mouseleave",D),j.on("mouseenter",E));if(z)C.on("resize orientationchange",j.resizeImages)};this.resizeImages=function(){j.each(function(c,d){var b=e(d).find("img:not(.BWFilter)"),a;A?(a=e(b).prop("width"),b=e(b).prop("height")):(a=e(b).width(),b=e(b).height());e(this).find(".BWFilter, canvas").css({width:a,
height:b})})};return this.init(d)}})})(jQuery);