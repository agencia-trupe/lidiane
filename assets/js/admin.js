//Reordenar Slides
$('.ordenar-slides').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.slides-mensagem, .salvar-ordem-slides').toggle();
  return false;
});

$('.salvar-ordem-slides').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/painel/slideshow/sort_slides/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.slides-mensagem span').text('Slides ordenados com sucesso.');
      $('.salvar-ordem-slides').toggle();
      $('.ordenar-slides').toggle();
    }
  });
  return false;
});

//Reordenar Mostras
$('.ordenar-mostras').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.mostras-mensagem, .salvar-ordem-mostras').toggle();
  return false;
});

$('.salvar-ordem-mostras').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/mostras/admin_mostras/sort_mostras/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.mostras-mensagem span').text('Mostras ordenados com sucesso.');
      $('.salvar-ordem-mostras').toggle();
      $('.ordenar-mostras').toggle();
    }
  });
  return false;
});

//Reordenar Projetos
$('.ordenar-projetos').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.projetos-mensagem, .salvar-ordem-projetos').toggle();
  return false;
});

$('.salvar-ordem-projetos').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/projetos/admin_projetos/sort_projetos/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.projetos-mensagem span').text('Projetos ordenados com sucesso.');
      $('.salvar-ordem-projetos').toggle();
      $('.ordenar-projetos').toggle();
    }
  });
  return false;
});

//Reordenar Categorias
$('.ordenar-tipos').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.tipos-mensagem, .salvar-ordem-tipos').toggle();
  return false;
});

$('.salvar-ordem-tipos').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/projetos/admin_tipos/sort_tipos/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.tipos-mensagem span').text('Categorias de projeto ordenados com sucesso.');
      $('.salvar-ordem-tipos').toggle();
      $('.ordenar-tipos').toggle();
    }
  });
  return false;
});

//Reordenar Mídias
$('.ordenar-midias').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.midias-mensagem, .salvar-ordem-midias').toggle();
  return false;
});

$('.salvar-ordem-midias').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/midias/admin_midias/sort_midias/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.midias-mensagem span').text('Entradas ordenadas com sucesso.');
      $('.salvar-ordem-midias').toggle();
      $('.ordenar-midias').toggle();
    }
  });
  return false;
});

function displayAlert(data){
    var type = 'alert-' + data.status;
    $('.top-alert').hide().addClass(type).fadeIn('fast', function(){
      $('.alert-content').text(data.texto);
    });
}

$(document.body).on('click', '.btn-reordenar', function(){
    $("#projeto-images").sortable({'disabled': false, 'delay':'100'});
    $("#projeto-images li").css({'cursor':'pointer'});
    $('.fotos-lista').addClass('reordena');
    $(this).removeClass('btn-info btn-reordenar')
           .addClass('btn-warning btn-salva-ordem')
           .text('Salvar ordem')
    return false;
  }).on('click', '.btn-salva-ordem', function(){
      $(this).removeClass('btn-warning btn-salva-ordem')
             .addClass('btn-info btn-reordenar')
             .text('Reordenar');
      $.ajax({
        type: "POST",
        url: location.protocol + "//" + location.hostname + "/painel/projetos/sort_fotos/",
        data: $("#projeto-images").sortable("serialize"),
        success: function(status){
          $("#projeto-images").sortable( "option", "disabled", true );
          $("#projeto-images li").css({'cursor':'default'});
          $(".fotos-lista").removeClass('reordena');
          var alerta = {'status':'success', 'texto':'Fotos reordenadas com sucesso.'}
          displayAlert(alerta);
        }
      });
      return false;
});
$(function(){
  /**
   * Fecha os alertas do topo.
   */
  $('.top-alert').on('click', '.close', function(){
    $('.top-alert').slideUp('fast', function(){
      $(this).removeClass('alert-success alert-error');
      $('.alert-content').text('');
    });
    return false;
  });
  /**
   * Exibe o formulário de adição de fotos;
   */
  $('.btn-adicionar-foto').click(function(){
    $('.form-adicionar-foto').slideDown().removeClass('invisible');
    return false;
  });
  $('.btn-adicionar-foto-cancela').click(function(){
    $('.form-adicionar-foto').slideUp();
    return false;
  });
  /**
   * Exclui uma foto relacionada a um projeto
   */
  function deletaFoto(target, module){
    var foto = target.parent();
    $.ajax({
        type: "POST",
        url: location.protocol + "//" + location.hostname + "/painel/" + module + "/deleta_foto/",
        data: {
          'ajax' : 1,
          'foto_id' : target.data('id')
        },
        success: function(msg){
          obj = JSON.parse(msg);
          var alerta = {'status':obj.status, 'texto':obj.msg }
          if(obj.status == "success"){
            $(foto).hide();
            displayAlert(alerta);
          } else {
            displayAlert(alerta);
          }
        }
    });
  }
  $(document.body).on('click', '.btn-delete', function(){
    deletaFoto($(this), $(this).data('module'));
    return false;
  });
  /*$(document.body).on('click', 'btn-delete', function(){
    
    return false;
  });*/
  function displayReordena()
  {
    if($('.btn-reordenar').hasClass('invisible'))
    {
      $('.btn-reordenar').removeClass('invisible');
    }
  }
  $('#projetos-upload').submit(function(e) {
      e.preventDefault();
      $.ajaxFileUpload({
         url         : location.protocol + "//" + location.hostname + "/painel/projetos/adiciona_foto/",
         secureuri      : false,
         fileElementId  : 'projeto-foto-upload',
         dataType    : 'json',
         data        : {
            'ajax' : 1,
            'projeto_id' : $('.id').val()
         },
         success  : function (data, status)
         {
            var alerta = {'status':data.status, 'texto' : data.msg}
            if(data.status == 'error')
            {
              displayAlert(alerta);
            }
            else
            {
              var foto_template = ich.foto_template(data);
              displayReordena();
              $('#projeto-images').append(foto_template);
              displayAlert(alerta);
            }
         }
      });
      return false;
   });

   $('#mostras-upload').submit(function(e) {
      e.preventDefault();
      $.ajaxFileUpload({
         url         : location.protocol + "//" + location.hostname + "/painel/mostras/adiciona_foto/",
         secureuri      : false,
         fileElementId  : 'projeto-foto-upload',
         dataType    : 'json',
         data        : {
            'ajax' : 1,
            'mostra_id' : $('.id').val()
         },
         success  : function (data, status)
         {
            var alerta = {'status':data.status, 'texto' : data.msg}
            if(data.status == 'error')
            {
              displayAlert(alerta);
            }
            else
            {
              var foto_template = ich.foto_template(data);
              displayReordena();
              $('#projeto-images').append(foto_template);
              displayAlert(alerta);
            }
         }
      });
      return false;
   });

  $('#clipping-upload').submit(function(e) {
      e.preventDefault();
      $.ajaxFileUpload({
         url         : location.protocol + "//" + location.hostname + "/painel/midia/adiciona_foto/",
         secureuri      : false,
         fileElementId  : 'projeto-foto-upload',
         dataType    : 'json',
         data        : {
            'ajax' : 1,
            'midia_id' : $('.id').val()
         },
         success  : function (data, status)
         {
            var alerta = {'status':data.status, 'texto' : data.msg}
            if(data.status == 'error')
            {
              displayAlert(alerta);
            }
            else
            {
              var midia_template = ich.midia_template(data);
              displayReordena();
              $('#midia-images').append(midia_template);
              displayAlert(alerta);
            }
         }
      });
      return false;
   });
});